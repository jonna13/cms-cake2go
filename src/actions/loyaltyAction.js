/**
 * Created by jedachas on 3/9/17.
 */
import React from 'react';
import * as types from '../constants/LoyaltyActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getCategorys = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=category';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadCategorySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadCategoryFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.PRODCATEGORYTABLE));
        dispatch(loadCategoryFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getLocations = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=locations';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadLocationSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadLocationFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.PRODCATEGORYTABLE));
        dispatch(loadLocationFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getLoyalty = (config, router) => {
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=loyaltytable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadLoyaltySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE+'1', Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LOYALTYTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      console.info('getLoyalty catch', e);
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE+'2', Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.LOYALTYTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}


export let viewLoyalty = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=loyaltytable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewLoyaltySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LOYALTYTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.LOYALTYTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let addLoyalty = (props, image, router) => {

  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_loyalty';
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());
    dispatch(addUploadImage(dispatch, props, image, router));
  }
}

let addUploadImage = (dispatch, props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.REWARDS
  };
  return dispatch => {
    if(!_.isEmpty(image)){
      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          dispatch(addLoyaltyFull(dispatch, props, image, router));
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });
    }
    else{
      dispatch(loadingAction.hideLoading());
      dispatch(dialogAction.openNotification('No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
    }
  }
}

let addLoyaltyFull = (dispatch, props, image, router) => {
  return dispatch => {
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('New reward added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/loyalty');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}
// export let addLoyalty = (props, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'add_loyalty';
//   props.status = (props.status) ? 'active' : 'inactive';
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//
//     post(props).then(result => {
//       let {data} = result;
//
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('New reward added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         router.push('/loyalty');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//
//   }
// }
// export let updateLoyalty = (props, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'update_loyalty';
//
//   if (typeof props.status == 'boolean') {
//     props.status = (props.status) ? 'active' : 'inactive';
//   }
//
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//     post(props).then(result => {
//       let {data} = result;
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('Great! Reward updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         dispatch(removeSelectedRecord());
//         router.push('/loyalty');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//   }
// }
export let updateLoyalty = (props, image, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_loyalty';

  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    dispatch(updateUploadImage(dispatch, props, image, router));
  }
}

let updateUploadImage = (dispatch, props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.REWARDS
  };
  return dispatch => {
    if(_.isEmpty(image)){
      dispatch(updateLoyaltyFull(dispatch, props, image, router));
    }
    else{
        upload(uploadparam).then(result => {
          let {data} = result;
          if (data.response == "Success") {
            props.image = data.data;
            dispatch(updateLoyaltyFull(dispatch, props, image, router));
          }
          else{
            dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
          }
        }).catch( e => {
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
          dispatch(loadingAction.hideLoading());
          console.info('Error', e);
        });
    }
  }
}

let updateLoyaltyFull = (dispatch, props, image, router) => {
  return dispatch => {
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! Reward updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        router.push('/loyalty');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadLoyaltySuccess = (data) => {
  return {
    type: types.GET_LOYALTY_SUCCESS,
    data
  }
}

export let viewLoyaltySuccess = (data) => {
  return{
    type:types.VIEW_LOYALTY_SUCCESS,
    data
  }
}

export let loadLevelSuccess = (data) => {
  return{
    type: types.GET_LOYALTYLEVEL_SUCCESS,
    data
  }
}

export let loadLevelFailed = () => {
  return{
    type: types.GET_LOYALTYLEVEL_FAILED
  }
}

export let loadCategorySuccess = (data) => {
  return{
    type: types.GET_LOYALTY_CATEGORY_SUCCESS,
    data
  }
}

export let loadCategoryFailed = () => {
  return{
    type: types.GET_LOYALTY_CATEGORY_FAILED
  }
}

export let loadLocationSuccess = (data) => {
  return{
    type: types.GET_LOCATION_SUCCESS,
    data
  }
}

export let loadLocationFailed = () => {
  return{
    type: types.GET_LOCATION_FAILED
  }
}
