/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import * as types from '../constants/CashierActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getCashiers = (config) => {
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=cashiertable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadCashierSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.CASHIERTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.CASHIERTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewCashier = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=cashiertable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewCashierSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.CASHIERTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.CASHIERTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let addCashier = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.CASHIER
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_cashier';
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (!_.isEmpty(image)) {
      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          saveCashier(props, router, dispatch);
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });
    }
    else{
      saveCashier(props, router, dispatch);
    }
  }
}

let saveCashier = (props, router, dispatch) => {
  post(props).then(result => {
    let {data} = result;
    console.info('cashier addCashier', result);
    if (data.response == 'Success') {
      dispatch(dialogAction.openNotification('New cashier added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
      router.push('/cashier');
    }
    else if (data.response == 'Expired'){
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
      dispatch(auth.logoutUser());
    }
    else{
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
    }
    dispatch(loadingAction.hideLoading());

  }).catch( e => {
    dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
    dispatch(loadingAction.hideLoading());
    console.info('Error', e);
  });
}

export let updateCashier = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.CASHIER
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_cashier';
  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }
  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (_.isEmpty(image)){
      updateSelectedRecord(props, router, dispatch);
    }
    else{
      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          updateSelectedRecord(props, router, dispatch);
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });
    }
  }
}

let updateSelectedRecord = (props, router, dispatch) => {
  post(props).then(result => {
    let {data} = result;
    console.info('updateCashier', result);
    if (data.response == 'Success') {
      dispatch(dialogAction.openNotification('Great! cashier updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
      dispatch(removeSelectedRecord());
      router.push('/cashier');
    }
    else if (data.response == 'Expired'){
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
      dispatch(auth.logoutUser());
    }
    else{
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
    }
    dispatch(loadingAction.hideLoading());
  }).catch( e => {
    dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
    dispatch(loadingAction.hideLoading());
    console.info('Error', e);
  });
}


export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadCashierSuccess = (data) => {
  return {
    type: types.GET_CASHIER_SUCCESS,
    data
  }
}

export let viewCashierSuccess = (data) => {
  return{
    type:types.VIEW_CASHIER_SUCCESS,
    data
  }
}
