/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import * as types from '../constants/PostActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getPosts = (config) => {

  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=posttable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadPostSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.POSTTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.POSTTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewPost = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=posttable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewPostSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.POSTTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.POSTTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}


export let addPost = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.POST
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_post';
  props.status = (props.status) ? 'active' : 'inactive';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (!_.isEmpty(image)) {

      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          post(props).then(result => {
            let {data} = result;
            console.info('post addPost', result);
            if (data.response == 'Success') {
              dispatch(dialogAction.openNotification('New post added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
              router.push('/post');
            }
            else if (data.response == 'Expired'){
              dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
              dispatch(auth.logoutUser());
            }
            else{
              dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
            }
            dispatch(loadingAction.hideLoading());

          }).catch( e => {
            dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
            dispatch(loadingAction.hideLoading());
            console.info('Error', e);
          });
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }

      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });

    }
    else{
      dispatch(loadingAction.hideLoading());
      dispatch(dialogAction.openNotification('No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
    }
  }
}

export let updatePost = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.POST
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_post';
  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }
  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (_.isEmpty(image)){
      updateSelectedRecord(props, router, dispatch);
    }
    else{
      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          updateSelectedRecord(props, router, dispatch);
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });
    }
  }
}

let updateSelectedRecord = (props, router, dispatch) => {
  post(props).then(result => {
    let {data} = result;
    console.info('updatePost', result);
    if (data.response == 'Success') {
      dispatch(dialogAction.openNotification('Great! Post updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
      dispatch(removeSelectedRecord());
      router.push('/post');
    }
    else if (data.response == 'Expired'){
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
      dispatch(auth.logoutUser());
    }
    else{
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
    }
    dispatch(loadingAction.hideLoading());
  }).catch( e => {
    dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
    dispatch(loadingAction.hideLoading());
    console.info('Error', e);
  });
}


export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadPostSuccess = (data) => {
  return {
    type: types.GET_POST_SUCCESS,
    data
  }
}

export let viewPostSuccess = (data) => {
  return{
    type:types.VIEW_POST_SUCCESS,
    data
  }
}
