/**
 * Created by jonna on 8/30/18.
 */
import React from 'react';
import * as types from '../constants/BranchActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getBranchCategory = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=loccategory';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadBranchCategorySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadBranchCategoryFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.BRANCHCATEGORYTABLE));
        dispatch(loadBranchCategoryFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getBranchCodes = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=branchCode';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadBranchCodeSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadBranchCodeFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.BRANCHTABLE));
        dispatch(loadBranchCodeFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getBranchs = (config) => {
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=loctable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadBranchSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.BRANCHTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.BRANCHTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewBranch = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=loctable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewBranchSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.BRANCHTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.BRANCHTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let addBranch = (props, image, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_branch';
  props.status = (props.status) ? 'active' : 'inactive';
  props.locFlag = (props.locFlag) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());
    dispatch(addUploadBranch(dispatch, props, image, router));
  }
}
let addUploadBranch = (dispatch, props, image, router) => {
    let uploadparam = {
      image: image,
      module: Config.IMAGE.HOTELS
    };
    return dispatch => {
      if(!_.isEmpty(image)){
        upload(uploadparam).then(result => {
          let {data} = result;
          if (data.response == "Success") {
            props.image = data.data;
            dispatch(addBranchFull(dispatch, props, image, router));
          }
          else{
            dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
          }
        }).catch( e => {
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
          dispatch(loadingAction.hideLoading());
          console.info('Error', e);
        });
      }
      else{
        dispatch(loadingAction.hideLoading());
        dispatch(dialogAction.openNotification('No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
      }
    }
}
let addBranchFull = (dispatch, props, image, router) => {
  return dispatch => {
    post(props).then(result => {
      let {data} = result;

      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('New branch added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/branch');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}
// export let addBranch = (props, image, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'add_branch';
//   props.status = (props.status) ? 'active' : 'inactive';
//   props.locFlag = (props.locFlag) ? 'active' : 'inactive';
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//
//     post(props).then(result => {
//       let {data} = result;
//       console.log(' Branch Dataaa', props);
//
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('New branch added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         router.push('/branch');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//
//   }
// }

export let updateBranch = (props, image, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_branch';

  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }
  if (typeof props.locFlag == 'boolean') {
    props.locFlag = (props.locFlag) ? 'active' : 'inactive';
  }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    dispatch(updateUploadBranch(dispatch, props, image, router));

  }
}

let updateUploadBranch = (dispatch, props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.HOTELS
  };
  return dispatch => {
    if(_.isEmpty(image)){
      dispatch(updateBranchFull(dispatch, props, image, router));
    }
    else{
        upload(uploadparam).then(result => {
          let {data} = result;
          if (data.response == "Success") {
            props.image = data.data;
            dispatch(updateBranchFull(dispatch, props, image, router));
          }
          else{
            dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
          }
        }).catch( e => {
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
          dispatch(loadingAction.hideLoading());
          console.info('Error', e);
        });
    }
  }
}

let updateBranchFull = (dispatch, props, image, router) => {
  return dispatch => {
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! Branch updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        router.push('/branch');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}
// export let updateBranch = (props, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'update_branch';
//
//   if (typeof props.status == 'boolean') {
//     props.status = (props.status) ? 'active' : 'inactive';
//   }
//   if (typeof props.locFlag == 'boolean') {
//     props.locFlag = (props.locFlag) ? 'active' : 'inactive';
//   }
//
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//     post(props).then(result => {
//       let {data} = result;
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('Great! Branch updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         dispatch(removeSelectedRecord());
//         router.push('/branch');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//   }
// }

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadBranchSuccess = (data) => {
  return {
    type: types.GET_BRANCH_SUCCESS,
    data
  }
}

export let viewBranchSuccess = (data) => {
  return{
    type:types.VIEW_BRANCH_SUCCESS,
    data
  }
}

export let viewCategorySuccess = (data) => {
  return{
    type:types.GET_CATEGORY_SUCCESS,
    data
  }
}

export let loadBranchCodeSuccess = (data) => {
  return{
    type: types.GET_BRANCHCODE_SUCCESS,
    data
  }
}

export let loadBranchCodeFailed = () => {
  return{
    type: types.GET_BRANCHCODE_FAILED
  }
}

export let loadBranchCategorySuccess = (data) => {
  return{
    type: types.GET_BRANCH_CATEGORY_SUCCESS,
    data
  }
}

export let loadBranchCategoryFailed = () => {
  return{
    type: types.GET_BRANCH_CATEGORY_FAILED
  }
}
