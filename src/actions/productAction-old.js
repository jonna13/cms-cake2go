/**
 * Created by jedachas on 2/23/17.
 *
 * Product Actions
 *
 */
import React from 'react';
import * as types from '../constants/ProductActionTypes';
import {get, post, upload, all} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let viewProduct = (id) => {
  return dispatch => {
    dispatch(loadingAction.showLoading());
    all([getProduct(id), getCategory()])
      .then(axios.spread( (item, cat ) => {
        const product = item.data, category = cat.data;
        let expired = false, prodfailed = false, categoryfailed = false;

        // Single Product
        if (product.response == 'Success') {
          dispatch(viewProductSuccess(product));
        }
        else if(product.response == 'Expired'){
          expired=true;
        }
        else if(product.response != 'Empty'){
          prodfailed=true;
        }

        // Category
        if (category.response == 'Success') {
          dispatch(loadCategorySuccess(category));
        }
        else if (category.response == 'Expired'){
          expired=true;
          dispatch(loadCategoryFailed(category));
        }
        else if (category.response == 'Empty'){
          dispatch(loadCategoryFailed(category));
        }
        else{
          categoryfailed=true;
          dispatch(loadCategoryFailed(category));
        }

        if (product.response != 'Success' || category.response != 'Success') {
          let failarray = [];
          if (prodfailed) {  failarray.push(Config.ERROR_CODE.PRODUCTTABLE); }
          if (categoryfailed) {  failarray.push(Config.ERROR_CODE.PRODCATEGORYTABLE); }

          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE,
            Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + failarray ));
        }
        else if(expired){
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
          dispatch(auth.logoutUser());
        }

        dispatch(loadingAction.hideLoading());
      }))
      .catch( (e)=> {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info(e);
      });
  }
}

let getProduct = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=producttable&filter='+id;
  return get(param);
}

let getCategory = () => {
  let param = '?function=json&category='+Config.GATEWAY.CATEGORY+'&table=categorytable';
  return get(param);
}

let getSubcategory = (catid) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=subcategorytable_category&filter='+catid;
  return get(param);
}

export let getProducts = (config) =>{
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=producttable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadProductSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else if (data.response != 'Empty'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.PRODUCTTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.PRODUCTTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getProductCategory = () =>{
  let param = '?function=json&category='+Config.GATEWAY.CATEGORY+'&table=categorytable_active';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadCategorySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadCategoryFailed(data));
        dispatch(auth.logoutUser());
      }
      else if (data.response == 'Empty'){
        dispatch(loadCategoryFailed(data));
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.PRODCATEGORYTABLE));
        dispatch(loadCategoryFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getProductSubCategory = (catID) =>{
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=subcategorytable_category&filter='+catID;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadSubcategorySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadSubcategoryFailed(data));
        dispatch(auth.logoutUser());
      }
      else if (data.response == 'Empty'){
        dispatch(loadSubcategoryFailed(data));
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.PRODSUBCATEGORYTABLE));
        dispatch(loadSubcategoryFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let addProduct = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.PRODUCT
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_product';
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (!_.isEmpty(image)) {

      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          post(props).then(result => {
            let {data} = result;
            console.info('post addProduct', result);
            if (data.response == 'Success') {
              dispatch(dialogAction.openNotification('New product added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
              router.push('/product');
            }
            else if (data.response == 'Expired'){
              dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
              dispatch(auth.logoutUser());
            }
            else{
              dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
            }
            dispatch(loadingAction.hideLoading());

          }).catch( e => {
            dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
            dispatch(loadingAction.hideLoading());
            console.info('Error', e);
          });
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });

    }
    else{
      dispatch(loadingAction.hideLoading());
      dispatch(dialogAction.openNotification('No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
    }
  }
}

export let updateProduct = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.PRODUCT
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_product';
  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }
  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (_.isEmpty(image)){
      updateSelectedRecord(props, router, dispatch);
    }
    else{
      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          updateSelectedRecord(props, router, dispatch);
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });
    }
  }
}

let updateSelectedRecord = (props, router, dispatch) => {
  post(props).then(result => {
    let {data} = result;
    console.info('updateProduct', result);
    if (data.response == 'Success') {
      dispatch(dialogAction.openNotification('Great! product updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
      dispatch(removeSelectedRecord());
      router.push('/product');
    }
    else if (data.response == 'Expired'){
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
      dispatch(auth.logoutUser());
    }
    else{
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
    }
    dispatch(loadingAction.hideLoading());
  }).catch( e => {
    dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
    dispatch(loadingAction.hideLoading());
    console.info('Error', e);
  });
}


export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/

export let loadProductSuccess = (data) => {
  return{
    type: types.GET_PRODUCT_SUCCESS,
    data
  }
}
export let loadCategorySuccess = (data) => {
  return{
    type: types.GET_PRODUCT_CATEGORY_SUCCESS,
    data
  }
}
export let loadSubcategorySuccess = (data) => {
  return{
    type: types.GET_PRODUCT_SUBCATEGORY_SUCCESS,
    data
  }
}

export let viewProductSuccess = (data) => {
  return{
    type:types.VIEW_PRODUCT_SUCCESS,
    data
  }
}


/*
* Failed
*/

export let loadCategoryFailed = (data) => {
  return{
    type: types.GET_PRODUCT_CATEGORY_FAILED,
    data
  }
}
export let loadSubcategoryFailed = (data) => {
  return{
    type: types.GET_PRODUCT_SUBCATEGORY_FAILED,
    data
  }
}
