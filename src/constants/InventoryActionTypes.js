/**
 * Created by jonna on 8/29/18.
 */
export const VIEW_INVENTORY_SUCCESS = 'VIEW_INVENTORY_SUCCESS';
export const GET_INVENTORY_SUCCESS = 'GET_INVENTORY_SUCCESS';

export const CHANGE_FILTER_STATUS = 'CHANGE_FILTER_STATUS';
export const REMOVE_SELECTED_RECORD = 'REMOVE_SELECTED_RECORD';

export const GET_CATEGORY_SUCCESS = 'GET_CATEGORY_SUCCESS';
export const GET_INVENTORY_CATEGORY_SUCCESS = 'GET_INVENTORY_CATEGORY_SUCCESS';
export const GET_INVENTORY_CATEGORY_FAILED = 'GET_INVENTORY_CATEGORY_FAILED';
