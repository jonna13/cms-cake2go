/**
 * Created by jedachas on 3/8/17.
 */
export const VIEW_LEVEL_SUCCESS = 'VIEW_LEVEL_SUCCESS';
export const GET_LEVEL_SUCCESS = 'GET_LEVEL_SUCCESS';
export const GET_LEVELCATEGORY_SUCCESS = 'GET_LEVELCATEGORY_SUCCESS';
export const GET_LEVELCATEGORY_FAILED = 'GET_LEVELCATEGORY_FAILED';

export const CHANGE_FILTER_STATUS = 'CHANGE_FILTER_STATUS';
export const REMOVE_SELECTED_RECORD = 'REMOVE_SELECTED_RECORD';
