/**
 * Created by jonna on 8/30/18.
 */
import * as types from '../constants/BranchActionTypes';
import update from 'react-addons-update';

const initialState = {
  branchs: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    },
    category: [],
    branchCodes: [],

  },
  branchcategorys: [],
  selectedRecord: {}
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_BRANCH_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        branchs: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_BRANCH_SUCCESS:
      console.info('GET_BRANCH_SUCCESS', action.data);
      return update(state, {
        branchs: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

      case types.GET_CATEGORY_SUCCESS:
      console.log('cat data', action.data);
        return update(state, {
          branchs: {
            category: {
              $set: action.data.data
            }
          }
        });

      case types.GET_BRANCHCODE_SUCCESS:
        return update(state, {
          branchCodes: {
            $set: action.data.data
          }
        });

      case types.GET_BRANCHCODE_FAILED:
        return update(state, {
          branchCodes: {
            records: {
              $set: []
            }
          }
        });

      case types.GET_BRANCH_CATEGORY_SUCCESS:
      console.log("GET_BRANCH_CATEGORY_SUCCESS Reducer", action.data.data);
        return update(state, {
          branchcategorys: {
            $set: action.data.data
          }
        });

      case types.GET_BRANCH_CATEGORY_FAILED:
        return update(state, {
          branchcategorys: {
            records: {
              $set: []
            }
          }
        });

    default:
      return state;

  }
}
