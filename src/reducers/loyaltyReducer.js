/**
 * Created by jedachas on 3/9/17.
 */

import * as types from '../constants/LoyaltyActionTypes';
import update from 'react-addons-update';

const initialState = {
  loyalty: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  categorys: [],
  levels: [],
  locations: [],
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_LOYALTY_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        loyalty: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_LOYALTY_SUCCESS:
      return update(state, {
        loyalty: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_LOYALTYLEVEL_SUCCESS:
      return update(state, {
        levels: {
          $set: action.data.data
        }
      });

    case types.GET_LOYALTYLEVEL_FAILED:
      return update(state, {
        levels: {
          $set: []
        }
      });

      case types.GET_LOYALTY_CATEGORY_SUCCESS:
        return update(state, {
          categorys: {
            $set: action.data.data
          }
        });

      case types.GET_LOYALTY_CATEGORY_FAILED:
        return update(state, {
          categorys: {
            records: {
              $set: []
            }
          }
        });

      case types.GET_LOCATION_SUCCESS:
      console.log("GET_LOCATION_SUCCESS", action.data.data);
        return update(state, {
          locations: {
            $set: action.data.data
          }
        });

      case types.GET_LOCATION_FAILED:
        return update(state, {
          locations: {
            records: {
              $set: []
            }
          }
        });

    default:
      return state;

  }
}
