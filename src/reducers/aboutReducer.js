/**
 * Created by jonna on 2/7/18.
 */
import * as types from '../constants/AboutActionTypes';
import update from 'react-addons-update';

const initialState = {
  abouts: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    },
    aboutStatus: [],
  },
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_ABOUT_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        abouts: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_ABOUT_SUCCESS:
      console.info('GET_ABOUT_SUCCESS', action.data);
      return update(state, {
        abouts: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

      case types.GET_ABOUTSTATUS_SUCCESS:
        return update(state, {
          aboutStatus: {
            $set: action.data.data
          }
        });

      case types.GET_ABOUTSTATUS_FAILED:
        return update(state, {
          aboutStatus: {
            records: {
              $set: []
            }
          }
        });

    default:
      return state;

  }
}
