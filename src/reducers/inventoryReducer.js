/**
 * Created by jonna on 8/29/18.
 */
import * as types from '../constants/InventoryActionTypes';
import update from 'react-addons-update';

const initialState = {
  inventorys: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    },
    category: []
  },
  categorys: [],
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_INVENTORY_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        inventorys: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_INVENTORY_SUCCESS:
      return update(state, {
        inventorys: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_CATEGORY_SUCCESS:
    console.log('cat data', action.data);
      return update(state, {
        inventorys: {
          category: {
            $set: action.data.data
          }
        }
      });

      case types.GET_INVENTORY_CATEGORY_SUCCESS:
        return update(state, {
          categorys: {
            $set: action.data.data
          }
        });

      case types.GET_INVENTORY_CATEGORY_FAILED:
        return update(state, {
          categorys: {
            records: {
              $set: []
            }
          }
        });

    default:
      return state;

  }
}
