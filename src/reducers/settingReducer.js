/**
 * Created by jedachas on 3/8/17.
 */
import * as types from '../constants/SettingActionTypes';
import update from 'react-addons-update';

const initialState = {
  settings: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  brands: [],
  brands2: [],
  locations: [],
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_SETTING_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        settings: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_SETTING_SUCCESS:
      return update(state, {
        settings: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_SETTINGBRAND_SUCCESS:
      return update(state, {
        brands: {
          $set: action.data.data
        }
      });

    case types.GET_SETTINGBRAND_FAILED:
      return update(state, {
        brands: {
          records: {
          $set: []
          }
        }
      });

      case types.GET_SETTINGBRAND_SUCCESS2:
        return update(state, {
          brands2: {
            $set: action.data.data
          }
        });

      case types.GET_SETTINGBRAND_FAILED2:
        return update(state, {
          brands2: {
            records: {
            $set: []
            }
          }
        });

      case types.GET_SETTINGLOCATION_SUCCESS:
      console.log("GET_SETTINGLOCATION_SUCCESS Reducer", action.data.data);
        return update(state, {
          locations: {
            $set: action.data.data
          }
        });

      case types.GET_SETTINGLOCATION_FAILED:
        return update(state, {
          locations: {
            records: {
            $set: []
            }
          }
        });

    default:
      return state;

  }
}
