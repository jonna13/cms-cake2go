/**
 * Created by jedachas on 3/8/17.
 */
import * as types from '../constants/LocationCategoryActionTypes';
import update from 'react-addons-update';

const initialState = {
  locationcategorys: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    },
    category: [],
    branchCodes: [],

  },
  selectedRecord: {}
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_LOCATIONCATEGORY_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        locationcategorys: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_LOCATIONCATEGORY_SUCCESS:
      console.info('GET_LOCATIONCATEGORY_SUCCESS', action.data);
      return update(state, {
        locationcategorys: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

      case types.GET_CATEGORY_SUCCESS:
      console.log('cat data', action.data);
        return update(state, {
          locationcategorys: {
            category: {
              $set: action.data.data
            }
          }
        });

      case types.GET_BRANCHCODE_SUCCESS:
        return update(state, {
          branchCodes: {
            $set: action.data.data
          }
        });

      case types.GET_BRANCHCODE_FAILED:
        return update(state, {
          branchCodes: {
            records: {
              $set: []
            }
          }
        });

    default:
      return state;

  }
}
