/**
 * Created by jedachas on 3/1/17.
 *
 * Constant Dialog Reducers
 *
 */
import * as types from '../constants/DialogActionTypes';
import update from 'react-addons-update';

const initialState = {
  alert: {
    open: false,
  },
  confirm: {
    open: false,
    confirmLabel: 'Yes',
    closeLabel: 'No'
  },
  notification: {
    open: false,
    message: ''
  }
};

module.exports = function(state = initialState, action){
  switch (action.type) {
    case types.DIALOG_OPEN_ALERT:
      return update(state, {
        alert: {
          title: { $set: action.title },
          message: { $set: action.message },
          open: { $set: action.open }
        }
      });
      break;

    case types.DIALOG_CLOSE_ALERT:
      return update(state, {
        alert: {
          open: { $set: false }
        }
      });
      break;

    case types.DIALOG_OPEN_CONFIRM:
      return update(state, {
        confirm: {
          open: { $set: action.open },
          title: { $set: action.title },
          message: { $set: action.message },
          confirmLabel: { $set: action.confirmLabel },
          closeLabel: { $set: action.closeLabel },
          onClose: { $set: action.onClose }
        }
      });
      break;

    case types.DIALOG_CLOSE_CONFIRM:
      return update(state, {
        confirm: {
          open: { $set: false }
        }
      });
      break;

    case types.OPEN_NOTIFICATION:
      return update(state, {
        notification: {
          open: { $set: true},
          delay: { $set: action.delay },
          message: { $set: action.message }
        }
      });
      break;

    case types.CLOSE_NOTIFICATION:
      return update(state, {
        notification: {
          open: { $set: false }
        }
      });
    break;

    default:
      return state;
  }
}
