/**
 * Created by jedachas on 2/23/17.
 *
 * Profile Reducer
 *
 */
import * as types from '../constants/ProductActionTypes';
import update from 'react-addons-update';

const initialState = {
  products: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  selectedRecord: {},
  categories: [],
  subcategories: [],
  status: {
    category: '',
    subcategory: ''
  }
};

module.exports = function(state = initialState, action) {

  switch(action.type) {

    case types.VIEW_PRODUCT_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        products: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_PRODUCT_SUCCESS:
      // console.info('Reducer: GET_PRODUCT_SUCCESS', action.data);
      return update(state, {
        products: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        },
        status:{
          record: {
            $set: action.data.response
          }
        }
      });

    case types.GET_PRODUCT_CATEGORY_SUCCESS:
      // console.info('Reducer: GET_PRODUCT_CATEGORY_SUCCESS', action.data);
      return update(state, {
        categories: {
          $set: action.data.data
        },
        status:{
          category: {
            $set: action.data.response
          }
        }
      });

    case types.GET_PRODUCT_SUBCATEGORY_SUCCESS:
      return update(state, {
        subcategories: {
          $set: action.data.data
        },
        status:{
          subcategory: {
            $set: action.data.response
          }
        }
      });

    case types.GET_PRODUCT_CATEGORY_FAILED:
      return update(state, {
        categories: {
          $set: []
        },
        status:{
          category: {
            $set: action.data.response
          }
        }
      });

    case types.GET_PRODUCT_SUBCATEGORY_FAILED:
      return update(state, {
        subcategories: {
          $set: []
        },
        status:{
          subcategory: {
            $set: action.data.response
          }
        }
      });

    default:
      /* Return original state if no actions were consumed. */
      return state;
  }
}
