/**
 * Created by jedachas on 3/8/17.
 */
import * as types from '../constants/LocationActionTypes';
import update from 'react-addons-update';

const initialState = {
  locations: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    },
    category: [],
    branchCodes: [],

  },
  locationcategorys: [],
  selectedRecord: {}
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_LOCATION_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        locations: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_LOCATION_SUCCESS:
      console.info('GET_LOCATION_SUCCESS', action.data);
      return update(state, {
        locations: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

      case types.GET_CATEGORY_SUCCESS:
      console.log('cat data', action.data);
        return update(state, {
          locations: {
            category: {
              $set: action.data.data
            }
          }
        });

      case types.GET_BRANCHCODE_SUCCESS:
        return update(state, {
          branchCodes: {
            $set: action.data.data
          }
        });

      case types.GET_BRANCHCODE_FAILED:
        return update(state, {
          branchCodes: {
            records: {
              $set: []
            }
          }
        });

      case types.GET_LOCATION_CATEGORY_SUCCESS:
      console.log("GET_LOCATION_CATEGORY_SUCCESS Reducer", action.data.data);
        return update(state, {
          locationcategorys: {
            $set: action.data.data
          }
        });

      case types.GET_LOCATION_CATEGORY_FAILED:
        return update(state, {
          locationcategorys: {
            records: {
              $set: []
            }
          }
        });

    default:
      return state;

  }
}
