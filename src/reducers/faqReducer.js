/**
 * Created by jonna on 2/7/18.
 */
import * as types from '../constants/FaqActionTypes';
import update from 'react-addons-update';

const initialState = {
  faqs: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_FAQ_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        faqs: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_FAQ_SUCCESS:
      console.info('GET_FAQ_SUCCESS', action.data);
      return update(state, {
        faqs: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    default:
      return state;

  }
}
