/* Combine all available reducers to a single root reducer.
 *
 * CAUTION: When using the generators, this file is modified in some places.
 *          This is done via AST traversal - Some of your formatting may be lost
 *          in the process - no functionality should be broken though.
 *          This modifications only run once when the generator is invoked - if
 *          you edit them, they are not updated again.
 */
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
/* Populated by react-webpack-redux:reducer */
const reducers = {
  profileState: require('../reducers/profileReducer.js'),
  authReducer: require('../reducers/authReducer.js'),
  dialogState: require('../reducers/dialogReducer.js'),
  loadingState: require('../reducers/loadingReducer.js'),
  productState: require('../reducers/productReducer.js'),
  inventoryState: require('../reducers/inventoryReducer.js'),
  locationcategoryState: require('../reducers/locationCategoryReducer.js'),
  cityState: require('../reducers/cityReducer.js'),
  locationState: require('../reducers/locationReducer.js'),
  branchState: require('../reducers/branchReducer.js'),
  loyaltyState: require('../reducers/loyaltyReducer.js'),
  voucherState: require('../reducers/voucherReducer.js'),
  brandState: require('../reducers/brandReducer.js'),
  postState: require('../reducers/postReducer.js'),
  announcementState: require('../reducers/announcementReducer.js'),
  tabletState: require('../reducers/tabletReducer.js'),
  categoryState: require('../reducers/categoryReducer.js'),
  subcategoryState: require('../reducers/subcategoryReducer.js'),
  settingState: require('../reducers/settingReducer.js'),
  levelState: require('../reducers/levelReducer.js'),
  levelCategoryState: require('../reducers/levelCategoryReducer.js'),
  termState: require('../reducers/termReducer.js'),
  aboutState: require('../reducers/aboutReducer.js'),
  socialmediaState: require('../reducers/socialmediaReducer.js'),
  faqState: require('../reducers/faqReducer.js'),
  skuState: require('../reducers/skuReducer.js'),
  cashierState: require('../reducers/cashierReducer.js'),
  accountState: require('../reducers/accountReducer.js'),
  orderState: require('../reducers/orderReducer.js'),
  routing
};
module.exports = combineReducers(reducers);
