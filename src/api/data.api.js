/*
 Data API
*/
import Config from '../config/base';
import axios from 'axios';
import qs from 'qs';

export let getHeader = () => {
  axios.defaults.headers.common['Authorization'] = `Bearer ${sessionStorage.getItem(Config.MERCHANT_NAME)}`;
  axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
}

export let postUrl = (url, data) => {
  axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
  return axios.post(url, qs.stringify(data));
}

export let post = (data) => {
  getHeader();
  return axios.post(Config.PROJECT_PATH + Config.GATEWAY_URL, qs.stringify(data));
};

export let get = (data) => {
  getHeader();
  return axios.get(Config.PROJECT_PATH + Config.GATEWAY_URL + data);
};

export let all = (funcs = []) => {
  getHeader();
  return axios.all(funcs);
}

export let upload = (data) => {
  console.log('upload data', data);

  let formData = new FormData();
  // if(data.image.file.length > 0){
  //   data.image.file.forEach((val, key) => {
  //     formData.append('file'+key, val);
  //   })
  // }
  let i;
  let images = data.image.file;
  for (i=0; i < images.length; i++){
    console.log('sample', images[i]);
    formData.append('file'+i, images[i]);

  }
  // formData.append('file', data.image.file[0]);
  // formData.append('file1', data.image.file[1]);
  formData.append('module', data.module);
  axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
  return axios.post(Config.PROJECT_PATH + Config.UPLOAD_URL, formData);
};
