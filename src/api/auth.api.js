import Config from '../config/base';
export default class SessionApi {

  static login(credentials){
    const request = new Request(Config.GATEWAY_URL, {
      method: 'POST',
      body: JSON.stringify({
        username: credentials.username,
        password: credentials.password,
        function: 'login',
        category: 'dashboard' })
    });

    return fetch(request).then(r => r.json())
    .then(data => { return data; })
    .catch(e => { return e; });

  }

}
