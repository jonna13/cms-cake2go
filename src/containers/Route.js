import React from 'react';
import { Route, IndexRoute } from 'react-router';

import Main from '../components/Main';
import App from './App';
import Login from './Login';
import Push from './Push';
import Product from './Product';
import Category from './Category';
import Subcategory from './Subcategory';
import Location from './Location';
import LocationCategory from './LocationCategory';
import Loyalty from './Loyalty';
import Voucher from './Voucher';
import Brand from './Brand';
import Post from './Post';
import Tablet from './Tablet';
import Setting from './Setting';
import Level from './Level';
import LevelCategory from './LevelCategory';
import Term from './Term';
import About from './About';
import SocialMedia from './SocialMedia';
import Faq from './Faq';
import Sku from './Sku';
import Cashier from './Cashier';
import Account from './Account';
import Order from './Order';
import OrderArchive from './OrderArchive';
import Announcement from './Announcement';
import Inventory from './Inventory';
import City from './City';

import ProductEditor from '../components/product/ProductEditor';
import CategoryEditor from '../components/category/CategoryEditor';
import SubcategoryEditor from '../components/subcategory/SubcategoryEditor';
import LocationCategoryEditor from '../components/locationcategory/LocationCategoryEditor';
import LocationEditor from '../components/location/LocationEditor';
import LoyaltyEditor from '../components/loyalty/LoyaltyEditor';
import VoucherEditor from '../components/voucher/VoucherEditor';
import BrandEditor from '../components/brand/BrandEditor';
import PostEditor from '../components/post/PostEditor';
import TabletEditor from '../components/tablet/TabletEditor';
import SettingEditor from '../components/setting/SettingEditor';
import ProfileEditor from '../components/profile/ProfileEditor';
import CredentialEditor from '../components/auth/CredentialEditor';
import LevelEditor from '../components/level/LevelEditor';
import LevelCategoryEditor from '../components/levelcategory/LevelCategoryEditor';
import TermEditor from '../components/term/TermEditor';
import AboutEditor from '../components/about/AboutEditor';
import SocialMediaEditor from '../components/socialmedia/SocialMediaEditor';
import FaqEditor from '../components/faq/FaqEditor';
import SkuEditor from '../components/sku/SkuEditor';
import CashierEditor from '../components/cashier/CashierEditor';
import AccountEditor from '../components/account/AccountEditor';
import OrderEditor from '../components/orders/OrderEditor';
import OrderArchiveEditor from '../components/orderarchive/OrderArchiveEditor';
import AnnouncementEditor from '../components/announcement/AnnouncementEditor';
import InventoryEditor from '../components/inventory/InventoryEditor';
import CityEditor from '../components/city/CityEditor';

import Config from '../config/base';

const requireAuth = (nextState, replace) => {
  if (!sessionStorage.getItem(Config.MERCHANT_NAME)) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    });
  }
}


module.exports = (
    <div>
      <Route path="/" component={Main} > {/* Holds the Navigation links */}
        {/*<IndexRoute component={App} onEnter={requireAuth}/> */}{/* Landing Page w/c is Profile Page */}
        <IndexRoute component={App} onEnter={requireAuth}/> {/* Landing Page w/c is Profile Page */}
        <Route path="/profile_edit" component={ProfileEditor} onEnter={requireAuth} />
        <Route path="/credential" component={CredentialEditor} onEnter={requireAuth} />
        <Route path="/level" component={Level} onEnter={requireAuth} />
        <Route path="/level/:id" component={LevelEditor} onEnter={requireAuth}/>
        <Route path="/level_add" component={LevelEditor} onEnter={requireAuth}/>
        <Route path="/levelcategory" component={LevelCategory} onEnter={requireAuth} />
        <Route path="/levelcategory/:id" component={LevelCategoryEditor} onEnter={requireAuth}/>
        <Route path="/levelcategory_add" component={LevelCategoryEditor} onEnter={requireAuth}/>
        <Route path="/term" component={Term} onEnter={requireAuth} />
        <Route path="/term/:id" component={TermEditor} onEnter={requireAuth}/>
        <Route path="/term_add" component={TermEditor} onEnter={requireAuth}/>
        <Route path="/about" component={About} onEnter={requireAuth} />
        <Route path="/about/:id" component={AboutEditor} onEnter={requireAuth}/>
        <Route path="/about_add" component={AboutEditor} onEnter={requireAuth}/>
        <Route path="/socialmedia" component={SocialMedia} onEnter={requireAuth} />
        <Route path="/socialmedia/:id" component={SocialMediaEditor} onEnter={requireAuth}/>
        <Route path="/socialmedia_add" component={SocialMediaEditor} onEnter={requireAuth}/>
        <Route path="/faq" component={Faq} onEnter={requireAuth} />
        <Route path="/faq/:id" component={FaqEditor} onEnter={requireAuth}/>
        <Route path="/faq_add" component={FaqEditor} onEnter={requireAuth}/>
        <Route path="/sku" component={Sku} onEnter={requireAuth} />
        <Route path="/sku/:id" component={SkuEditor} onEnter={requireAuth}/>
        <Route path="/sku_add" component={SkuEditor} onEnter={requireAuth}/>
        <Route path="/setting" component={Setting} onEnter={requireAuth} />
        <Route path="/setting/:id" component={SettingEditor} onEnter={requireAuth}/>
        <Route path="/setting_add" component={SettingEditor} onEnter={requireAuth}/>
        <Route path="/push" component={Push} onEnter={requireAuth}/>
        <Route path="/product" component={Product} onEnter={requireAuth} />
        <Route path="/product/:id" component={ProductEditor} onEnter={requireAuth}/>
        <Route path="/product_add" component={ProductEditor} onEnter={requireAuth}/>
        <Route path="/category" component={Category} onEnter={requireAuth} />
        <Route path="/category/:id" component={CategoryEditor} onEnter={requireAuth} />
        <Route path="/category_add" component={CategoryEditor} onEnter={requireAuth} />
        <Route path="/subcategory" component={Subcategory} onEnter={requireAuth} />
        <Route path="/subcategory/:id" component={SubcategoryEditor} onEnter={requireAuth} />
        <Route path="/subcategory_add" component={SubcategoryEditor} onEnter={requireAuth} />
        <Route path="/locationcategory" component={LocationCategory} onEnter={requireAuth} />
        <Route path="/locationcategory/:id" component={LocationCategoryEditor} onEnter={requireAuth} />
        <Route path="/locationcategory_add" component={LocationCategoryEditor} onEnter={requireAuth} />
        <Route path="/branch" component={Location} onEnter={requireAuth} />
        <Route path="/branch/:id" component={LocationEditor} onEnter={requireAuth} />
        <Route path="/branch_add" component={LocationEditor} onEnter={requireAuth} />
        <Route path="/loyalty" component={Loyalty} onEnter={requireAuth}/>
        <Route path="/loyalty/:id" component={LoyaltyEditor} onEnter={requireAuth}/>
        <Route path="/loyalty_add" component={LoyaltyEditor} onEnter={requireAuth}/>
        <Route path="/voucher" component={Voucher} onEnter={requireAuth}/>
        <Route path="/voucher/:id" component={VoucherEditor} onEnter={requireAuth}/>
        <Route path="/voucher_add" component={VoucherEditor} onEnter={requireAuth}/>
        <Route path="/brand" component={Brand} onEnter={requireAuth}/>
        <Route path="/brand/:id" component={BrandEditor} onEnter={requireAuth}/>
        <Route path="/brand_add" component={BrandEditor} onEnter={requireAuth}/>
        <Route path="/post" component={Post} onEnter={requireAuth}/>
        <Route path="/post/:id" component={PostEditor} onEnter={requireAuth}/>
        <Route path="/post_add" component={PostEditor} onEnter={requireAuth}/>
        <Route path="/tablet" component={Tablet} onEnter={requireAuth}/>
        <Route path="/tablet/:id" component={TabletEditor} onEnter={requireAuth}/>
        <Route path="/tablet_add" component={TabletEditor} onEnter={requireAuth}/>
        <Route path="/cashier" component={Cashier} onEnter={requireAuth}/>
        <Route path="/cashier/:id" component={CashierEditor} onEnter={requireAuth}/>
        <Route path="/cashier_add" component={CashierEditor} onEnter={requireAuth}/>
        <Route path="/account" component={Account} onEnter={requireAuth}/>
        <Route path="/account/:id" component={AccountEditor} onEnter={requireAuth}/>
        <Route path="/account_add" component={AccountEditor} onEnter={requireAuth}/>
        <Route path="/orders" component={Order} onEnter={requireAuth}/>
        <Route path="/orders/:transactionID" component={OrderEditor} onEnter={requireAuth}/>
        <Route path="/orders_add" component={OrderEditor} onEnter={requireAuth}/>
        <Route path="/orderarchive" component={OrderArchive} onEnter={requireAuth}/>
        <Route path="/orderarchive/:id" component={OrderArchiveEditor} onEnter={requireAuth}/>
        <Route path="/orderarchive_add" component={OrderArchiveEditor} onEnter={requireAuth}/>
        <Route path="/announcement" component={Announcement} onEnter={requireAuth}/>
        <Route path="/announcement/:id" component={AnnouncementEditor} onEnter={requireAuth}/>
        <Route path="/announcement_add" component={AnnouncementEditor} onEnter={requireAuth}/>
        <Route path="/inventory" component={Inventory} onEnter={requireAuth}/>
        <Route path="/inventory/:id" component={InventoryEditor} onEnter={requireAuth}/>
        <Route path="/inventory_add" component={InventoryEditor} onEnter={requireAuth}/>
        <Route path="/city" component={City} onEnter={requireAuth}/>
        <Route path="/city/:cityID" component={CityEditor} onEnter={requireAuth}/>
        <Route path="/city_add" component={CityEditor} onEnter={requireAuth}/>
      </Route>
      <Route path="/login" component={Login} />
    </div>
);
