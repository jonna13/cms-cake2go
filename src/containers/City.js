import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/city/CityComponent';

class City extends Component {
  render() {
    const {actions, cityState, router } = this.props;
    return <Main actions={actions} data={cityState} router={router}/>;
  }
}

City.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { cityState: state.cityState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cityAction: require('../actions/cityAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.cityAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(City);
