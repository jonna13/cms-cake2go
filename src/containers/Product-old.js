import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/product/ProductComponent';

class Product extends Component {
  render() {
    const { actions, productState, router } = this.props;
    return <Main actions={actions} data={productState} router={router}/>;
  }
}

Product.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { productState: state.productState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    productAction: require('../actions/productAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.productAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);
