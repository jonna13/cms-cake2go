import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/socialmedia/SocialMediaComponent';

class SocialMedia extends Component {
  render() {
    const {actions, socialmediaState, router } = this.props;
    return <Main actions={actions} data={socialmediaState} router={router}/>;
  }
}

SocialMedia.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { socialmediaState: state.socialmediaState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    socialmediaAction: require('../actions/socialmediaAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.socialmediaAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(SocialMedia);
