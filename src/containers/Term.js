import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/term/TermComponent';

class Term extends Component {
  render() {
    const {actions, termState, router } = this.props;
    return <Main actions={actions} data={termState} router={router}/>;
  }
}

Term.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { termState: state.termState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    termAction: require('../actions/termAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.termAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Term);
