import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/orders/OrderComponent';

class Order extends Component {
  render() {
    const {actions, orderState, router} = this.props;
    return <Main actions={actions} data={orderState} router={router}/>;
  }
}

Order.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { orderState: state.orderState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { orderAction : require('../actions/orderAction.js') };
  const actionMap = { actions: bindActionCreators(actions.orderAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);
