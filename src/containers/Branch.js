import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/branch/BranchComponent';

class Branch extends Component {
  render() {
    const {actions, branchState, router } = this.props;
    return <Main actions={actions} data={branchState} router={router}/>;
  }
}

Branch.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { branchState: state.branchState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    branchAction: require('../actions/branchAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.branchAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Branch);
