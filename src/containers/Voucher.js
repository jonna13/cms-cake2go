import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/voucher/VoucherComponent';

class Voucher extends Component {
  render() {
    const {actions, voucherState, router} = this.props;
    return <Main actions={actions} data={voucherState} router={router}/>;
  }
}

Voucher.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { voucherState: state.voucherState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    voucherAction: require('../actions/voucherAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.voucherAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Voucher);
