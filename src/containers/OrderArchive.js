import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/orderarchive/OrderArchiveComponent';

class OrderArchive extends Component {
  render() {
    const {actions, categoryState, router} = this.props;
    return <Main actions={actions} data={categoryState} router={router}/>;
  }
}

OrderArchive.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { categoryState: state.categoryState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { categoryAction : require('../actions/categoryAction.js') };
  const actionMap = { actions: bindActionCreators(actions.categoryAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderArchive);
