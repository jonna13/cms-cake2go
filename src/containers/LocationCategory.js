import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/locationcategory/LocationCategoryComponent';

class LocationCategory extends Component {
  render() {
    const {actions, locationcategoryState, router } = this.props;
    return <Main actions={actions} data={locationcategoryState} router={router}/>;
  }
}

LocationCategory.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { locationcategoryState: state.locationcategoryState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    locationCategoryAction: require('../actions/locationCategoryAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.locationCategoryAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationCategory);
