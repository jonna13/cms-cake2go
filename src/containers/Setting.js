import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/setting/SettingComponent';

class Setting extends Component {
  render() {
    const {actions, settingState, router} = this.props;
    return <Main actions={actions} data={settingState} router={router}/>;
  }
}

Setting.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { settingState: state.settingState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { settingAction: require('../actions/settingAction.js') };
  const actionMap = { actions: bindActionCreators(actions.settingAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Setting);
