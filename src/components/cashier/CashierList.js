/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';


import GridRow from './CashierGridRow';

class CashierList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getCashiers(params);
  }

  render(){
    let {cashiers} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'ID', value: 'cashierID'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (cashiers.records.length > 0) {
      cashiers.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.CASHIER}/>);
      });
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.cashiers}
            onGetData={this.handleGetData}
            imageModule={Config.IMAGE.CASHIER}
            hasImage={true}
            imageKey='image'
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.cashiers}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default CashierList;
