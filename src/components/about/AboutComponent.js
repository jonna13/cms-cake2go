'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import AboutList from './AboutList';

class AboutComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/about_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/about/'+item.aboutID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.abouts;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.abouts;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.abouts;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.abouts;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.abouts;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getAbouts(params);
  }

  render() {
    const {pageinfo} = this.props.data.abouts;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>About Us</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <div className={( pageinfo.totalRecord != 1 ) ? 'show' : 'hide'}>
          <AddButton
            handleOpen={this.handleAdd}/>
        </div>
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.abouts.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.abouts.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.abouts.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

        { /* List */ }
        <AboutList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

AboutComponent.displayName = 'AboutAboutComponent';

// Uncomment properties you need
// AboutComponent.propTypes = {};
// AboutComponent.defaultProps = {};

export default AboutComponent;
