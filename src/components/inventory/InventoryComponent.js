'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton, ViewButton, InactiveButton, ActiveButton, ShowAllButton, ShowAllProductButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import Search from 'material-ui/svg-icons/action/search';


import InventoryList from './InventoryList';

class InventoryComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/inventory_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/inventory/'+item.prodID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.inventorys;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getInventorys(params);
  }

  render() {
    const {pageinfo} = this.props.data.inventorys;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Inventory</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.inventorys.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.inventorys.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.inventorys.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

        <Card className="order-card">
          <Form>
            <Row>
              <Col md={12} className="inline-block">
                <ShowAllProductButton
                  handleOpen={this.handleAdd}/>&nbsp;&nbsp;

                <Label className="label-dft strong-6 title-gotham">Inventory by Branch: &nbsp;</Label>

                <FormGroup>
                  <Input type="select" name="select" id="exampleSelect" className="fullwidth">
                    <option>- List of Branches -</option>
                  </Input>
                </FormGroup>


                <Label className="blue-icon pull-right-responsive search"><Search className='nav-icon'/></Label>
                <Input type="text" name="search" placeholder="" className="search-prod"/>

              </Col>
            </Row>
          </Form>
        </Card>

        { /* List */ }
        <InventoryList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default InventoryComponent;
