/**
 * Created by jonna on 8/29/18.
 */
import React from 'react';
import Config from '../../config/base';
import TextField from 'material-ui/TextField';
import { DefaultTextArea } from '../common/fields';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';


class InventoryForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      categoryID: '',
      categoryName: ''
    };
  }

  componentWillMount(){
    const {actions} = this.props;
    // console.log('actions', actions);
    actions.getCategory();
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        categoryID: nextProps.data.selectedRecord.categoryID,
        categoryName: nextProps.data.selectedRecord.categoryName,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleCategoryChange = (e, idx, categoryID) => {
    const {actions} = this.props;
    let name = this.props.data.categorys.filter(x=> { return x.categoryID == categoryID})[0].name;
    this.setState({categoryID});
    this.props.onCategoryChange(categoryID, name);
  };

  handleBrandChange = (e, idx, brandID) => {
    this.setState({brandID});
    this.props.onChangeBrand(brandID);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    let categoryList = [];
    const {data} = this.props;
    {/*console.log('inventory data', data);*/}

    {/*var category = [];
        if (data.inventorys.category.length > 0) {
          data.inventorys.category.forEach((val, key) => {
            category.push(
              <MenuItem
              key={key}
              value={val.brandID}
              primaryText={val.name}
              />
            );
          });
        }*/}
    console.log("Inventory Data", data);

    if (data.categorys) {
      categoryList.push(<MenuItem key={0} value='' primaryText='Select Category' />);
      data.categorys.map( (val, idx) => {
        categoryList.push(<MenuItem key={(idx+1)} value={val.categoryID} primaryText={val.name} />);
      });
    }

    if (this.state.shouldDisplay){

      return(
        <Grid fluid>
          <Row>
            <div>
              <Col className="modal-container">
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Inventory Name : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Inventory Code : </Label>
                </Col>
                <Col md={4}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={2}>
                  <Label className="label-dft strong-6 title-gotham pull-right-responsive">Status : </Label>
                </Col>
                <Col md={3}>
                  <FormGroup>
                    <Input type="select" name="status" id="status">
                      <option> Active </option>
                      <option> Inactive </option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Category Name : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Input type="select" name="status" id="status">
                      <option> Cakes 2 Comfort </option>
                      <option> Cake 2 Taste </option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Available on Branches : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label hidden></Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Input type="select" name="status" id="status" className="half">
                      <option> - Select Branch - </option>
                      <option> Pasay </option>
                      <option> Quezon City </option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={12}>
                  <Col md={3} style= {{ paddingLeft: '0px' }}>
                    <Label className="label-dft strong-6 title-gotham">Tag : </Label>
                  </Col>
                  <Col md={4}>
                      <Checkbox
                        className="chkbx-label"
                        label="New"/>
                      <Checkbox
                        className="chkbx-label"
                        label="Not Available"/>
                  </Col>
                  <Col md={5}>
                      <Checkbox
                        className="chkbx-label"
                        label="Bestseller"/>
                  </Col>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Price : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Input type="number" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={12}>
                  <Col md={3} style= {{ paddingLeft: '0px' }}>
                    <Label className="label-dft strong-6 title-gotham">Add Ons : </Label>
                  </Col>
                  <Col md={4}>
                      <Checkbox
                        className="chkbx-label"
                        label="Gift Card"/>
                      <Checkbox
                        className="chkbx-label"
                        label="Customize Dedication"/>
                  </Col>
                  <Col md={5}>
                      <Checkbox
                        className="chkbx-label"
                        label="Buy Candle"/>
                      <Checkbox
                        className="chkbx-label"
                        label="Happy Birthday Candle"/>
                  </Col>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Description </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="textarea" name="description" id="description" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth big-textarea"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Image </Label>
                </Col>
                <Col md={9}>
                  <div className='item-image-box'>
                    <ImageUpload image={null}
                      onImageChange={this.props.onImageChange}
                      image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                      imageModule={this.props.imageModule}
                      info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                  </div>
                </Col>
              </Col>
            </div>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default InventoryForm;
