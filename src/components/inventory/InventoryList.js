/**
 * Created by jonna on 8/29/18.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './InventoryGridRow';

class InventoryList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getInventorys(params);
  }

  render(){
    let {inventorys} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Product Code', value: 'productCode'},
      { title: 'Product Name', value: 'productName'},
      { title: 'In Stock', value: 'quantity'}
    ];

    var rows = [];
    if (inventorys.records.length > 0) {
      inventorys.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.INVENTORY}/>);
      });
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.inventorys}
            onGetData={this.handleGetData}
            imageModule={Config.IMAGE.INVENTORY}
            hasImage={true}
            imageKey='image'
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.inventorys}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default InventoryList;
