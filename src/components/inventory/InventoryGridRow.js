import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Checkbox from 'material-ui/Checkbox';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import AppStyles from '../../styles/Style';
import {ProperCase} from '../common/Utility';

class InventoryGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
          </IconButton>
        </td>
        <td className="pointer" onClick={()=> { this.props.onEditClick(item); }}>
          {item.productCode}
        </td>
        <td className="pointer" onClick={()=> { this.props.onEditClick(item); }}>
          {item.productName}
        </td>
        <td className="pointer" onClick={()=> { this.props.onEditClick(item); }}>
          {item.quantity}
        </td>
      </tr>
    );
  }
}

export default InventoryGridRow;
