/**
 * Created by jonna on 8/29/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap'
import Config from '../../config/base';
import _ from 'lodash';

import InventoryForm from './InventoryForm';

class InventoryEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      inventory: {
        productCode: '',
        productName: '',
        quantity: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.INVENTORY,
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewInventory(this.props.params.id);
    }
    actions.getCategorys();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.inventoryState.selectedRecord)) {
        this.setState({
          inventory: nextProps.inventoryState.selectedRecord
        });
      }
    }
  }

  // handle going back to Inventory
  handleReturn = () => {
    this.props.router.push('/inventory');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.inventory)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_INVENTORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateInventory(this.state.inventory, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.inventory)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_INVENTORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addInventory(this.state.inventory, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.description == '') {
      dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.categoryID == '') {
      dialogActions.openNotification('Oops! Please select category', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.url == '') {
      dialogActions.openNotification('Oops! No url found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    let newDescription = data.description.replace(/<br\/>/g, "\n");
    this.state.inventory['description'] = newDescription;

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let inventory = this.state.inventory;
    inventory[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.inventory['status'] = e;
  }

  handleChangeBrand = (e) => {
    this.state.inventory['brandID'] = e;
  }

  // handle category
  handleCategoryChange = (e, name) => {
    this.state.inventory['categoryID'] = e;
    this.state.inventory['categoryName'] = name;
  }

  // close Modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    const {actions} = this.props;
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Inventory' : 'Add Inventory'}</h2>
        <Divider className='content-divider2' />

        { /* Action Buttons */ }
        {/*<ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }*/}

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? 'Edit Inventory' : 'Add Inventory'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <InventoryForm
            data={this.props.inventoryState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            actions={actions}
            onCategoryChange={this.handleCategoryChange}
            onChangeBrand={this.handleChangeBrand}
            onStatusChange={this.handleStatusChange}
            onImageChange={this.handleImageChange}
            imageModule={this.state.imageModule}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <CreateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    inventoryState: state.inventoryState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    inventoryAction: require('../../actions/inventoryAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.inventoryAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(InventoryEditor);
