import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import AppStyles from '../../styles/Style';

class GridRowComponent extends Component {
  render(){
    let item = this.props.items;
    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <EditIcon />
          </IconButton>
        </td>
        <td>{item.deviceCode}</td>
        <td>{item.deviceID}</td>
        <td>{item.locName}</td>
        <td>{item.locID}</td>
        <td>
          <label className={ (item.status == 'active' ) ? 'label-active' : 'label-inactive'}>{item.status}</label>
        </td>
        <td>
          <label className={ (item.deploy == 'true' ) ? 'label-active' : 'label-inactive'}>{item.deploy}</label>
        </td>
      </tr>
    );
  }
}

export default GridRowComponent;
