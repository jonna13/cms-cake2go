'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import SettingList from './SettingList';

class SettingComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/setting_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/setting/'+item.settingID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.settings;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.settings;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.settings;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.settings;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.settings;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getSettings(params);
  }

  render() {
    const {pageinfo} = this.props.data.settings;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Earn Settings</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.settings.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.settings.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.settings.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <SettingList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default SettingComponent;
