'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import LoyaltyList from './LoyaltyList';

class LoyaltyComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/loyalty_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/loyalty/'+item.loyaltyID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.loyalty;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.loyalty;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.loyalty;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.loyalty;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.loyalty;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getLoyalty(params);
  }

  render() {
    const {pageinfo} = this.props.data.loyalty;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Reward</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.loyalty.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.loyalty.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.loyalty.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <LoyaltyList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit}
          router={this.props.router} />
      </div>
    );
  }

}

LoyaltyComponent.displayName = 'LoyaltyLoyaltyComponent';

// Uncomment properties you need
// LoyaltyComponent.propTypes = {};
// LoyaltyComponent.defaultProps = {};

export default LoyaltyComponent;
