/**
 * Created by jonna on 2/12/18.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';

class LoyaltyForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      categoryID: '',
      categoryName: '',
      locID: '',
      locName: ''
    };
  }

  componentWillMount(){
    const {actions, data} = this.props;
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        levelID: nextProps.data.selectedRecord.levelID,
        categoryID: nextProps.data.selectedRecord.categoryID,
        categoryName: nextProps.data.selectedRecord.categoryName,
        locID: nextProps.data.selectedRecord.locID,
        locName: nextProps.data.selectedRecord.locName,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  handleCategoryChange = (e, idx, categoryID) => {
    const {actions} = this.props;
    let name = this.props.data.categorys.filter(x=> { return x.categoryID == categoryID})[0].name;
    this.setState({categoryID});
    this.props.onCategoryChange(categoryID, name);
  };

  handleLocationChange = (e, idx, locID) => {
    const {actions} = this.props;
    let name = this.props.data.locations.filter(x=> { return x.locID == locID})[0].name;
    this.setState({locID});
    this.props.onLocationChange(locID, name);
  };

  handleLevelChange = (e, idx, levelID) => {
    this.setState({levelID});
    this.props.onLevelChange(levelID);
  };

  handleTypeChange = (e, idx, promoType) => {
    this.setState({promoType});
    this.props.onTypeChange(promoType);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    let levelList = [];
    let categoryList = [];
    let locationList = [];
    const {data} = this.props;

    if (data.levels) {
      levelList.push(<MenuItem key={0} value='' primaryText='Select Level' />);
      data.levels.map( (val, idx) => {
        levelList.push(<MenuItem key={(idx+1)} value={val.levelID} primaryText={val.name} />);
      });
    }

    if (data.categorys) {
      categoryList.push(<MenuItem key={0} value='' primaryText='Select Category' />);
      data.categorys.map( (val, idx) => {
        categoryList.push(<MenuItem key={(idx+1)} value={val.categoryID} primaryText={val.name} />);
      });
    }

    if (data.locations) {
      locationList.push(<MenuItem key={0} value='' primaryText='Select Location' />);
      data.locations.map( (val, idx) => {
        locationList.push(<MenuItem key={(idx+1)} value={val.locID} primaryText={val.name} />);
      });
    }
    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <div className='content-container'>
                  <div className='item-image-box'>
                    <ImageUpload image={null}
                      onImageChange={this.props.onImageChange}
                      image={(data.selectedRecord.image) ? data.selectedRecord.image: '' }
                      imageModule={this.props.imageModule}
                    info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                  </div>
                </div>
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="points"
                  type="number"
                  min="0"
                  hintText="Enter Points"
                  floatingLabelText="Points"
                  defaultValue={(data.selectedRecord.points) ? data.selectedRecord.points : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                {/*<SelectField value={this.state.promoType} onChange={this.handleTypeChange} floatingLabelText="Type" hintText="Select Type" floatingLabelFixed={true}>
                  <MenuItem value="type1" primaryText="Type 1" />
                  <MenuItem value="type2" primaryText="Type 2" />
                </SelectField><br />*/}
                <SelectField value={(data.selectedRecord.promoType) ? data.selectedRecord.promoType : this.state.promoType} onChange={this.handleTypeChange} floatingLabelText="Type" hintText="Select Type" floatingLabelFixed={true}>
                  <MenuItem value="regular" primaryText="Regular" />
                  <MenuItem value="premium" primaryText="Premium" />
                </SelectField><br />

              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <DropDownMenu
                  value={this.state.locID}
                  onChange={this.handleLocationChange}
                  autoWidth={true}
                className='dropdownButton-fixed-300'>
                  {locationList}
                </DropDownMenu>
                {/*<DropDownMenu
                  name="categoryID"
                  value={this.state.categoryID}
                  onChange={this.handleCategoryChange}
                  autoWidth={true}
                  openImmediately={(!this.props.shouldEdit) ? true: false}
                  className='dropdownButton-fixed-300'>
                  {categoryList}
                </DropDownMenu>*/}
                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                /><br />
                <DefaultTextArea
                  name="terms"
                  hintText="Enter Terms"
                  floatingLabelText="Terms"
                  defaultValue={(data.selectedRecord.terms) ? data.selectedRecord.terms : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default LoyaltyForm;
