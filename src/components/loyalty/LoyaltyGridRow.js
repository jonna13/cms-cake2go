import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import AppStyles from '../../styles/Style';
import {ProperCase} from '../common/Utility';

class GridRowComponent extends Component {
  render(){
    let item = this.props.items;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <EditIcon />
          </IconButton>
        </td>
        <td>
          {item.name}
        </td>
        <td>{ProperCase(item.promoType)}</td>
        <td>{item.locName}</td>
        <td>{item.points}</td>
        <td>
          <label className={ (item.status == 'active' ) ? 'label-active' : 'label-inactive'}>{item.status}</label>
        </td>
      </tr>
    );
  }
}

export default GridRowComponent;
