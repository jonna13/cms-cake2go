/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';

import PostForm from './PostForm';

class PostEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      post: {
        title: '',
        description: '',
        url: '',
        image: '',
        status: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.POST,
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewPost(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.postState.selectedRecord)) {
        this.setState({
          post: nextProps.postState.selectedRecord
        });
      }
    }
  }

  // handle going back to Post
  handleReturn = () => {
    this.props.router.push('/post');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.post)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_POST_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updatePost(this.state.post, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.post)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_POST_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addPost(this.state.post, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (this.state.post.image == '') {
      if (_.isEmpty(this.state.uploadImage)) {
        dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
    }
    if (data.title == '') {
      dialogActions.openNotification('Oops! No title found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.description == '') {
      dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.url == '') {
      dialogActions.openNotification('Oops! No url found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    let newDescription = data.description.replace(/<br\/>/g, "\n");
    this.state.post['description'] = newDescription;

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let post = this.state.post;
    post[field] = e.target.value;
  }

  // handle change in type
  handleTypeChange = (e) => {
    this.state.post['type'] = e;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.post['status'] = e;
  }

  //close modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? "Edit What's New" :"Create What's New"}</h2>
        <Divider className='content-divider2' />

        { /* Action Buttons */ }
        {/*<ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }*/}

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? "Edit What's New" : "Create What's New"}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          className="dialog-title"
        >
          <PostForm
            data={this.props.postState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            onTypeChange={this.handleTypeChange}
            onStatusChange={this.handleStatusChange}
            onImageChange={this.handleImageChange}
            imageModule={this.state.imageModule}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <CreateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    postState: state.postState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    postAction: require('../../actions/postAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.postAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(PostEditor);
