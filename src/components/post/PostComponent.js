'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton, ViewButton, InactiveButton, ActiveButton, ShowAllButton} from '../common/buttons';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';
import Clear from 'material-ui/svg-icons/content/clear';

import PostList from './PostList';

class PostComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/post_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/post/'+item.postID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.posts;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.posts;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.posts;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.posts;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.posts;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getPosts(params);
  }

  render() {
    const {pageinfo} = this.props.data.posts;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>What&#39;s New</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.posts.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.posts.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.posts.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

        <Card className="order-card">
          <Form>
            <Row>
              <Col md={12} className="inline-block">
                <AddButton
                  handleOpen={this.handleAdd}/>&nbsp;&nbsp;
                <ShowAllButton
                  />&nbsp;&nbsp;
                <ActiveButton
                  />&nbsp;&nbsp;
                <InactiveButton
                  />
              </Col>
            </Row>
          </Form>
        </Card>

        { /* List  */ }
        <PostList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default PostComponent;
