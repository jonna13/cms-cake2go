/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './VoucherGridRow';

class VoucherList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getVouchers(params);
  }

  render(){
    let {vouchers} = this.props.data;
    const headers = [
      { title: '', value: 'dateAdded'},
      { title: 'Name', value: 'name'},
      { title: 'Voucher ID', value: 'voucherID'},
      { title: 'Type', value: 'type'},
      { title: 'Redemption Limit', value: 'redemptionlimit'},
      { title: 'Start Date', value: 'startDate'},
      { title: 'End Date', value: 'endDate'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (vouchers.records.length > 0) {
      vouchers.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.VOUCHER}/>);
      });
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.vouchers}
            onGetData={this.handleGetData}
            imageModule={Config.IMAGE.VOUCHER}
            hasImage={true}
            imageKey='image'
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.vouchers}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default VoucherList;
