/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import TextField from 'material-ui/TextField';
import { DefaultTextArea } from '../common/fields';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';


class VoucherForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      type: '',
      shouldDisplay: false,
      startDate: null,
      endDate: null,
      hasDate: ['flash', 'wemissyou'],
      wmyperiod: '',
      action: ''
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        if (nextProps.data.selectedRecord.type == 'birthday'){
          let date = new Date(nextProps.data.selectedRecord.startDate);
          this.setState({bday:date.getMonth()});
        }
        this.setState({
          type: nextProps.data.selectedRecord.type,
          shouldDisplay: true,
          startDate: new Date(nextProps.data.selectedRecord.startDate),
          endDate: new Date(nextProps.data.selectedRecord.endDate),
        });
      }
    }
  }

  handleTypeChange = (value, idx, type) => {
    this.setState({type});
    this.props.onTypeChange(type);
  }

  handleCardTypeChange = (e, idx, cardType) => {
    this.setState({cardType});
    this.props.onCardTypeChange(cardType);
  }

  handleCardVersionChange = (e, idx, cardVersion) => {
    this.setState({cardVersion});
    this.props.onCardVersionChange(cardVersion);
  }

  handleFrequencyTypeChange = (e, idx, frequencyType) => {
    this.setState({frequencyType});
    this.props.onFrequencyTypeChange(frequencyType);
  }

  handleMonthChange = (e, idx, month) => {
    this.setState({month});
    this.props.onMonthChange(month);
  }

  handleFrequencyStartChange = (e, idx, frequencyStart) => {
    this.setState({frequencyStart});
    this.props.onFrequencyStartChange(frequencyStart);
  }

  handleFrequencyEndChange = (e, idx, frequencyEnd) => {
    this.setState({frequencyEnd});
    this.props.onFrequencyEndChange(frequencyEnd);
  }

  handleTimeStartChange = (e, idx, startTime) => {
    this.setState({startTime});
    this.props.onTimeStartChange(startTime);
  }

  handleTimeEndChange = (e, idx, endTime) => {
    this.setState({endTime});
    this.props.onTimeEndChange(endTime);
  }

  handleParameterStringChange = (e, idx, parameterString) => {
    this.setState({parameterString});
    this.props.onParameterStringChange(parameterString);
  }

  handleBirthdayChange = (e, idx, bday) => {
    this.setState({bday});
    this.props.onBdayChange(bday);
  }

  handleStartDateChange = (e, idx, startDate) => {
    this.setState({startDate});
    this.props.onStartDateChange(startDate);
  }

  handleEndDateChange = (e, idx, endDate) => {
    this.setState({endDate});
    this.props.onEndDateChange(endDate);
  }

  handleActionChange = (e, idx, action) => {
    this.setState({action});
    this.props.onActionChange(action);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }
  onChange = (e, val) => {
    this.props.onWMYChange(val);
  }

  render(){
    const {data} = this.props;
    console.log('voucher data', data, this.state);
    const today = new Date();

    let startNoDefault = true,
      endNoDefault = true;


    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December'];
    let monthList = []
    monthList.push(<MenuItem key={0} value='' primaryText='Select Month' />);
    months.map( (val, idx) => {
      monthList.push(<MenuItem key={(idx+1)} value={idx} primaryText={val} />);
    });

    if (this.state.shouldDisplay){

      startNoDefault = (this.props.shouldEdit) ? ((data.selectedRecord.startDate == '0000-00-00') ? true : false)
        : true;
      endNoDefault = (this.props.shouldEdit) ? ((data.selectedRecord.endDate == '0000-00-00') ? true : false)
        : true;
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}
                  info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                </div>

                <div className='content-field-holder'>
                  <TextField
                    name="name"
                    hintText="Enter Name"
                    floatingLabelText="Name"
                    defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}
                    rows={2}
                    className="textfield-regular"
                  /><br />
                  {/* <TextField
                    name="description"
                    hintText="Enter Description"
                    floatingLabelText="Description"
                    defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                    onChange={this.props.onChange}
                    rows={4}

                  /><br /> */}
                  <DefaultTextArea
                    name="description"
                    hintText="Enter Description"
                    floatingLabelText="Description"
                    defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                    onChange={this.props.onChange}
                    rows={4}
                    maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                    regular={true}
                  /><br />
                  <DefaultTextArea
                    name="terms"
                    hintText="Enter Terms"
                    floatingLabelText="Terms"
                    defaultValue={(data.selectedRecord.terms) ? data.selectedRecord.terms : '' }
                    onChange={this.props.onChange}
                    rows={4}
                    maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                    regular={true}
                  /><br />
                  {/* <TextField
                    name="terms"
                    hintText="Enter Terms"
                    floatingLabelText="Terms"
                    defaultValue={(data.selectedRecord.terms) ? data.selectedRecord.terms : '' }
                    onChange={this.props.onChange}
                    rows={4}

                  /><br /> */}
                  <br />
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <br />
                <Row>
                  <Col md={12} style={{ marginBottom: '-20px' }}>
                    <span style={{ color: '#b2b2b2' }}>Voucher Validity</span>
                  </Col>
                  <Col md={6}>
                    { startNoDefault ?
                      <DatePicker
                        hintText="Enter Start Date"
                        floatingLabelText="Start Date"
                        onChange={this.props.onStartDate}
                        firstDayOfWeek={0}
                        minDate={today}
                        className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'show'}
                      /> :
                      <DatePicker
                        hintText="Enter Start Date"
                        floatingLabelText="Start Date"
                        onChange={this.props.onStartDate}
                        defaultDate={this.state.startDate}
                        firstDayOfWeek={0}
                        minDate={today}
                        className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'show'}
                      /> }
                  </Col>
                  <Col md={6}>
                    { endNoDefault ?
                      <DatePicker
                        hintText="Enter End Date"
                        floatingLabelText="End Date"
                        onChange={this.props.onEndDate}
                        firstDayOfWeek={0}
                        minDate={today}
                        className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'show'}
                      /> :
                      <DatePicker
                        hintText="Enter End Date"
                        floatingLabelText="End Date"
                        onChange={this.props.onEndDate}
                        defaultDate={this.state.endDate}
                        firstDayOfWeek={0}
                        minDate={today}
                        className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'show'}
                      /> }
                  </Col>
                </Row>
                <Row>
                  <Col md={12}>
                    <SelectField value={this.state.type} onChange={this.handleTypeChange}
                      autoWidth={false}  floatingLabelText="Voucher Type"
                      hintText="Select Voucher Type" floatingLabelFixed={true} >
                      <MenuItem value="" primaryText="" />
                      <MenuItem value="registration" primaryText="Registration" />
                      <MenuItem value="flash" primaryText="Flash" />
                      <MenuItem value="birthday" primaryText="Birthday" />
                      <MenuItem value="wemissyou" primaryText="We Miss You" />
                      {/*<MenuItem value="transaction" primaryText="Transaction" />*/}
                    </SelectField>
                  </Col>
                </Row>
                <Row>
                  <Col md={12}>
                    <SelectField value={(data.selectedRecord.month) ? data.selectedRecord.month : this.state.month}
                      onChange={this.handleMonthChange} autoWidth={false}  floatingLabelText="Month Selection"
                      hintText="Select month" floatingLabelFixed={true} className={(this.state.type == 'birthday') ? 'textfield-default' : 'hide'}>
                      <MenuItem value="13" primaryText="All Months" />
                      <MenuItem value="1" primaryText="January" />
                      <MenuItem value="2" primaryText="February" />
                      <MenuItem value="3" primaryText="March" />
                      <MenuItem value="4" primaryText="April" />
                      <MenuItem value="5" primaryText="May" />
                      <MenuItem value="6" primaryText="June" />
                      <MenuItem value="7" primaryText="July" />
                      <MenuItem value="8" primaryText="August" />
                      <MenuItem value="9" primaryText="September" />
                      <MenuItem value="10" primaryText="October" />
                      <MenuItem value="11" primaryText="November" />
                      <MenuItem value="12" primaryText="December" />
                    </SelectField>
                  </Col>
                </Row>
                {/*<SelectField value={(data.selectedRecord.frequencyType) ? data.selectedRecord.frequencyType : this.state.frequencyType}
                  onChange={this.handleFrequencyTypeChange} autoWidth={false}  floatingLabelText="Frequency Type"
                  hintText="Select Frequency Type" floatingLabelFixed={true} className={(this.state.type == 'birthday') ? 'textfield-default' : 'hide'}>
                  {/*<MenuItem value="year" primaryText="Year" />
                  <MenuItem value="month" primaryText="Month" />
                  <MenuItem value="day" primaryText="Day" />
                  <MenuItem value="hour" primaryText="Hour" />
                  </SelectField>
                <br />*/}


                {/*<SelectField value={(data.selectedRecord.parameterString) ? data.selectedRecord.parameterString : this.state.parameterString} onChange={this.handleParameterStringChange}
                  autoWidth={true}  floatingLabelText="Parameter Title" hintText="Select Parameter Title" floatingLabelFixed={true}>
                  <MenuItem value="transaction" primaryText="Transaction" />
                  <MenuItem value="discount_percentage" primaryText="Discount Percentage" />
                  <MenuItem value="discount_amount" primaryText="Discount Amount" />
                  </SelectField>
                  <br />
                  <TextField
                  type="text"
                  name="parameterValue"
                  floatingLabelText="Parameter Value"
                  defaultValue={(data.selectedRecord.parameterValue) ? data.selectedRecord.parameterValue : '' }
                  onChange={this.props.onChange}

                  /><br />
                  <TextField
                  name="wmyperiod"
                  type="number"
                  hintText="Enter We Miss You Period"
                  floatingLabelText="We Miss You Period"
                  defaultValue={(data.selectedRecord.wmyperiod) ? data.selectedRecord.wmyperiod : '' }
                  onChange={this.props.onChange}
                  className={(this.state.type == 'wemissyou') ? 'textfield-default' : 'hide'}

                  />
                  <DropDownMenu
                  value={this.state.bday}
                  onChange={this.handleBirthdayChange}

                  className={(this.state.type == 'birthday') ? 'dropdownButton' : 'hide'}>
                  {monthList}
                  </DropDownMenu><br />

                  startNoDefault ?
                  <DatePicker
                    hintText="Enter Start Date"
                    floatingLabelText="Start Date"
                    onChange={this.props.startDate}
                    firstDayOfWeek={0}

                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                    /> :
                  <DatePicker
                    hintText="Enter Start Date"
                    floatingLabelText="Start Date"
                    onChange={this.props.startDate}
                    defaultDate={this.state.startDate}
                    firstDayOfWeek={0}

                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                    /> }

                  { endNoDefault ?
                  <DatePicker
                    hintText="Enter End Date"
                    floatingLabelText="End Date"
                    onChange={this.props.endDate}
                    firstDayOfWeek={0}

                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                    /> :
                  <DatePicker
                    hintText="Enter End Date"
                    floatingLabelText="End Date"
                    onChange={this.props.endDate}
                    defaultDate={this.state.endDate}
                    firstDayOfWeek={0}

                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                /> */}


                {/*<TextField
                  name="quantity"
                  type="number"
                  min="1"
                  max="1"
                  hintText="Enter Quantity"
                  floatingLabelText="Quantity"
                  defaultValue="1"
                  onChange={this.props.onChange}
                  className={(this.state.type == 'discount') ? 'textfield-default' : 'hide'}
                  /><br />

                  <TextField
                  name="quantity"
                  type="number"
                  min="1"
                  max="1"
                  hintText="Enter Quantity"
                  floatingLabelText="Quantity"
                  defaultValue="1"
                  onChange={this.props.onChange}
                  className={(this.state.type == 'flash') ? 'textfield-default' : 'hide'}
                  /><br />
                  <TextField
                  name="quantity"
                  type="number"
                  min="1"
                  hintText="Enter Quantity"
                  floatingLabelText="Quantity"
                  defaultValue={(data.selectedRecord.quantity) ? data.selectedRecord.quantity : '' }
                  onChange={this.props.onChange}
                  className={(this.state.type != 'discount' && this.state.type != 'flash' ) ? 'textfield-default' : 'hide'}
                /><br />*/}


                {/*<TextField
                  name="quantity"
                  type="number"
                  min="1"
                  hintText="Enter Quantity"
                  floatingLabelText="Quantity"
                  defaultValue={(data.selectedRecord.quantity) ? data.selectedRecord.quantity : '' }
                  onChange={this.props.onChange}
                  className={( this.state.type != 'flash' ) ? 'textfield-default' : 'hide'}
                  /><br />

                  <TextField
                  name="redemptionlimit"
                  type="number"
                  min="1"
                  max="1"
                  hintText="Redemption Limit"
                  floatingLabelText="Redemption Limit"
                  defaultValue="1"
                  onChange={this.props.onChange}
                  className={(this.state.type == 'flash') ? 'textfield-default' : 'hide'}
                  /><br />
                  <SelectField value={(data.selectedRecord.action) ? data.selectedRecord.action : this.state.action} onChange={this.handleActionChange}
                  autoWidth={false}  floatingLabelText="Action" hintText="Select Action" floatingLabelFixed={true}>
                  <MenuItem value="move" primaryText="Move" />
                  <MenuItem value="copy" primaryText="Copy" />
                  </SelectField><br />
                  <SelectField value={(data.selectedRecord.action) ? data.selectedRecord.action : this.state.action} onChange={this.handleActionChange}
                  autoWidth={false}  floatingLabelText="Action" hintText="Select Action" floatingLabelFixed={true}>
                  <MenuItem value="move" primaryText="Move" />
                  <MenuItem value="copy" primaryText="Copy" />
                </SelectField><br />*/}
                <Row>
                  <Col md={12} style={{ marginBottom: '-20px', marginTop: '10px' }} className={(this.state.type == 'wemissyou') ? 'textfield-default' : 'hide'}>
                    <span style={{ color: '#b2b2b2' }}>Select Lapse Period</span>
                  </Col>
                  <RadioButtonGroup name="wmyperiod" defaultSelected={(data.selectedRecord.wmyperiod) ? data.selectedRecord.wmyperiod : '' }
                    className={(this.state.type == 'wemissyou') ? 'textfield-default' : 'hide'} style={{ marginTop: '40px', marginLeft: '20px' }}
                    value={(data.selectedRecord.wmyperiod) ? data.selectedRecord.wmyperiod : this.state.wmyperiod} onChange={this.onChange}>
                    <RadioButton
                      value="30"
                      name="30"
                      label="30 Days"
                    />
                    <RadioButton
                      value="60"
                      name="60"
                      label="60 Days"
                    />
                    <RadioButton
                      value="90"
                      name="90"
                      label="90 Days"
                    />
                  </RadioButtonGroup>
                </Row>
                <Row>
                  <Col md={12}>
                    {
                      (this.state.type != 'flash')?
                        <TextField
                          readOnly
                          name="quantity"
                          type="number"
                          pattern="[0-9]*"
                          min="1"
                          max="1"
                          hintText="Enter Quantity"
                          floatingLabelText="Quantity"
                          value="1"
                          onChange={this.props.onChange}
                        />
                      :
                      <TextField
                        name="quantity"
                        type="number"
                        pattern="[0-9]*"
                        min="1"
                        max="1"
                        hintText="Enter Quantity"
                        floatingLabelText="Quantity"
                        defaultValue={(data.selectedRecord.quantity) ? data.selectedRecord.quantity : '' }
                        onChange={this.props.onChange}
                        className={(this.state.type == 'flash') ? 'hide' : 'hide'}
                      />
                    }
                  </Col>
                </Row>
                {/* <Row>
                  <Col md={6} className={(this.state.type == 'flash') ? 'textfield-default' : 'hide'}>
                    <SelectField value={(data.selectedRecord.frequencyStart) ? data.selectedRecord.frequencyStart : this.state.frequencyStart} onChange={this.handleFrequencyStartChange}
                  autoWidth={false}  floatingLabelText="Start Day" hintText="Select Start Day" floatingLabelFixed={true}>
                  <MenuItem value="sunday" primaryText="Sunday" />
                  <MenuItem value="monday" primaryText="Monday" />
                  <MenuItem value="tuesday" primaryText="Tuesday" />
                  <MenuItem value="wednesday" primaryText="Wednesday" />
                  <MenuItem value="thursday" primaryText="Thursday" />
                  <MenuItem value="friday" primaryText="Friday" />
                  <MenuItem value="saturday" primaryText="Saturday" />
                    </SelectField>
                  </Col>
                  <Col md={6}  className={(this.state.type == 'flash') ? 'textfield-default' : 'hide'}>
                    <SelectField value={(data.selectedRecord.frequencyEnd) ? data.selectedRecord.frequencyEnd : this.state.frequencyEnd} onChange={this.handleFrequencyEndChange}
                  autoWidth={false}  floatingLabelText="End Day" hintText="Select End Day" floatingLabelFixed={true}>
                  <MenuItem value="sunday" primaryText="Sunday" />
                  <MenuItem value="monday" primaryText="Monday" />
                  <MenuItem value="tuesday" primaryText="Tuesday" />
                  <MenuItem value="wednesday" primaryText="Wednesday" />
                  <MenuItem value="thursday" primaryText="Thursday" />
                  <MenuItem value="friday" primaryText="Friday" />
                  <MenuItem value="saturday" primaryText="Saturday" />
                    </SelectField>
                    <br />
                  </Col>
                  </Row>
                  <Row>
                  <Col md={6}>
                    <SelectField value={(data.selectedRecord.startTime) ? data.selectedRecord.startTime : this.state.startTime}
                  onChange={this.handleTimeStartChange} autoWidth={false}  floatingLabelText="Start Time"
                  hintText="Select Start Time" floatingLabelFixed={true} className={(this.state.type == 'flash') ? 'textfield-default' : 'hide'}>
                  <MenuItem value="00:00:00" primaryText="00:00" />
                  <MenuItem value="01:00:00" primaryText="01:00" />
                  <MenuItem value="02:00:00" primaryText="02:00" />
                  <MenuItem value="03:00:00" primaryText="03:00" />
                  <MenuItem value="04:00:00" primaryText="04:00" />
                  <MenuItem value="05:00:00" primaryText="05:00" />
                  <MenuItem value="06:00:00" primaryText="06:00" />
                  <MenuItem value="07:00:00" primaryText="07:00" />
                  <MenuItem value="08:00:00" primaryText="08:00" />
                  <MenuItem value="09:00:00" primaryText="09:00" />
                  <MenuItem value="10:00:00" primaryText="10:00" />
                  <MenuItem value="11:00:00" primaryText="11:00" />
                  <MenuItem value="12:00:00" primaryText="12:00" />
                  <MenuItem value="13:00:00" primaryText="13:00" />
                  <MenuItem value="14:00:00" primaryText="14:00" />
                  <MenuItem value="15:00:00" primaryText="15:00" />
                  <MenuItem value="16:00:00" primaryText="16:00" />
                  <MenuItem value="17:00:00" primaryText="17:00" />
                  <MenuItem value="18:00:00" primaryText="18:00" />
                  <MenuItem value="19:00:00" primaryText="19:00" />
                  <MenuItem value="20:00:00" primaryText="20:00" />
                  <MenuItem value="21:00:00" primaryText="21:00" />
                  <MenuItem value="22:00:00" primaryText="22:00" />
                  <MenuItem value="23:00:00" primaryText="23:00" />
                    </SelectField>
                  </Col>
                  <Col md={6}>
                    <SelectField value={(data.selectedRecord.endTime) ? data.selectedRecord.endTime : this.state.endTime}
                  onChange={this.handleTimeEndChange} autoWidth={false}  floatingLabelText="End Time"
                  hintText="Select End Time" floatingLabelFixed={true} className={(this.state.type == 'flash') ? 'textfield-default' : 'hide'}>
                  <MenuItem value="00:00:00" primaryText="00:00" />
                  <MenuItem value="01:00:00" primaryText="01:00" />
                  <MenuItem value="02:00:00" primaryText="02:00" />
                  <MenuItem value="03:00:00" primaryText="03:00" />
                  <MenuItem value="04:00:00" primaryText="04:00" />
                  <MenuItem value="05:00:00" primaryText="05:00" />
                  <MenuItem value="06:00:00" primaryText="06:00" />
                  <MenuItem value="07:00:00" primaryText="07:00" />
                  <MenuItem value="08:00:00" primaryText="08:00" />
                  <MenuItem value="09:00:00" primaryText="09:00" />
                  <MenuItem value="10:00:00" primaryText="10:00" />
                  <MenuItem value="11:00:00" primaryText="11:00" />
                  <MenuItem value="12:00:00" primaryText="12:00" />
                  <MenuItem value="13:00:00" primaryText="13:00" />
                  <MenuItem value="14:00:00" primaryText="14:00" />
                  <MenuItem value="15:00:00" primaryText="15:00" />
                  <MenuItem value="16:00:00" primaryText="16:00" />
                  <MenuItem value="17:00:00" primaryText="17:00" />
                  <MenuItem value="18:00:00" primaryText="18:00" />
                  <MenuItem value="19:00:00" primaryText="19:00" />
                  <MenuItem value="20:00:00" primaryText="20:00" />
                  <MenuItem value="21:00:00" primaryText="21:00" />
                  <MenuItem value="22:00:00" primaryText="22:00" />
                  <MenuItem value="23:00:00" primaryText="23:00" />
                    </SelectField>
                  </Col>
                </Row> */}
                {/*<TextField
                  name="accumulated_amt"
                  type="number"
                  min="1"
                  hintText="Enter Accumulated Amount"
                  floatingLabelText="Accumulated Amount"
                  defaultValue={(data.selectedRecord.accumulated_amt) ? data.selectedRecord.accumulated_amt : '' }
                  onChange={this.props.onChange}
                  className={(this.state.type == 'flash') ? 'textfield-default' : 'hide'}
                /><br />*/}
                <TextField
                  name="redemptionlimit"
                  type="number"
                  pattern="[0-9]*"
                  min="0"
                  hintText="Redemption Limit"
                  floatingLabelText="Redemption Limit"
                  defaultValue={(data.selectedRecord.redemptionlimit) ? data.selectedRecord.redemptionlimit : 0 }
                  onChange={this.props.onChange}
                  /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default VoucherForm;
