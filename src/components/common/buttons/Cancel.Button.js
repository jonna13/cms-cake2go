'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class CancelButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Cancel"
            onClick={this.props.handleOpen}
            className="gray-btn"
          />
        </div>
      </div>
    );
  }
}

export default CancelButton;
