'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';
import Done from 'material-ui/svg-icons/action/done';

class ActiveButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Active"
            onClick={this.props.handleOpen}
            className="white-btn"
            icon={<Done className="green-icon" />}
          />
        </div>
      </div>
    );
  }
}

export default ActiveButton;
