'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class ShowAllProductButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Show All Products"
            onClick={this.props.handleOpen}
            className="blue-btn"
          />
        </div>
      </div>
    );
  }
}

export default ShowAllProductButton;
