'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';
import Done from 'material-ui/svg-icons/action/done';

class ShowAllButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Show All"
            onClick={this.props.handleOpen}
            className="white-btn"
          />
        </div>
      </div>
    );
  }
}

export default ShowAllButton;
