'use strict';

import React from 'react';

import IconButton from 'material-ui/IconButton';
import Update from 'material-ui/svg-icons/content/save';
import RaisedButton from 'material-ui/RaisedButton';

class UpdateButton extends React.Component{
  render(){
    return(
      <div>
        {/*<div className='v-floating-action-circle hidden-xs'>
          <IconButton tooltip="Click to update" touch={true} tooltipPosition="bottom-left" onClick={this.props.handleOpen}>
            <Update className='floating-button-icon'/>
          </IconButton>
        </div>

        <div className='floating-button visible-xs'>
          <IconButton touch={true} tooltipPosition="bottom-left" onClick={this.props.handleOpen}>
            <Update className='floating-button-icon'/>
          </IconButton>
        </div>*/}
        <div>
          <RaisedButton
            label="Update"
            onClick={this.props.handleOpen}
            className="red-btn"
          />
        </div>
      </div>
    );
  }
}

export default UpdateButton;
