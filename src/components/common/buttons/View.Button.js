'use strict';

import React from 'react';

import RaisedButton from 'material-ui/RaisedButton';
import Add from 'material-ui/svg-icons/content/add';

class ViewButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="View Details"
            onClick={this.props.handleOpen}
            className="blue-btn"
          />
        </div>


      </div>
    );
  }
}

export default ViewButton;
