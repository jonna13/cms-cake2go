/**
 * Created by jedachas on 3/30/17.
 */
'use strict';

import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import {CleanWord} from '../Utility';
import classNames from 'classnames';


class DefaultTextAreaTwo extends Component {

  state = {
    remaining: 0
  }

  handleChange = (e) => {
    this.props.onChange(e);
    this.setState({text: e.target.value });
    let chars = e.target.value;
    this.setState({remaining: this.props.maxSize - chars.length })
  }

  componentWillMount(){
    this.setState({ remaining: (this.props.defaultValue.length > 0) ?
      (this.props.maxSize - this.props.defaultValue.length) : this.props.maxSize });
  }

  render() {
    let areaHolderClass = classNames({
      'text-area-holder-default': (!this.props.wide && !this.props.regular),
      'text-area-holder-wide': this.props.wide,
      'text-area-holder-regular': this.props.regular
    });
    let textareaClass = classNames({
      'text-area-content-wide': this.props.wide,
      'text-area-content-regular': this.props.regular
    });

    return (
      <div className={areaHolderClass}>
        <TextField
          name={this.props.name}
          hintText={this.props.hintText}
          floatingLabelText={this.props.floatingLabelText}
          defaultValue={CleanWord(this.props.defaultValue)}
          onChange={this.handleChange}
          multiLine={true}
          rows={this.props.rows}
          className={textareaClass}
          disabled={this.props.disabled}
          fullWidth={this.props.fullWidth}
        /><br />
      </div>
    );
  }
}

export default DefaultTextAreaTwo;
