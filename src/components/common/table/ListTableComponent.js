/**
 * Created by jedachas on 2/23/17.
 */
'use strict';

import React, {Component} from 'react';
import Pagination from './PaginationComponent';
import {Table} from 'react-bootstrap';
import Sort from 'material-ui/svg-icons/content/sort';

class ListTableComponent extends Component {
  componentWillMount(){
    this.populateData();
  }

  populateData(){
    const { pageinfo } = this.props.data;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: (pageinfo.sortColumnName) ? pageinfo.sortColumnName : '',
      sortOrder: (pageinfo.sortOrder) ? pageinfo.sortOrder: '',
      selectedStatus: (pageinfo.selectedStatus) ? pageinfo.selectedStatus: ''
    }
    this.props.onGetData(params);
  }

  pageChanged = (eventKey, e) => {
    e.preventDefault();
    this.props.data.pageinfo.currentPage = eventKey;
    this.populateData();
  }

  sortChanged = (columnname, order) => {
    const { pageinfo } = this.props.data;
    pageinfo.sortColumnName = columnname;
    pageinfo.sortOrder = order == 'ASC' ? 'DESC' : 'ASC';

    this.populateData();
  }

  render(){
    const {pageinfo} = this.props.data;
    let headers = [];
    this.props.headers.forEach((val, idx) => {
      // if (val.value == '') {
      //   headers.push(
      //   <th key={idx} onClick={this.sortChanged.bind(this, '', pageinfo.sortOrder)}>
      //     <Sort className={(pageinfo.sortColumnName == '') ? 'active sorticon': 'sorticon'}/>
      //   </th>);
      // }
      if (val.value == 'dateAdded') {
        headers.push(
        <th className="strong-th" key={idx} onClick={this.sortChanged.bind(this, 'dateAdded', pageinfo.sortOrder)}>
          <Sort className={(pageinfo.sortColumnName == 'dateAdded') ? 'active sorticon': 'sorticon'}/>
        </th>);
      }
      else{
        headers.push(
        <th className="strong-th" key={idx} onClick={this.sortChanged.bind(this, val.value, pageinfo.sortOrder)}>
          <label className={(pageinfo.sortColumnName == val.value) ? 'active': ''}>{val.title}</label>
        </th>);
      }
    });

    return(
      <div>
      <Table striped hover className='list-table'>
          <thead>
          <tr>
            {headers}
          </tr>
          </thead>
          <tbody>
            {this.props.children}
          </tbody>
      </Table>

        <Pagination Size={pageinfo.totalPage}
          onPageChanged={this.pageChanged}
          currentPage={pageinfo.currentPage}/>
      </div>
    );
  }
}

export default ListTableComponent;
