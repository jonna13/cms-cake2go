'use strict';

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ThumbImage from './table/ThumbImage';
import Config from '../../config/base';
import Info from 'material-ui/svg-icons/action/info';
import RaisedButton from 'material-ui/RaisedButton';
import styles from '../../styles/Style';
const defaultImage = require('../../images/no-image.jpg');

class ImageUploader extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: ''
    }
    this.state = {file: '',imagePreviewUrl: ''};
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    console.log('file', file);
    console.log('reader', reader);

    reader.onloadend = () => {
      if (file.size > Config.IMAGE_SIZE_LIMIT) {
        this.props.dialogActions.openNotification('Image size is too large',
          Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
          return;
      }
      if (file.type > Config.IMAGE_FORMAT) {
        this.props.dialogActions.openNotification('Invalid image type',
          Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
          return;
      }
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
      this.props.onImageChange(this.state);
    }
    reader.readAsDataURL(file);
  }

  render(){
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH + '' + this.props.imageModule + '/thumb/' + this.props.image;

    let imageinfo =  (this.props.info) ? this.props.info : 'Best size: Less than 100kb. Max 2MB';

    if (imagePreviewUrl) {
      $imagePreview = (<ThumbImage image={imagePreviewUrl} ref='myImage'/>);
    }
    else{
      if (this.props.image) {
        $imagePreview = (<ThumbImage image={path} />);
      }
      else{
        $imagePreview = (<ThumbImage image={defaultImage} />);
      }
    }
    return(
      <div>
        <div className="modal-image-preview">
          { $imagePreview }
        </div>
        <div className="modal-image-info">
          <Info/><div>{imageinfo}</div>
        </div>
        <div className="modal-image-button">
          <form onSubmit={(e)=>this._handleSubmit(e)}>
            <RaisedButton style= {{ backgroundColor: '#000' }} containerElement='image' label="Choose an Image" labelPosition="before" onChange={(e)=>this._handleImageChange(e)}>
              <input type="file" style={styles.imageInput}/>
            </RaisedButton>
          </form>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const props = { dialogState: state.dialogState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { dialogAction: require('../../actions/dialogAction.js') };
  const actionMap = {dialogActions: bindActionCreators(actions.dialogAction, dispatch)};
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(ImageUploader);
