'use strict';

import React, {
  Component
} from 'react';
import Search from 'material-ui/svg-icons/action/search';
import Close from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';

class SearchBar extends Component{
  constructor(props){
    super(props);
    this.state = {
      text: ''
    }
  }

  handleClose = () => {
    this.setState({text:''});
    this.props.handleClose();
  }

  handleChange = (e) => {
    this.setState({
      text: e.target.value
    });
    let text = e.target.value;
    this.props.handleChange(text);
  }

  render() {
    let {pageinfo} = this.props;
    let Icon = () => {
      if(pageinfo == undefined){
        return(
          <IconButton touch={true} className='search-button'>
            <Search className='search-icon'/>
          </IconButton>
        );
      }else {
        if (pageinfo.searchFilter == '') {
          return(
            <IconButton touch={true} className='search-button'>
              <Search className='search-icon'/>
            </IconButton>
          );
        }
        else{
          return(
            <IconButton touch={true}
              className='close-button'
              onClick={this.handleClose}>
              <Close className='close-icon'/>
            </IconButton>
          );
        }
      }
    }

    return(
      <div className="search-form">
        <Icon />
        <input className='search-bar'
          name="search"
          type="text"
          value={this.state.text}
          placeholder="Search for?"
          onChange={this.handleChange}
          ref='searchField'/>
      </div>
    );
  }
}

export default SearchBar;
