'use strict';

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TextField from 'material-ui/TextField';
import Config from '../../config/base';
import Divider from 'material-ui/Divider';
import {UpdateButton} from '../common/buttons';
import { Grid, Row, Col } from 'react-bootstrap';
import ImageUpload from '../common/ImageUpload';
import jwtDecode from 'jwt-decode';
import _ from 'lodash';
import PrintComponent from '../common/PrintComponent';

import AuthApi from '../../api/auth.api';
const logo = require('../../images/logo.png');

class CredentialEditor extends React.Component {

    state = {
      credentials: {
        old_password: '',
        new_password: '',
        reenter: '',
        profilePic: '',
        username: '',
        fullname: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.ACCOUNT,
    }

    componentWillMount(){
      let user = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
      console.info('user', user);
      this.setState({
        credentials: {
          profilePic: user.profilePic,
          username: user.username,
          fullname: user.fullname
        }
      });
    }

    handleInput = (event) => {
      const field = event.target.name;
      const details = this.state.credentials;
      details[field] = event.target.value;
    }

    handleImageChange = (e) => {
      this.setState({ uploadImage: e });
    }

    // handle Update
    handleUpdate = () => {
      if (this.validateInput(this.state.credentials)) {
        const {dialogActions} = this.props;
        dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
          Config.DIALOG_MESSAGE.UPDATE_CREDENTIAL_MESSAGE,
          Config.DIALOG_MESSAGE.CONFIRM_LABEL,
          Config.DIALOG_MESSAGE.CLOSE_LABEL,
          (result) => {
            if (result) {
              let {authActions} = this.props;
              authActions.updatePassword(this.state.credentials, this.state.uploadImage, this.props.router);
            }
          });
      }
    }

    validateInput = (data) => {
      console.log("validate data", data.new_password);
      const Expression = (/^(?=.*\d)[0-9a-zA-Z]{5,}$/);
      const {dialogActions} = this.props;
      if (data.old_password == '') {
        dialogActions.openNotification('Oops! Please enter old password.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.new_password == '') {
        dialogActions.openNotification('Oops! Please enter new password.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if ((data.new_password == '') || (!data.new_password.match(Expression))) {
        dialogActions.openNotification('Should have at least 1 number & 1 letter. At least 5 characters', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }

      if (data.reenter == '') {
        dialogActions.openNotification('Oops! Please re-enter new password.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.new_password != data.reenter) {
        dialogActions.openNotification('Oops! Re-enter new password correctly.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }

      return true;
    }

    render() {
      console.info('credential state', this.state);
      return (
        <div>
          <h2 className='content-heading'> Credentials </h2>
          <Divider className='content-divider' />

          { /* Action Buttons */ }
          <UpdateButton handleOpen={this.handleUpdate}/>
          <Grid fluid>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6}>
                <div className='content-container'>
                  <div className='item-image-box'>
                    <ImageUpload image={null}
                      onImageChange={this.handleImageChange}
                      image={(this.state.credentials.profilePic) ? this.state.credentials.profilePic : '' }
                      imageModule={this.state.imageModule}
                      info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                  </div>
                </div>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6}>
                <div className='content-field-holder'>
                  <TextField
                    disabled={true}
                    name="username"
                    floatingLabelText="Username"
                    defaultValue={(this.state.credentials.username) ? this.state.credentials.username : '' }
                    className="textfield-regular"
                  /><br />
                  <TextField
                    name="fullname"
                    hintText="Enter Fullname"
                    floatingLabelText="Fullname"
                    defaultValue={(this.state.credentials.fullname) ? this.state.credentials.fullname : '' }
                    onChange={this.handleInput}
                    className="textfield-regular"
                  /><br />
                  <TextField
                    name="old_password"
                    hintText="Enter Old Password"
                    floatingLabelText="Old Password"
                    type="password"
                    onChange={this.handleInput}
                    className="textfield-regular"
                  /><br />
                  <TextField
                    name="new_password"
                    hintText="Enter New Password"
                    floatingLabelText="New Password"
                    type="password"
                    onChange={this.handleInput}
                    className="textfield-regular"
                  /><br />
                  <TextField
                    name="reenter"
                    hintText="Enter Password Again"
                    floatingLabelText="Re-enter Password"
                    type="password"
                    onChange={this.handleInput}
                    className="textfield-regular"
                  /><br />
                </div>
              </Col>
            </Row>
          </Grid>


        </div>
      );
    }

}

CredentialEditor.displayName = 'AuthCredentialEditor';



function mapStateToProps(state) {
  const props = {
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    dialogAction: require('../../actions/dialogAction.js'),
    authAction: require('../../actions/authAction.js'),
  };
  const actionMap = {
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
    authActions: bindActionCreators(actions.authAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CredentialEditor);
