import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import AppStyles from '../../styles/Style';

class OrderArchiveGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <EditIcon />
          </IconButton>
        </td>
        <td>
          {item.name}
        </td>
        <td>
          {item.name}
        </td>
        <td>
          {item.name}
        </td>
        <td>
          {item.name}
        </td>
        <td>
          <label className={ (item.status == 'active' ) ? 'label-active' : 'label-inactive'}>{item.status}</label>
        </td>
      </tr>
    );
  }
}

export default OrderArchiveGridRow;
