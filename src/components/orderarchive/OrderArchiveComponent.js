'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import Search from 'material-ui/svg-icons/action/search';
import Config from '../../config/base';
import {AddButton, ViewButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import OrderArchiveList from './OrderArchiveList';

class CategoryArchiveComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/category_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/category/'+item.categoryID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.categorys;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.categorys;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.categorys;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.categorys;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.categorys;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getCategorys(params);
  }

  render() {
    const {pageinfo} = this.props.data.categorys;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading title-gotham'>Order Archive</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ (pageinfo == undefined)? '' : pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.categorys.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.categorys.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.categorys.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

        <Card className="orderarchive-card">
          <Form>
            <Row>
              <Col md={12}>
                <Label className="label-dft strong-6 title-gotham">Total Count: &nbsp;</Label>
                <Label className="big-txt strong-2 title-gotham">13</Label>
              </Col>
            </Row>
          </Form>
        </Card>

        { /* List  */ }
        <OrderArchiveList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default CategoryArchiveComponent;
