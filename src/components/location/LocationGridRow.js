import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import AppStyles from '../../styles/Style';

class GridRowComponent extends Component {
  render(){
    let item = this.props.items;

    return(
      <tr>
        <td>
          {item.branchCode}
        </td>
        <td className="pointer blue-text" onClick={()=> { this.props.onEditClick(item); }}>
          {item.name}
        </td>
        <td className="pointer blue-text" onClick={()=> { this.props.onEditClick(item); }}>
          {item.address}
        </td>
        <td>{item.latitude}</td>
        <td>
          {item.longitude}
        </td>
        <td>
          {item.phone}
        </td>
        <td>
          <label className={ (item.status == 'active' ) ? 'label-active white capital' : 'label-inactive white capital'}>{item.status}</label>
        </td>
      </tr>
    );
  }
}

export default GridRowComponent;
