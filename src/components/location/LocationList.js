/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import GridRow from './LocationGridRow';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

class LocationList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getLocations(params);
  }

  render(){
    let {locations} = this.props.data;
    const headers = [
      { title: 'Code', value: 'branchCode'},
      { title: 'Branch Name', value: 'name'},
      { title: 'Address', value: 'address'},
      { title: 'Latitude', value: 'latitude'},
      { title: 'Longitude', value: 'longitude'},
      { title: 'Contact Number', value: 'phone'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (locations.records.length > 0) {
      locations.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.locations}
            onGetData={this.handleGetData}
            hasImage={false}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.locations}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default LocationList;
