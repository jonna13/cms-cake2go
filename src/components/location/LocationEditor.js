/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import LocationForm from './LocationForm';


class LocationEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      location: {
        name: '',
        branchCode: '',
        address: '',
        latitude: '',
        longitude: '',
        phone: '',
        status: false,
        locFlag: false
      },
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewLocation(this.props.params.id);
    }
    actions.getBranchCodes();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.locationState.selectedRecord)) {
        this.setState({
          location: nextProps.locationState.selectedRecord
        });
      }
    }else{
      var bID = nextProps.locationState.locations.category.map(function(x) {
         return x['brandID'];
      });

      // var myJSON = JSON.stringify(bID[0]);
      console.log('bID', bID[0]);
      let _location = this.state.location;
      _location.brandID = bID[0];
      this.setState({
        location: _location
      });
    }


  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/branch');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput2(this.state.location)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_LOCATION_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateLocation(this.state.location, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.location)) {
    // if (this.validateBranchCode(this.state.location)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_LOCATION_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addLocation(this.state.location, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let location = this.state.location;
    location[field] = e.target.value;
    console.log("handleData", location[field]);
  }

  // validateBranchCode = (data) => {
  //   const {dialogActions} = this.props;
  //   let branchArray = this.props.locationState.branchCodes;
  //   console.log('Validate Branch Code', branchArray);
  //
  //   function in_array(array, branchCode)
  //   {
  //       return array.some(function(item) {
  //           return item.branchCode === branchCode;
  //       });
  //   }
  //
  //   var brCode = data.branchCode;
  //   let userInput = brCode.toString();
  //   console.log('User Input', userInput);
  //
  //   if (in_array(branchArray, userInput) == true) {
  //     dialogActions.openNotification('Oops! Branch Code Already Used!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
  //     return false;
  //   }
  //
  //   return true;
  // }


  validateInput = (data) => {
    const {dialogActions} = this.props;
    let branchArray = this.props.locationState.branchCodes;
    // console.log('Validate Branch Code', branchArray);

    // check if Branch Code already exists
    function in_array(array, branchCode)
    {
        return array.some(function(item) {
            return item.branchCode === branchCode;
        });
    }
    // check user input (Branch Code)
    var brCode = data.branchCode;
    let userInput = brCode.toString();

    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.branchCode == '') {
      dialogActions.openNotification('Oops! No branch code found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (in_array(branchArray, userInput) == true) {
      dialogActions.openNotification('Oops! Branch Code Already Used!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.address == '') {
      dialogActions.openNotification('Oops! No address found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.latitude == '') {
      dialogActions.openNotification('Oops! No latitude found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.longitude == '') {
      dialogActions.openNotification('Oops! No longitude found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.phone == '') {
      dialogActions.openNotification('Oops! No phone found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }


    return true;
  }

  // Validation for Update button
  validateInput2 = (data) => {
    const {dialogActions} = this.props;
    let branchArray = this.props.locationState.branchCodes;
    // console.log('Validate Branch Code', branchArray);

    // check if Branch Code already exists
    function in_array(array, branchCode)
    {
        return array.some(function(item) {
            return item.branchCode === branchCode;
        });
    }
    // check user input (Branch Code)
    var brCode = data.branchCode;
    let userInput = brCode.toString();
    // console.log('User Input', userInput);

    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.address == '') {
      dialogActions.openNotification('Oops! No address found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle Branch Code
  handleBranchCodeChange = (value) => {
    this.state.location['branchCode'] = value;
  }

  // handle status locFlag
  handleLocFlagChange = (e) => {
    this.state.location['locFlag'] = e;
  }

  // handle status checkbox
  // handleStatusChange = (e) => {
  //   this.state.location['status'] = e;
  // }

  // handle status select
  handleStatusChange = (e) => {
    this.state.location['status'] = e;
    console.log('Status', e);
  }

  // handle brand
  handleChangeBrand = (e) => {
    this.state.location['brandID'] = e;
  }

  // handle image change
  // handleImageChange = (e) => {
  //   this.setState({ uploadImage: e });
  // }

  //close modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    const {actions} = this.props;
    console.log('actions', actions);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Branch' : 'Create New Branch'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? 'Edit Branch' : 'Create New Branch'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <LocationForm
            data={this.props.locationState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            actions={actions}
            onBranchCodeChange={this.handleBranchCodeChange}
            onChangeBrand={this.handleChangeBrand}
            onStatusChange={this.handleStatusChange}
            onLocFlagChange={this.handleLocFlagChange}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>



      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    locationState: state.locationState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    locationAction: require('../../actions/locationAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.locationAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationEditor);
