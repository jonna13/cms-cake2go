/**
 * Created by jonna on 8/30/18.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

class LocationForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      name: '',
      branchCode: ''
    };
  }

  componentWillMount(){
    const {actions, data} = this.props;
    // for add
    if (!this.props.shouldEdit){
      this.setState({
        shouldDisplay: true,
      });
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit

    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
        branchCode: nextProps.data.selectedRecord.branchCode
      }
    }
  }

  handleBranchCodeChange = (e, idx, branchCode) => {
    this.setState({branchCode});
    this.props.onBranchCodeChange(branchCode);
  };

  handleBrandChange = (e, idx, brandID) => {
    this.setState({brandID});
    this.props.onChangeBrand(brandID);
  }

  handleStatusChange = (e, idx, status) => {
    console.log('status', e.target.value);
    this.setState({status: e.target.value});
    this.props.onStatusChange(e.target.value);
  }

  // onCheck = (e, val) =>{
  //   this.props.onStatusChange(val);
  // }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    let branchCodeList = [];
    const {data} = this.props;
    console.log('Location Form data', data);


    if (this.props.data.branchCodes) {
      branchCodeList.push(<MenuItem key={0} value='Select Branch Code' />);
      this.props.data.branchCodes.map( (val, idx) => {
        branchCodeList.push(<MenuItem key={(idx+1)} value={val.locID} primaryText={val.branchCode} />);
      });
    }

    // var category = [];
    // console.log('category', data.locations.category[0].brandID);
    //     if (data.locations.category.length > 0) {
    //       data.locations.category.forEach((val, key) => {
    //         category.push(
    //           <MenuItem
    //           key={key}
    //           value={val.brandID}
    //           primaryText={val.name}
    //           />
    //         );
    //       });
    //     }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <div>
              <Col className="modal-container">
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Branch Name : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Branch Code : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="branchCode" id="branchCode" placeholder="" defaultValue={(data.selectedRecord.branchCode) ? data.selectedRecord.branchCode : '' }
                    onChange={this.props.onChange}  className="fullwidth uppercase"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Address : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="textarea" name="address" id="address" placeholder="" defaultValue={(data.selectedRecord.address) ? data.selectedRecord.address : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Latitude : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="latitude" id="latitude" placeholder="" defaultValue={(data.selectedRecord.latitude) ? data.selectedRecord.latitude : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Longitude : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="longitude" id="longitude" placeholder="" defaultValue={(data.selectedRecord.longitude) ? data.selectedRecord.longitude : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Contact Number : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="phone" id="phone" placeholder="" defaultValue={(data.selectedRecord.phone) ? data.selectedRecord.phone : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Status : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Input type="select" name="status" id="status" value={(data.selectedRecord.status) ? data.selectedRecord.status : this.state.status} onChange={this.handleStatusChange}>
                      <option value=""> Please Select Status </option>
                      <option value="active"> Active </option>
                      <option value="inactive"> Inactive </option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={12}>
                  <Label className="label-dft strong-6 title-gotham">Set Business Day and Time : </Label>
                </Col>

                <Col md={6} className="margin-top-10">
                  <Checkbox
                    className="chkbx-label"
                    label="Mon"/>
                  <Col md={12}>
                    <div style={{ display: 'inline-flex' }}>
                    <TextField
                      name="mon_time_from"
                      type="time"
                      defaultValue="09:45"
                      className='form-control-react'
                    />
                    <Label className="label-dft strong-6 title-gotham padding-5">to</Label>
                    <TextField
                      name="mon_time_to"
                      type="time"
                      defaultValue="22:00"
                      className='form-control-react'
                    />
                    </div>
                  </Col>
                </Col>

                <Col md={6} className="margin-top-10">
                  <Checkbox
                    className="chkbx-label"
                    label="Tue"/>
                    <Col md={12}>
                      <div style={{ display: 'inline-flex' }}>
                      <TextField
                        name="tue_time_from"
                        type="time"
                        defaultValue="09:45"
                        className='form-control-react'
                      />
                      <Label className="label-dft strong-6 title-gotham padding-5">to</Label>
                      <TextField
                        name="tue_time_to"
                        type="time"
                        defaultValue="22:00"
                        className='form-control-react'
                      />
                      </div>
                    </Col>
                </Col>

                <Col md={6} className="margin-top-10">
                <Checkbox
                  className="chkbx-label"
                  label="Wed"/>
                  <Col md={12}>
                    <div style={{ display: 'inline-flex' }}>
                    <TextField
                      name="wed_time_from"
                      type="time"
                      defaultValue="09:45"
                      className='form-control-react'
                    />
                    <Label className="label-dft strong-6 title-gotham padding-5">to</Label>
                    <TextField
                      name="wed_time_to"
                      type="time"
                      defaultValue="22:00"
                      className='form-control-react'
                    />
                    </div>
                  </Col>
                </Col>

                <Col md={6} className="margin-top-10">
                  <Checkbox
                    className="chkbx-label"
                    label="Thu"/>
                    <Col md={12}>
                      <div style={{ display: 'inline-flex' }}>
                      <TextField
                        name="thu_time_from"
                        type="time"
                        defaultValue="09:45"
                        className='form-control-react'
                      />
                      <Label className="label-dft strong-6 title-gotham padding-5">to</Label>
                      <TextField
                        name="thu_time_to"
                        type="time"
                        defaultValue="22:00"
                        className='form-control-react'
                      />
                      </div>
                    </Col>
                </Col>

                <Col md={6} className="margin-top-10">
                  <Checkbox
                    className="chkbx-label"
                    label="Fri"/>
                    <Col md={12}>
                      <div style={{ display: 'inline-flex' }}>
                      <TextField
                        name="fri_time_from"
                        type="time"
                        defaultValue="09:45"
                        className='form-control-react'
                      />
                      <Label className="label-dft strong-6 title-gotham padding-5">to</Label>
                      <TextField
                        name="fri_time_to"
                        type="time"
                        defaultValue="22:00"
                        className='form-control-react'
                      />
                      </div>
                    </Col>
                </Col>

                <Col md={6} className="margin-top-10">
                  <Checkbox
                    className="chkbx-label"
                    label="Sat"/>
                    <Col md={12}>
                      <div style={{ display: 'inline-flex' }}>
                      <TextField
                        name="sat_time_from"
                        type="time"
                        defaultValue="09:45"
                        className='form-control-react'
                      />
                      <Label className="label-dft strong-6 title-gotham padding-5">to</Label>
                      <TextField
                        name="sat_time_to"
                        type="time"
                        defaultValue="22:00"
                        className='form-control-react'
                      />
                      </div>
                    </Col>
                </Col>

                <Col md={6} className="margin-top-10">
                  <Checkbox
                    className="chkbx-label"
                    label="Sun"/>
                    <Col md={12}>
                      <div style={{ display: 'inline-flex' }}>
                      <TextField
                        name="sun_time_from"
                        type="time"
                        defaultValue="09:45"
                        className='form-control-react'
                      />
                      <Label className="label-dft strong-6 title-gotham padding-5">to</Label>
                      <TextField
                        name="sun_time_to"
                        type="time"
                        defaultValue="22:00"
                        className='form-control-react'
                      />
                      </div>
                    </Col>
                </Col>

              </Col>
            </div>
            {/*<Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                {/*<TextField
                  disabled={true}
                  name="brandID"
                  floatingLabelText="Brand"

                  /><br />
                  <SelectField
                  value={(data.selectedRecord.brandID) ? data.selectedRecord.brandID: this.state.brandID}
                  onChange={this.handleBrandChange}
                  className='textfield-regular'
                  floatingLabelText="Brand"
                  hintText="Select Brand">
                  {category}
                </SelectField><br />*/}
                {/*<SelectField
                  value={(data.selectedRecord.branchCode) ? data.selectedRecord.branchCode: this.state.branchCode}
                  onChange={this.handleBranchCodeChange}
                  className='textfield-regular'
                  floatingLabelText="Branch Code"
                  hintText="Select Branch Code"
                  autoWidth={false}>
                  {branchCodeList}
                </SelectField><br />*/}
                {/*<div className='content-container'>
                  <div className='item-image-box'>
                    <ImageUpload image={null}
                      onImageChange={this.props.onImageChange}
                      image={(data.selectedRecord.image) ? data.selectedRecord.image: '' }
                      imageModule={this.props.imageModule}
                    info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                  </div>
                </div>
                <DropDownMenu
                  value={this.state.locCategoryID}
                  onChange={this.handleLocationCategoryChange}
                  autoWidth={true}
                  className='dropdownButton-fixed-400'
                >
                  {locationCategoryList}
                </DropDownMenu>
                <TextField
                  name="name"
                  hintText="Enter Location Name"
                  floatingLabelText="Location Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <DefaultTextArea
                  name="address"
                  hintText="Enter Address"
                  floatingLabelText="Address"
                  defaultValue={(data.selectedRecord.address) ? data.selectedRecord.address : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                /><br />
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="latitude"
                  type="number"
                  hintText="Enter Latitude"
                  floatingLabelText="Latitude"
                  defaultValue={(data.selectedRecord.latitude) ? data.selectedRecord.latitude : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="longitude"
                  type="number"
                  hintText="Enter Longitude"
                  floatingLabelText="Longitude"
                  defaultValue={(data.selectedRecord.longitude) ? data.selectedRecord.longitude : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <DefaultTextArea
                  name="businessHrs"
                  hintText="Enter Business Hours"
                  floatingLabelText="Business Hours"
                  defaultValue={(data.selectedRecord.businessHrs) ? data.selectedRecord.businessHrs : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                /><br />
                <TextField
                  name="branchCode"
                  hintText="Enter Branch Code"
                  floatingLabelText="Branch Code"
                  defaultValue={(data.selectedRecord.branchCode) ? data.selectedRecord.branchCode : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="phone"
                  hintText="Enter Phone"
                  floatingLabelText="Phone"
                  defaultValue={(data.selectedRecord.phone) ? data.selectedRecord.phone : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="email"
                  hintText="Enter Email"
                  floatingLabelText="Email"
                  defaultValue={(data.selectedRecord.email) ? data.selectedRecord.email : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <Checkbox
                  label="locFlag"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.locFlag == 'active') ? true : false) : false }
                  onCheck={this.onLocFlagCheck}
                />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>*/}
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default LocationForm;
