'use strict';

import React, {
  Component,
  PropTypes
} from 'react';
import { Link } from 'react-router';

import Term from 'material-ui/svg-icons/action/help-outline';
import Voucher from 'material-ui/svg-icons/action/card-giftcard';
import LevelIcon from 'material-ui/svg-icons/action/supervisor-account';
import Business from 'material-ui/svg-icons/communication/business';
import Chat from 'material-ui/svg-icons/communication/chat-bubble';
import Cart from 'material-ui/svg-icons/action/shopping-basket';
import Assignment from 'material-ui/svg-icons/action/assignment';
import Store from 'material-ui/svg-icons/action/store';
import Category from 'material-ui/svg-icons/action/group-work';
import Subcategory from 'material-ui/svg-icons/action/reorder';
import Star from 'material-ui/svg-icons/action/stars';
import Branch from 'material-ui/svg-icons/communication/location-on';
import Loyalty from 'material-ui/svg-icons/action/loyalty';
import New from 'material-ui/svg-icons/av/fiber-new';
import Device from 'material-ui/svg-icons/device/devices';
import Lock from 'material-ui/svg-icons/action/lock-outline';
import Info from 'material-ui/svg-icons/action/info';
import Setting from 'material-ui/svg-icons/action/settings';
import Level from 'material-ui/svg-icons/av/recent-actors';
import Person from 'material-ui/svg-icons/social/person';
import Account from 'material-ui/svg-icons/action/account-circle';
import LocalOffer from 'material-ui/svg-icons/maps/local-offer';
import LocationCity from 'material-ui/svg-icons/social/location-city';
import Description from 'material-ui/svg-icons/action/description';
import Share from 'material-ui/svg-icons/social/share';
import Cake from 'material-ui/svg-icons/social/cake';
import Place from 'material-ui/svg-icons/maps/place';
import Notifications from 'material-ui/svg-icons/social/notifications';
import InsertComment from 'material-ui/svg-icons/editor/insert-comment';
import ShoppingBasket from 'material-ui/svg-icons/action/shopping-basket';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import Config from '../../config/base';
import jwtDecode from 'jwt-decode';
import InfoComponent from '../common/InfoComponent';

import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';

import FontIcon from 'material-ui/FontIcon';

import Header from './VerticalHeader';


class LinkComponent extends React.Component {

  state = {
    activeKey: 1,
    open: false,
    role: '',
    tabs: []
  }

  handleSelect = (selectedKey) => {
    this.setState({activeKey: selectedKey});

    switch (selectedKey) {
      case 1:
        this.props.router.push('/');
        break;

      // case 2.1:
      //   this.props.router.push('/level');
      //   break;
      // case 2.2:
      //   this.props.router.push('/levelcategory');
      //   break;
      case 2.1:
        this.props.router.push('/orders');
        break;
      // case 3.2:
      //   this.props.router.push('/subcategory');
      //   break;
      case 2.2:
        this.props.router.push('/orderarchive');
        break;
      // case 3:
      //   this.props.router.push('/product');
      //   break;
      case 3:
        this.props.router.push('/category');
        break;
      case 4:
        this.props.router.push('/product');
        break;
      case 5:
        this.props.router.push('/inventory');
        break;
      case 6:
        this.props.router.push('/branch');
        break;
      case 7:
        this.props.router.push('/city');
        break;
      case 8:
        this.props.router.push('/post');
        break;
      case 9:
        this.props.router.push('/announcement');
        break;
      case 10:
        this.props.router.push('/push');
        break;
      // case 10:
      //   this.props.router.push('/socialmedia');
      //   break;
      // case 4:
      //   this.props.router.push('/brand');
      //   break;

      case 11:
        this.props.router.push('/voucher');
        break;
      // case 8:
      //   this.props.router.push('/sku');
      //   break;
      // case 11:
      //   this.props.router.push('/setting');
      //   break;
      // case 11:
      //   this.props.router.push('/setting/EARNqh670VGtcpj');
      //   break;
      // case 12:
      //   this.props.router.push('/push');
      //   break;
      case 12:
        this.props.router.push('/account');
        break;

      default:
        return;

    }
  }

  handleRouteReload = (pathname) => {

    // if (pathname == '/level') {
    //   this.setState({activeKey: 2.1});
    // }
    // else if (pathname.includes('/levelcategory')) {
    //   this.setState({activeKey: 2.2});
    // }
    if (pathname == '/orders') {
      this.setState({activeKey: 2.1});
    }
    // else if (pathname.includes('/subcategory')) {
    //   this.setState({activeKey: 3.2});
    // }
    else if (pathname.includes('/orderarchive')) {
      this.setState({activeKey: 2.2});
    }
    else if (pathname == '/category') {
      this.setState({activeKey: 3});
    }
    // else if (pathname.includes('/product')) {
    //   this.setState({activeKey: 3});
    // }
    else if (pathname.includes('/product')) {
      this.setState({activeKey: 4});
    }
    // else if (pathname.includes('/brand')) {
    //   this.setState({activeKey: 4});
    // }
    else if (pathname.includes('/inventory')) {
      this.setState({activeKey: 5});
    }
    else if (pathname.includes('/branch')) {
      this.setState({activeKey: 6});
    }
    else if (pathname.includes('/city')) {
      this.setState({activeKey: 7});
    }
    else if (pathname.includes('/post')) {
      this.setState({activeKey: 8});
    }
    else if (pathname.includes('/announcement')) {
      this.setState({activeKey: 9});
    }
    else if (pathname.includes('/push')) {
      this.setState({activeKey: 10});
    }
    else if (pathname.includes('/voucher')) {
      this.setState({activeKey: 11});
    }
    // else if (pathname.includes('/socialmedia')) {
    //   this.setState({activeKey: 10});
    // }
    // else if (pathname.includes('/tablet')) {
    //   this.setState({activeKey: 10});
    // }
    // else if (pathname.includes('/sku')) {
    //   this.setState({activeKey: 8});
    // }
    // else if (pathname.includes('/setting')) {
    //   this.setState({activeKey: 11});
    // }
    // else if (pathname.includes('/setting/EARNqh670VGtcpj')) {
    //   this.setState({activeKey: 11});
    // }

    else if (pathname.includes('/account')) {
      this.setState({activeKey: 12});
    }
    else if (pathname == '/') {
      this.setState({activeKey: 1});
    }
    console.info('LinkComponent handleRouteReload', pathname, this.state);
  }

  componentWillMount(){
    let {pathname} = this.props.router.location;
    this.handleRouteReload(pathname);

    let user = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    this.setState({
      role: user.role,
      tabs: user.cmsTabs.split(',')
    });
  }

  handleClick = (event) => {
    const {actions} = this.props;
    actions.logoutUser(this.props.router);
  }

  handleInfo = (event) => {
    this.setState({open: true});
  }

  handleClose = () => {
    this.setState({open:false});
  }

  handleAccount = () => {
    const {router} = this.props;
    this.setState({activeKey:0});
    router.push('/credential');
  }

  linkClass = (menu) => {
    // console.log("LinkComponent menu", menu);
    const {role, tabs} = this.state;
    // console.log("Role,Tabs", role, tabs);
    if (tabs[0] == 'all') {
      return 'visible';
    }else
    // console.log("dasdsa", tabs.includes(menu));
      return (tabs.includes(menu)) ? 'visible' : 'hide';
  }

  groupLinkClass = (menus) => {
    // console.log("groupLinkClass menu", menus);
    const {role, tabs} = this.state;
    // console.log("Group Role,Tabs", role, tabs);
    if (tabs[0] == 'all') {
      return 'visible';
    }
    else {
      let contains = 0;
      menus.forEach( x => {
        if (tabs.includes(x)) contains++;
      });
      return (contains) ? 'visible' : 'hide';
    }
  }

  render() {
    const {role, tabs} = this.state;
    // console.log("render role tabs", role, tabs);
    return (
      <div className="hidden-xs nav-fixed">
        <Header router={this.props.router}>
          <div className='nav-bar-container'>
            <ul className='nav-bar'>
              <Nav stacked activeKey={this.state.activeKey} onSelect={this.handleSelect}>
                <NavItem eventKey={1} className={this.linkClass('profile')}>
                  <Business className='nav-icon'/>Dashboard
                </NavItem>
                {/*<NavItem eventKey={2} className={this.linkClass('level')}>
                  <LevelIcon className='nav-icon'/>Level
                  </NavItem>
                  <NavDropdown id="basic-nav-dropdown" eventKey={2} className={this.groupLinkClass(['level', 'levelcategory'])}
                    title={<span><Store className='nav-icon'/>Level</span>}>
                    <MenuItem eventKey={2.1} className={this.linkClass('level')}>
                      <LevelIcon className='nav-icon-sub'/>Level</MenuItem>
                    <MenuItem eventKey={2.2} className={this.linkClass('levelcategory')}>
                      <Subcategory className='nav-icon-sub'/>Level Category</MenuItem>
                    </NavDropdown>*/}
                {/*<NavItem eventKey={3} className={this.linkClass('level')}>
                  <LevelIcon className='nav-icon'/>Offers
                  </NavItem>*/}
                <NavDropdown id="basic-nav-dropdown" eventKey={3} className={this.groupLinkClass(['orders', 'product'])}
                  title={<span><Cart className='nav-icon'/>Orders</span>}>
                  <MenuItem eventKey={2.1} className={this.linkClass('orders')}>
                    All Orders</MenuItem>
                  {/*<MenuItem eventKey={3.2} className={this.linkClass('levelcategory')}>
                    <Subcategory className='nav-icon-sub'/>Subcategory</MenuItem>*/}
                  <MenuItem eventKey={2.2} className={this.linkClass('orderarchive')}>
                    Order Archive</MenuItem>
                  </NavDropdown>
                <NavItem eventKey={3} className={this.linkClass('category')}>
                  <Category className='nav-icon'/>Category
                  </NavItem>
                {/*<NavItem eventKey={3} className={this.linkClass('product')}>
                  <Store className='nav-icon'/>Featured Products
                  </NavItem>*/}
                <NavItem eventKey={4} className={this.linkClass('product')}>
                  <Cake className='nav-icon'/>Products
                  </NavItem>
                <NavItem eventKey={5} className={this.linkClass('inventory')}>
                  <Assignment className='nav-icon'/>Inventory
                  </NavItem>
                <NavItem eventKey={6} className={this.linkClass('branch')}>
                  <Store className='nav-icon'/>Branches
                  </NavItem>
                <NavItem eventKey={7} className={this.linkClass('city')}>
                  <Place className='nav-icon'/>Delivery Locations
                  </NavItem>
                <NavItem eventKey={8} className={this.linkClass('post')}>
                  <New className='nav-icon'/>What&#39;s New
                  </NavItem>
                {/*<NavDropdown id="basic-nav-dropdown" eventKey={6} className={this.groupLinkClass(['locationcategory', 'location'])}
                  title={<span><Branch className='nav-icon'/>Locations</span>}>
                  <MenuItem eventKey={6.1} className={this.linkClass('locationcategory')}>
                    <Category className='nav-icon-sub'/>Location Category</MenuItem>
                  <MenuItem eventKey={6.2} className={this.linkClass('location')}>
                    <Branch className='nav-icon-sub'/>Locations</MenuItem>
                  </NavDropdown>
                <NavItem eventKey={6} className={this.linkClass('location')}>
                  <Branch className='nav-icon'/>Locations
                  </NavItem>*/}
                <NavItem eventKey={9} className={this.linkClass('announcement')}>
                  <Notifications className='nav-icon'/>Announcement
                  </NavItem>
                <NavItem eventKey={10} className={this.linkClass('push')}>
                  <InsertComment className='nav-icon'/>Push Notification
                  </NavItem>
                <NavItem eventKey={11} className={this.linkClass('voucher')}>
                  <Voucher className='nav-icon'/>Vouchers
                  </NavItem>
                {/*<NavItem eventKey={9} className={this.linkClass('term')}>
                  <Description className='nav-icon'/>Terms and Conditions
                  </NavItem>
                <NavItem eventKey={10} className={this.linkClass('socialmedia')}>
                  <Share className='nav-icon'/>Social Media
                  </NavItem>
                <NavItem eventKey={10} className={this.linkClass('tablet')}>
                  <Device className='nav-icon'/>Tablet Management
                  </NavItem>
                <NavItem eventKey={8} className={this.linkClass('sku')}>
                  <Assignment className='nav-icon'/>SKU
                  </NavItem>
                <NavItem eventKey={11} className={this.linkClass('setting')}>
                  <Setting className='nav-icon'/>Earn Settings
                  </NavItem>
                {/*<NavItem eventKey={11} className={this.linkClass('setting/EARNqh670VGtcpj')}>
                  <Setting className='nav-icon'/>Earn Settings
                  </NavItem>*/}

                <NavItem eventKey={12} className={this.linkClass('account')}>
                  <Account className='nav-icon'/>Accounts
                  </NavItem>
              </Nav>
            </ul>
          </div>
        </Header>

        <div className="footer-container">
          <div className="footer-btn footer-left" onClick={this.handleAccount}>
            <Account className='footer-icon'/>
          </div>
          <div className="footer-btn" onClick={this.handleInfo}>
            <Info className='footer-icon'/>
          </div>
          <div className="footer-btn" onClick={this.handleClick}>
            <Lock className='footer-icon'/>
          </div>
        </div>

        {/* Info */}
        <InfoComponent
          open={this.state.open}
          onRequestClose={this.handleClose}/>
      </div>
    )
  }
}

LinkComponent.displayName = 'NavigationLinkComponent';

// Uncomment properties you need
// LinkComponent.propTypes = {};
// LinkComponent.defaultProps = {};
//  <li><Link to="" onClick={ () => handleClick(property) }>Logout</Link></li>

export default LinkComponent;
