/**
 * Created by jonna on 10/04/18.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import AppStyle from '../../styles/Style.js';

class CityForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      city: ''
    };
  }

  componentWillMount(){
    const {actions, data} = this.props;
    // actions.getCategory();
    // for add
    if (!this.props.shouldEdit){
      this.setState({
        shouldDisplay: true,
      });
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    const {data} = this.props;
    console.log('City Form data', data);

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <div>
              <Col className="modal-container">
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">City : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="city" id="city" placeholder="" defaultValue={(data.selectedRecord.city) ? data.selectedRecord.city : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                </Col>
                <Col md={9}>
                  <Checkbox
                    label="Status"
                    className='chkbx-label'
                    defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                    onCheck={this.onCheck}
                  />
                </Col>
              </Col>
            </div>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default CityForm;
