/**
 * Created by jonna on 10/04/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';
// import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import CityForm from './CityForm';


class CityEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cities: {
        city: '',
        status: false
      },
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.cityID) {
      actions.viewCity(this.props.params.cityID);
    }
    console.log('actions', actions);
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.cityID) {
      if (!_.isEmpty(nextProps.cityState.selectedRecord)) {
        this.setState({
          cities: nextProps.cityState.selectedRecord
        });
      }
    }
  }


  // handle going back
  handleReturn = () => {
    this.props.router.push('/city');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput2(this.state.cities)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_CITY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateCity(this.state.cities, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.cities)) {
      console.log("handleAdd");
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_CITY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addCity(this.state.cities, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let city = this.state.cities;
    city[field] = e.target.value;
    console.log("handleData City", city[field]);
  }


  validateInput = (data) => {
    const {dialogActions} = this.props;

    if (data.city == '') {
      dialogActions.openNotification('Oops! No city found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

  }

  // Validation for Update button
  validateInput2 = (data) => {
    const {dialogActions} = this.props;

    if (data.city == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
  }

  // handle status locFlag
  handleLocFlagChange = (e) => {
    this.state.cities['locFlag'] = e;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.cities['status'] = e;
    console.log("handleStatusChange", e);
  }

  // close Modal
  handleClose = () => {
    this.setState({open: false});
  };


  render(){
    const {actions} = this.props;
    console.log('city', this.state.cities);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.cityID) ? 'Edit City' : 'Add City'}</h2>
        <Divider className='content-divider2' />

        { /* Action Buttons */ }
        {/*<ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }*/}

        { /* Form */ }
        <Dialog
          title={ (this.props.params.cityID) ? 'Edit City' : 'Create New City'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <CityForm
            data={this.props.cityState}
            shouldEdit={ (this.props.params.cityID) ? true : false }
            onChange={this.handleData}
            actions={actions}
            onStatusChange={this.handleStatusChange}
            onLocFlagChange={this.handleLocFlagChange}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.cityID) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    cityState: state.cityState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cityAction: require('../../actions/cityAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.cityAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CityEditor);
