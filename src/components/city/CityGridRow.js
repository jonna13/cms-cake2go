import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import AppStyles from '../../styles/Style';

class GridRowComponent extends Component {
  render(){
    let item = this.props.items;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <EditIcon />
          </IconButton>
        </td>
        <td>
          <label className='capital'> {item.status}</label>
        </td>
        <td>
          {item.city}
        </td>
        <td>{item.description}</td>
        <td>
          View Streets
        </td>
      </tr>
    );
  }
}

export default GridRowComponent;
