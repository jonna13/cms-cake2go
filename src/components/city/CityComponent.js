'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton, ViewButton, InactiveButton, ActiveButton, ShowAllButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import Search from 'material-ui/svg-icons/action/search';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';

import CityList from './CityList';

class CityComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/city_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/city/'+item.cityID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.citys;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.citys;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.citys;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.citys;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.citys;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    console.info('CityComponent: param', params);
    actions.getLocations(params);
  }

  render() {
    const {pageinfo} = this.props.data.citys;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Delivery Locations</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.citys.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.citys.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.citys.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

        <Card className="order-card">
          <Form>
            <Row>
              <Col md={12} className="inline-block">
                <AddButton
                  handleOpen={this.handleAdd}/>&nbsp;&nbsp;
                <ShowAllButton
                  />&nbsp;&nbsp;
                <ActiveButton
                  />&nbsp;&nbsp;
                <InactiveButton
                  />

                <Label className="blue-icon pull-right-responsive search"><Search className='nav-icon'/></Label>
                <Input type="text" name="search" placeholder="" className="search-prod"/>

              </Col>
            </Row>
          </Form>
        </Card>

        { /* List */ }
        <CityList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

CityComponent.displayName = 'LocationCityComponent';

// Uncomment properties you need
// CityComponent.propTypes = {};
// CityComponent.defaultProps = {};

export default CityComponent;
