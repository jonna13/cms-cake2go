/**
 * Created by jonna on 10/04/18.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import GridRow from './CityGridRow';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

class CityList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getCitys(params);
  }

  render(){
    let {citys} = this.props.data;
    const headers = [
      { title: '', value: 'dateAdded'},
      { title: 'Status', value: 'status'},
      { title: 'City', value: 'city'},
      { title: '', value: ''}
    ];

    var rows = [];
    if (citys.records.length > 0) {
      citys.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.citys}
            onGetData={this.handleGetData}
            hasImage={false}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.citys}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default CityList;
