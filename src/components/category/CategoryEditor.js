/**
 * Created by jonna on 2/02/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';

import CategoryForm from './CategoryForm';

class CategoryEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      category: {
        name: '',
        description: '',
        status: '',
        image: '',
      },
      uploadImage: {},
      imageModule: Config.IMAGE.CATEGORY,
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    console.log('category actions', actions);
    if (this.props.params.id) {
      actions.viewCategory(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.categoryState.selectedRecord)) {
        this.setState({
          category: nextProps.categoryState.selectedRecord
        });
      }
    }
  }

  // handle going back to Category
  handleReturn = () => {
    this.props.router.push('/category');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.category)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_CATEGORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateCategory(this.state.category, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.category)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_CATEGORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addCategory(this.state.category, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.status == '') {
      dialogActions.openNotification('Oops! Please select status', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    // if (data.description == '') {
    //   dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
    //   return false;
    // }
    if (this.state.category.image == '') {
      if (_.isEmpty(this.state.uploadImage)) {
        dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let category = this.state.category;
    category[field] = e.target.value;
    console.log('Data Field', category[field]);
  }

  // handle change in type
  handleTypeChange = (e) => {
    this.state.category['type'] = e;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status select
  handleStatusChange = (e) => {
    this.state.category['status'] = e;
    console.log('Status', e);
  }

  // handleOpen = () => {
  //   this.setState({open: true});
  // };

  //close modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){

    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Category' : 'Create New Category'}</h2>
        <Divider className='content-divider2' />

        { /* Action Buttons */ }
        {/*<ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }*/}

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? 'Edit Category' : 'Create New Category'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <CategoryForm
            data={this.props.categoryState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            onTypeChange={this.handleTypeChange}
            onStatusChange={this.handleStatusChange}
            onImageChange={this.handleImageChange}
            imageModule={this.state.imageModule}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    categoryState: state.categoryState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    categoryAction: require('../../actions/categoryAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.categoryAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryEditor);
