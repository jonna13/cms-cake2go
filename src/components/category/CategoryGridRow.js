import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Checkbox from 'material-ui/Checkbox';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import AppStyles from '../../styles/Style';
import Badge from 'material-ui/Badge';

class CategoryGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <MoreVertIcon />
          </IconButton>
        </td>
        <td>
          <Checkbox
            className="inherit"/>
        </td>
        <td>
          <label className='capital'>{item.status}</label>
        </td>
        <td className="pointer blue-text" onClick={()=> { this.props.onEditClick(item); }}>
          {item.name}
        </td>
        <td>
          <label className='green round white'>2</label>
          <label className='red round white'>2</label>
        </td>
      </tr>
    );
  }
}

export default CategoryGridRow;
