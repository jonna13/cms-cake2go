'use strict';

import React from 'react';
import Config from '../../config/base';
import { ValueTextArea } from '../common/fields';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import {SendButton} from '../common/buttons';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';


class PushComponent extends React.Component {
  state = {
    push: {
      message: '',
      type: 'normal'
    }
  }

  onChange = (event) => {
    const field = event.target.name;
    const details = this.state.push;
    details[field] = event.target.value;
    this.setState({ push: { message: event.target.value } });
  }

  handleSend = () => {
    console.info('PushComponent', this.state);
    if (this.validateInput(this.state.push)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.SEND_PUSH_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.sendPush(this.state.push);
            this.setState({ push: { message: '' } });
          }
        });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.message == '') {
      dialogActions.openNotification('Oops! No message found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    return true;
  }

  render() {
    return (
      <div>
        <h2 className='content-heading'>Push Notification</h2>
        <Divider className='content-divider2' />
        { /* Action Buttons */ }
        {/*<SendButton
            handleOpen={this.handleSend}/>*/}

        <Card className="order-card">
          <Form>
            <Row>
              <Col md={12} className="inline-block">
                <ValueTextArea
                  name="message"
                  hintText="Enter Message"
                  floatingLabelText="Message"
                  onChange={this.onChange}
                  value={this.state.push.message}
                  rows={5}
                  wide={true}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />
                <SendButton
                  handleOpen={this.handleAdd}/>
              </Col>
            </Row>
          </Form>
        </Card>
      </div>
    );
  }
}

PushComponent.displayName = 'PushPushComponent';

// Uncomment properties you need
// PushComponent.propTypes = {};
// PushComponent.defaultProps = {};

export default PushComponent;
