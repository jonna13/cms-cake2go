/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';

import ProductForm from './ProductForm';

class ProductEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      product: {
        name: '',
        productCode: '',
        description: '',
        categoryID: '',
        categoryName: '',
        tag: '',
        image: [],
        status: '',
        price: '',
        addons: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.PRODUCT,
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewProduct(this.props.params.id);
    }
    actions.getCategorys();
    actions.getAddOns();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.productState.selectedRecord)) {
        this.setState({
          product: nextProps.productState.selectedRecord
        });
      }
    }
  }

  // handle going back to Product
  handleReturn = () => {
    this.props.router.push('/product');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.product)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_PRODUCT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateProduct(this.state.product, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.product)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_PRODUCT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            console.log('data', this.state.uploadImage);
            let {actions} = this.props;
            actions.addProduct(this.state.product, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    console.log("validate data", data);
    const {dialogActions} = this.props;

    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.productCode == '') {
      dialogActions.openNotification('Oops! No product code found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.status == '') {
      dialogActions.openNotification('Oops! No status found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.categoryName == '') {
      dialogActions.openNotification('Oops! Please select category', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.tag == '') {
      dialogActions.openNotification('Oops! Please select tag', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.price == '') {
      dialogActions.openNotification('Oops! No price found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    // if (data.description == '') {
    //   dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
    //   return false;
    // }
    if (this.state.product.image == '') {
      if (_.isEmpty(this.state.uploadImage)) {
        dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
    }

    let newDescription = data.description.replace(/<br\/>/g, "\n");
    this.state.product['description'] = newDescription;

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let product = this.state.product;
    product[field] = e.target.value;
    console.log("handleData", product[field]);
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
    console.log("upload editor", e);
  }

  // handle tag checkbox
  handleTagChange = (e) => {
    this.state.product['tag'] = e;
    console.log('Tag', e);
  }

  handleChangeBrand = (e) => {
    this.state.product['brandID'] = e;
  }

  // handle category
  handleCategoryChange = (e, name) => {
    this.state.product['categoryID'] = e;
    this.state.product['categoryName'] = name;
  }

  // handle category select
  handleCategory2Change = (e) => {
    this.state.product['categoryName'] = e;
    console.log('Category', e);
  }

  // handle status select
  handleTagChange = (e) => {
    this.state.product['tag'] = e;
    console.log("tag", e);
  }

  // handle status select
  handleStatusChange = (e) => {
    this.state.product['status'] = e;
    console.log('Status', e);
  }

  // handle addons checkbox
  handleAddOnChange = (e) => {
    this.state.product['addons'] = e;
    console.log('Addons Editor', e);
  }

  // handleAddOnChange = (val) => {
  //   let addon = JSON.stringify(val);
  //   let newAddon = addon.replace (/"/g,'');
  //   let newAddon2 = newAddon.replace (/\[/g,'').replace(/\]/g,'');
  //   console.log("handleAddOnChange", newAddon2);
  //   this.state.product['addons'] = newAddon2;
  // }

  // close Modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    const {actions} = this.props;
    console.log("addon array", this.state.product.addons);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Product' : 'Create New Product'}</h2>
        <Divider className='content-divider2' />

        { /* Action Buttons */ }
        {/*<ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }*/}

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? 'Edit Product' : 'Create New Product'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <ProductForm
            data={this.props.productState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            actions={actions}
            onCategoryChange={this.handleCategoryChange}
            onCategory2Change={this.handleCategory2Change}
            onChangeBrand={this.handleChangeBrand}
            onStatusChange={this.handleStatusChange}
            onAddOnChange={this.handleAddOnChange}
            onTagChange={this.handleTagChange}
            onImageChange={this.handleImageChange}
            imageModule={this.state.imageModule}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    productState: state.productState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    productAction: require('../../actions/productAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.productAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductEditor);
