/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import TextField from 'material-ui/TextField';
import { DefaultTextArea } from '../common/fields';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import { Grid, Row, Col, Radio } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import MultipleImageUpload from '../common/MultipleImageUpload';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
// import {Checkbox, CheckboxGroup} from 'react-checkbox-group';


class ProductForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      productCode: '',
      description: '',
      categoryID: '',
      categoryName: '',
      tag: '',
      image: [],
      status: '',
      price: '',
      addons: ''
    };
  }

  componentWillMount(){
    const {actions} = this.props;
    // console.log('actions', actions);
    actions.getCategorys();
    actions.getAddOns();
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        categoryID: nextProps.data.selectedRecord.categoryID,
        categoryName: nextProps.data.selectedRecord.categoryName,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleCategoryChange = (e, idx, categoryID) => {
    const {actions} = this.props;
    let name = this.props.data.categorys.filter(x=> { return x.categoryID == categoryID})[0].name;
    this.setState({categoryID});
    this.props.onCategoryChange(categoryID, name);
  };

  handleCategory2Change = (e, idx, status) => {
    console.log('category', e.target.value);
    this.setState({category: e.target.value});
    this.props.onCategory2Change(e.target.value);
  }

  handleBrandChange = (e, idx, brandID) => {
    this.setState({brandID});
    this.props.onChangeBrand(brandID);
  }

  handleStatusChange = (e, idx, status) => {
    console.log('status', e.target.value);
    this.setState({status: e.target.value});
    this.props.onStatusChange(e.target.value);
  }

  onCheck = (e, val) =>{
    // console.log(this.refs.check_me.checked);
    this.props.onAddOnChange(val);
    console.log("onCheck", val);
  }

  onChange = (e, val) => {
    this.props.onTagChange(val);
  }

  // handleAddOnChange = (addons) => {
  //   this.setState({
  //     newAddons: addons
  //   });
  //   console.log("addons", addons);
  //   this.props.onAddOnChange(addons);
  // }

  render(){
    let categoryList = [];
    const {data} = this.props;
    {/*console.log('product data', data);*/}

    {/*var category = [];
        if (data.products.category.length > 0) {
          data.products.category.forEach((val, key) => {
            category.push(
              <MenuItem
              key={key}
              value={val.brandID}
              primaryText={val.name}
              />
            );
          });
        }*/}
    console.log("Product Data", data);

    if (data.categorys) {
      categoryList.push(<MenuItem key={0} value='' primaryText='Select Category' />);
      data.categorys.map( (val, idx) => {
        categoryList.push(<MenuItem key={(idx+1)} value={val.categoryID} primaryText={val.name} />);
      });
    }

    // if (this.props.data.categories) {
    //   categoryList.push(<MenuItem key={0} value='' primaryText='Select Category' />);
    //   this.props.data.categories.map( (val, idx) => {
    //     categoryList.push(<MenuItem key={(idx+1)} value={val.categoryID} primaryText={val.name} />);
    //   });
    // }

    if (this.state.shouldDisplay){

      return(
        <Grid fluid>
          <Row>
            <div>
              <Col className="modal-container">
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Product Name : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Product Code : </Label>
                </Col>
                <Col md={4}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="number" name="productCode" id="productCode" placeholder="" defaultValue={(data.selectedRecord.productCode) ? data.selectedRecord.productCode : '' }
                    onChange={this.props.onChange}  className="fullwidth uppercase"/>
                  </FormGroup>
                </Col>
                <Col md={2}>
                  <Label className="label-dft strong-6 title-gotham pull-right-responsive">Status : </Label>
                </Col>
                <Col md={3}>
                  <FormGroup>
                    <Input type="select" name="status" id="status" value={(data.selectedRecord.status) ? data.selectedRecord.status : this.state.status} onChange={this.handleStatusChange}>
                      <option value=""> Please Select Status </option>
                      <option value="active"> Active </option>
                      <option value="inactive"> Inactive </option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Category Name : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup className="form-group-no-space">
                    <DropDownMenu
                      value={this.state.categoryID}
                      onChange={this.handleCategoryChange}
                      autoWidth={true}
                      className='form-control-react'>
                      {categoryList}
                    </DropDownMenu>
                    <br/>
                    <br/>
                    {/*<Input type="select" name="categoryName" id="categoryName" value={(data.selectedRecord.categoryName) ? data.selectedRecord.categoryName : this.state.categoryName} onChange={this.handleCategory2Change}>
                      <option value=""> Please Select a Category </option>
                      <option value="cake2comfort"> Cakes 2 Comfort </option>
                      <option value="twist2perf"> Twist 2 Perfection </option>
                      <option value="new2taste"> New 2 Taste </option>
                      <option value="barscookiessingles"> Bars, Cookies & Singles </option>
                      <option value="addons"> Add Ons </option>
                    </Input>*/}
                  </FormGroup>
                </Col>
                {/*<Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Available on Branches : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label hidden></Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Input type="select" name="status" id="status" className="half">
                      <option> - Select Branch - </option>
                      <option> Pasay </option>
                      <option> Quezon City </option>
                    </Input>
                  </FormGroup>
                </Col>*/}
                <Col md={12}>
                  <Col md={3} style= {{ paddingLeft: '0px' }}>
                    <Label className="label-dft strong-6 title-gotham">Tag : </Label>
                  </Col>
                  <Col md={4}>
                    <RadioButtonGroup name="tag" defaultSelected={(data.selectedRecord.tag) ? data.selectedRecord.tag : '' }
                      value={(data.selectedRecord.tag) ? data.selectedRecord.tag : this.state.tag} onChange={this.onChange}>
                      <RadioButton
                        value="new"
                        name="new"
                        className="inherit-font"
                        label="New"
                      />
                      <RadioButton
                        value="regular"
                        name="regular"
                        className="inherit-font"
                        label="Regular"
                      />
                      <RadioButton
                        value="not_available"
                        name="not_available"
                        className="inherit-font"
                        label="Not Available"
                      />
                      <RadioButton
                        value="bestseller"
                        name="bestseller"
                        className="inherit-font"
                        label="Bestseller"
                      />
                    </RadioButtonGroup>
                    <br/>
                  </Col>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Price : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Input type="number" name="price" id="price" placeholder="" defaultValue={(data.selectedRecord.price) ? data.selectedRecord.price : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={12}>
                  {/*<Col md={3} style= {{ paddingLeft: '0px' }}>
                    <Label className="label-dft strong-6 title-gotham">Add Ons : </Label>
                  </Col>
                  <Col md={12}>
                    <CheckboxGroup
                      checkboxDepth={2}
                      name="addons"
                      value={this.state.newAddons}
                      onChange={this.handleAddOnChange}>

                      <label className="inherit-font"><Checkbox value="gift_card"/>&nbsp;&nbsp; Gift Card</label><br/>
                      <label className="inherit-font"><Checkbox value="custom_dedication"/>&nbsp;&nbsp; Customize Dedication</label><br/>
                      <label className="inherit-font"><Checkbox value="buy_candle"/>&nbsp;&nbsp; Buy Candle</label><br/>
                      <label className="inherit-font"><Checkbox value="bday_candle"/>&nbsp;&nbsp; Happy Birthday Candle</label>
                    </CheckboxGroup>*/}

                      <Checkbox
                        className="chkbx-label"
                        label="Has Addons?"
                        value="addons"
                        defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.addons == 'true') ? true : false) : false }
                        onCheck={this.onCheck} />
                  {/*</Col>
                  <Col md={5}>
                      <Checkbox
                        className="chkbx-label"
                        label="Buy Candle"/>
                      <Checkbox
                        className="chkbx-label"
                        label="Happy Birthday Candle"/>
                  </Col>*/}
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Description </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="textarea" name="description" id="description" placeholder="" defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                    onChange={this.props.onChange}  className="fullwidth big-textarea"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Image </Label>
                </Col>
                <Col md={9}>
                  <div className='item-image-box'>
                    <ImageUpload image={null}
                      onImageChange={this.props.onImageChange}
                      image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                      imageModule={this.props.imageModule}
                      info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                    {/*<MultipleImageUpload image={null}
                      onImageChange={this.props.onImageChange}
                      image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                      imageModule={this.props.imageModule}
                      info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>*/}
                  </div>
                </Col>
              </Col>
            </div>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default ProductForm;
