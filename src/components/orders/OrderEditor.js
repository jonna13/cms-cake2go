/**
 * Created by jonna on 2/02/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';

import OrderForm from './OrderForm';

class OrderEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      order: {
        name: '',
        status: ''
      },
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.transactionID) {
      actions.viewOrder(this.props.params.transactionID);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.transactionID) {
      if (!_.isEmpty(nextProps.orderState.selectedRecord)) {
        this.setState({
          order: nextProps.orderState.selectedRecord
        });
      }
    }
  }

  // handle going back to Order
  handleReturn = () => {
    this.props.router.push('/orders');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.order)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_ORDER_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateOrder(this.state.order, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.order)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_ORDER_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addOrder(this.state.order, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let order = this.state.order;
    order[field] = e.target.value;
  }

  // handle change in type
  handleTypeChange = (e) => {
    this.state.order['type'] = e;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.order['status'] = e;
  }

  // close Modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    console.log("paymentStatus", this.props.params);

    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.transactionID) ? 'Edit Order' : 'Add Order'}</h2>
        <Divider className='content-divider2' />

        { /* Action Buttons */ }
        {/*<ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.transactionID) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }*/}

        { /* Form */ }
        <Dialog
          title={ (this.props.params.transactionID) ? 'Edit Product' : 'Create New Product'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <OrderForm
            data={this.props.orderState}
            shouldEdit={ (this.props.params.transactionID) ? true : false }
            onChange={this.handleData}
            onTypeChange={this.handleTypeChange}
            onStatusChange={this.handleStatusChange}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    orderState: state.orderState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    orderAction: require('../../actions/orderAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.orderAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderEditor);
