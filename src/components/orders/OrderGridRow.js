import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import AppStyles from '../../styles/Style';

class OrderGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr className="pointer" onClick={()=> { this.props.onEditClick(item); }}>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <EditIcon />
          </IconButton>
        </td>
        <td>
          <label className='capital center'> {item.fname} {item.lname} </label>
        </td>
        <td>
          {item.transactionID}
        </td>
        <td>
          {item.locName}
        </td>
        <td>
          {item.grandTotal}
        </td>
        <td>
          <label className={ (item.paymentStatus == 'New Order') ? 'red-text strong-7 capital' : 'green-txt strong-7 capital'}>{item.paymentStatus}</label>
        </td>
      </tr>
    );
  }
}

export default OrderGridRow;
