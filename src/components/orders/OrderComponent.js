'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import Search from 'material-ui/svg-icons/action/search';
import Config from '../../config/base';
import {AddButton, ViewButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import OrderList from './OrderList';

class OrderComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/order_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/orders/'+item.transactionID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.orders;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.orders;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.orders;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.orders;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.orders;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getOrders(params);
  }

  render() {
    const {pageinfo} = this.props.data.orders;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading title-gotham'>All Orders</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ (pageinfo == undefined)? '' : pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.orders.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.orders.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.orders.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

        <Card className="order-card">
          <Form>
            <Row>
              <Col md={2}>
                <Label className="label-dft strong-6 title-gotham">Check Order Status </Label>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Input type="select" name="select" id="exampleSelect" className="fullwidth">
                    <option>- Search By Name -</option>
                    <option>- Search By Trans. No. -</option>
                    <option>- Search By Branch -</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col md={5}>
                <FormGroup>
                  <Label hidden>Search</Label>
                  <Input type="text" name="search" id="- Search By Name -" placeholder=""  className="fullwidth"/>
                </FormGroup>
              </Col>
              <Col md={1}>
                <FormGroup>
                  <Label className="search"><Search className='nav-icon'/></Label>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md={2}>
                <Label className="label-dft strong-6 title-gotham">Status Result<span className="pull-colo-r">:</span> </Label>
              </Col>
              <Col md={4}>
                <p className="label-dft"> Branch - <span className="label-dft green-txt"> Preparing</span></p>

              </Col>
              <Col md={5}>

                <FormGroup className="pull-right-responsive">
                  <ViewButton
                      handleOpen={this.handleAdd}
                      style= {{ float: 'right'}}/>
                </FormGroup>
              </Col>
              <Col md={1}>
                <FormGroup>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </Card>

        { /* List  */ }
        <OrderList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default OrderComponent;
