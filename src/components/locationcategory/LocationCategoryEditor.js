/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import LocationCategoryForm from './LocationCategoryForm';


class LocationCategoryEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      locationcategory: {
        name: '',
        description: '',
        status: false
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewLocationCategory(this.props.params.id);
    }
    console.log('actions', actions);
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.locationcategoryState.selectedRecord)) {
        this.setState({
          locationcategory: nextProps.locationcategoryState.selectedRecord
        });
      }
    }
  }


  // handle going back
  handleReturn = () => {
    this.props.router.push('/locationcategory');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput2(this.state.locationcategory)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_LOCATIONCATEGORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateLocationCategory(this.state.locationcategory, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.locationcategory)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_LOCATIONCATEGORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addLocationCategory(this.state.locationcategory, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let locationcategory = this.state.locationcategory;
    locationcategory[field] = e.target.value;
  }


  validateInput = (data) => {
    const {dialogActions} = this.props;

    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.description == '') {
      dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    let newDescription = data.description.replace(/<br\/>/g, "\n");
    this.state.locationcategory['description'] = newDescription;
    return true;
  }

  // Validation for Update button
  validateInput2 = (data) => {
    const {dialogActions} = this.props;

    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    let newDescription = data.description.replace(/<br\/>/g, "\n");
    this.state.locationcategory['description'] = newDescription;
    return true;
  }

  // handle status locFlag
  handleLocFlagChange = (e) => {
    this.state.locationcategory['locFlag'] = e;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.locationcategory['status'] = e;
  }


  render(){
    const {actions} = this.props;
    console.log('nram', this.state.locationcategory);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Location Category' : 'Add Location Category'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <LocationCategoryForm
          data={this.props.locationcategoryState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          actions={actions}
          onStatusChange={this.handleStatusChange}
          onLocFlagChange={this.handleLocFlagChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    locationcategoryState: state.locationcategoryState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    locationcategoryAction: require('../../actions/locationCategoryAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.locationcategoryAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationCategoryEditor);
