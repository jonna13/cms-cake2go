/**
 * Created by jonna on 8/28/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';

import AnnouncementForm from './AnnouncementForm';

class AnnouncementEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      announcement: {
        title: '',
        schedule: '',
        message: '',
        url: '',
        image: '',
        status: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.ANNOUNCEMENT,
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewAnnouncement(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.announcementState.selectedRecord)) {
        this.setState({
          announcement: nextProps.announcementState.selectedRecord
        });
      }
    }
  }

  // handle going back to Announcement
  handleReturn = () => {
    this.props.router.push('/announcement');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.announcement)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_ANNOUNCEMENT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateAnnouncement(this.state.announcement, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.announcement)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_ANNOUNCEMENT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addAnnouncement(this.state.announcement, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (this.state.announcement.image == '') {
      if (_.isEmpty(this.state.uploadImage)) {
        dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
    }
    if (data.title == '') {
      dialogActions.openNotification('Oops! No title found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.message == '') {
      dialogActions.openNotification('Oops! No message found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.url == '') {
      dialogActions.openNotification('Oops! No url found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    let newDescription = data.description.replace(/<br\/>/g, "\n");
    this.state.announcement['description'] = newDescription;

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let announcement = this.state.announcement;
    announcement[field] = e.target.value;
  }

  // handle change in type
  handleTypeChange = (e) => {
    this.state.announcement['type'] = e;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.announcement['status'] = e;
  }

  //close modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? "Edit Announcement" :"Create New Announcement"}</h2>
        <Divider className='content-divider2' />

        { /* Action Buttons */ }
        {/*<ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }*/}

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? "Edit Announcement" : "Create New Announcement"}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          className="dialog-title"
        >
          <AnnouncementForm
            data={this.props.announcementState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            onTypeChange={this.handleTypeChange}
            onStatusChange={this.handleStatusChange}
            onImageChange={this.handleImageChange}
            imageModule={this.state.imageModule}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <CreateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    announcementState: state.announcementState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    announcementAction: require('../../actions/announcementAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.announcementAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(AnnouncementEditor);
