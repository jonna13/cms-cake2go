import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Checkbox from 'material-ui/Checkbox';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import AppStyles from '../../styles/Style';

class AnnouncementGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <MoreVertIcon />
          </IconButton>
        </td>
        <td>
          <Checkbox
            className="inherit" style= {{ width: 'auto'}}/>
          <label className='capital'>{item.status}</label>
        </td>
        <td className="pointer blue-text" onClick={()=> { this.props.onEditClick(item); }}>
          {item.title}
        </td>
        <td>
          {item.schedule}
        </td>
      </tr>
    );
  }
}

export default AnnouncementGridRow;
