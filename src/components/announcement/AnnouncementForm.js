/**
 * Created by jonna on 8/28/18.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';


class AnnouncementForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleTypeChange = (e, idx, type) => {
    this.setState({type});
    this.props.onTypeChange(type);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    const {data} = this.props;
    console.log('AnnouncementForm', data);

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <div>
              <Col className="modal-container">
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Title : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="title" id="title" placeholder="" defaultValue={(data.selectedRecord.title) ? data.selectedRecord.title : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Schedule : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="schedule" id="schedule" placeholder="" defaultValue={(data.selectedRecord.schedule) ? data.selectedRecord.schedule : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham"> Message : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="textarea" name="message" id="message" placeholder="" defaultValue={(data.selectedRecord.message) ? data.selectedRecord.message : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">URL : </Label>
                </Col>
                <Col md={8}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="url" id="url" placeholder="" defaultValue={(data.selectedRecord.url) ? data.selectedRecord.url : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <Label className="label-dft strong-6 title-gotham">Image : </Label>
                </Col>
                <Col md={8}>
                  <div className='item-image-box'>
                    <ImageUpload image={null}
                      onImageChange={this.props.onImageChange}
                      image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                      imageModule={this.props.imageModule}
                      info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                  </div>
                </Col>
              </Col>
            </div>
            {/*<Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}
                    info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="title"
                  hintText="Enter Title"
                  floatingLabelText="Title"
                  defaultValue={(data.selectedRecord.title) ? data.selectedRecord.title : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                {/*<SelectField value={this.state.type} onChange={this.handleTypeChange} floatingLabelText="Type" hintText="Select Type" floatingLabelFixed={true}>
                  <MenuItem value="type1" primaryText="Type 1" />
                  <MenuItem value="type2" primaryText="Type 2" />
                </SelectField><br />*/}
                {/*<DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  regular={true}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />
                <TextField
                  name="url"
                  hintText="Enter URL"
                  floatingLabelText="URL"
                  defaultValue={(data.selectedRecord.url) ? data.selectedRecord.url : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>*/}
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default AnnouncementForm;
