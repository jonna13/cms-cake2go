/**
 * Created by jonna on 8/29/18.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './AnnouncementGridRow';

class AnnouncementList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getAnnouncements(params);
  }

  render(){
    let {announcements} = this.props.data;
    const headers = [
      { title: '', value: 'dateAdded'},
      { title: 'Status', value: 'status'},
      { title: 'Title', value: 'title'},
      { title: 'Publishing Schedule', value: 'schedule'}
    ];

    var rows = [];
    if (announcements.records.length > 0) {
      announcements.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.ANNOUNCEMENT}/>);
      });
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.announcements}
            onGetData={this.handleGetData}
            imageModule={Config.IMAGE.ANNOUNCEMENT}
            hasImage={true}
            imageKey='image'
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.announcements}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default AnnouncementList;
