/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';

class LevelForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    const {data} = this.props;

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <SelectField
                  value={(data.selectedRecord.brandID) ? data.selectedRecord.brandID: this.state.brandID}
                  onChange={this.handleBrandChange}
                  className='textfield-regular'
                  floatingLabelText="Brand"
                  hintText="Select Brand"
                  autoWidth={false}>
                </SelectField><br />
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="min"
                  type="number"
                  hintText="Enter Minimum"
                  floatingLabelText="Minimum"
                  defaultValue={(data.selectedRecord.min) ? data.selectedRecord.min : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="max"
                  type="number"
                  hintText="Enter Maximum"
                  floatingLabelText="Maximum"
                  defaultValue={(data.selectedRecord.max) ? data.selectedRecord.max : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="level"
                  type="number"
                  hintText="Enter Level"
                  floatingLabelText="Level"
                  defaultValue={(data.selectedRecord.level) ? data.selectedRecord.level : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextAreaField
                  name="quote"
                  hintText="Enter Quote"
                  floatingLabelText="Quote"
                  defaultValue={(data.selectedRecord.quote) ? data.selectedRecord.quote : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  regular={true}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default LevelForm;
