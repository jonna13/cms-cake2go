/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import LevelForm from './LevelForm';


class LevelEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      level: {
        levelCategoyID: '',
        name: '',
        min: 0,
        max: 0,
        quote: '',
        status: ''
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewLevel(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.levelState.selectedRecord)) {
        this.setState({
          level: nextProps.levelState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/level');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.level)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_LEVEL_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateLevel(this.state.level, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.level)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_LEVEL_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addLevel(this.state.level, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let level = this.state.level;
    level[field] = e.target.value;
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.min == '') {
      dialogActions.openNotification('Oops! No minimum found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.max == '') {
      dialogActions.openNotification('Oops! No maximum found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.level['status'] = e;
  }


  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Level' : 'Add Level'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <LevelForm
          data={this.props.levelState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onStatusChange={this.handleStatusChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    levelState: state.levelState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    levelAction: require('../../actions/levelAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.levelAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LevelEditor);
