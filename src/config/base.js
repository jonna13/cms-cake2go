'use strict';

import { grey300, grey100, grey500, white, grey900} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// Settings configured here will be merged into the final config object.
// ================================================
export default {
  MERCHANT_NAME: 'Cake2Go',
  // For LIVE
  // PROJECT_PATH: 'http://13.228.225.86/',
  // PROJECT_PATH: 'http://henannrewards.com/',
  // PROJECT_BASE: '/dashboard/',
  // UPLOAD_URL: 'api/misc/upload_dashboard.php',

  // For Dev AWS Server
  PROJECT_PATH: 'http://52.221.188.227/cake2go/',
  PROJECT_BASE: '/dashboard/',
  UPLOAD_URL: 'api/misc/upload_dashboard.php',

  GATEWAY_URL: 'api.php',
  MUITHEME: getMuiTheme({
    palette: {
      accent1Color: grey300,
      accent2Color: grey100,
      primary1Color: grey500,
      primary2Color: grey500,
      textColor: grey900,
      alternateTextColor: white
    }
  }),
  BUILD_NAME: 'Cake2Go CMS',
  BUILD_VERSION: 'v1.1.1',
  BUILD_DATE: '7/18/18',
  IMAGE: {
    PATH: 'assets/images/',
    PROFILE: 'profile',
    CATEGORY: 'category',
    SUBCATEGORY: 'subcategory',
    LOCATION: 'locations',
    LOYALTY: 'loyalty',
    VOUCHER: 'vouchers',
    POST: 'posts',
    ANNOUNCEMENT: 'announcements',
    BRAND: 'brands',
    PRODUCT: 'products',
    SETTING: 'settings',
    CASHIER: 'cashiers',
    ACCOUNT: 'accounts',
    LEVEL: 'level',
    LEVELCATEGORY: 'levelcategories',
    TERM: 'term',
    TABLET: 'tablet',
    REWARDS: 'rewards',
    HOTELS: 'hotels'
  },
  GATEWAY: {
    CATEGORY: 'dashboard'
  },
  DIALOG_MESSAGE: {
    CONFIRM_TITLE: 'Confirm',
    UPDATE_PROFILE_MESSAGE: 'Are you sure you want to update company profile?',
    ADD_PRODUCT_MESSAGE: 'Are you sure you want to add new product?',
    UPDATE_PRODUCT_MESSAGE: 'Are you sure you want to update selected product?',
    ADD_CATEGORY_MESSAGE: 'Are you sure you want to add new category?',
    UPDATE_CATEGORY_MESSAGE: 'Are you sure you want to update selected category?',
    ADD_ORDER_MESSAGE: 'Are you sure you want to add new order?',
    UPDATE_ORDER_MESSAGE: 'Are you sure you want to update selected order?',
    ADD_SUBCATEGORY_MESSAGE: 'Are you sure you want to add new subcategory?',
    UPDATE_SUBCATEGORY_MESSAGE: 'Are you sure you want to update selected subcategory?',
    ADD_LOCATION_MESSAGE: 'Are you sure you want to add new location?',
    UPDATE_LOCATION_MESSAGE: 'Are you sure you want to update selected location?',
    ADD_LOCATIONCATEGORY_MESSAGE: 'Are you sure you want to add new location category?',
    UPDATE_LOCATIONCATEGORY_MESSAGE: 'Are you sure you want to update selected location category?',
    ADD_CITY_MESSAGE: 'Are you sure you want to add new city?',
    UPDATE_CITY_MESSAGE: 'Are you sure you want to update selected city?',
    ADD_LOYALTY_MESSAGE: 'Are you sure you want to add new campaign?',
    UPDATE_LOYALTY_MESSAGE: 'Are you sure you want to update selected campaign?',
    ADD_VOUCHER_MESSAGE: 'Are you sure you want to add new voucher?',
    UPDATE_VOUCHER_MESSAGE: 'Are you sure you want to update selected voucher?',
    ADD_BRAND_MESSAGE: 'Are you sure you want to add new brand?',
    UPDATE_BRAND_MESSAGE: 'Are you sure you want to update selected brand?',
    ADD_POST_MESSAGE: 'Are you sure you want to add new post?',
    UPDATE_POST_MESSAGE: 'Are you sure you want to update selected post?',
    ADD_ANNOUNCEMENT_MESSAGE: 'Are you sure you want to add new announcement?',
    UPDATE_ANNOUNCEMENT_MESSAGE: 'Are you sure you want to update selected announcement?',
    ADD_TABLET_MESSAGE: 'Are you sure you want to add new tablet?',
    UPDATE_TABLET_MESSAGE: 'Are you sure you want to update selected tablet?',
    ADD_SETTING_MESSAGE: 'Are you sure you want to add new setting?',
    UPDATE_SETTING_MESSAGE: 'Are you sure you want to update selected setting?',
    ADD_LEVEL_MESSAGE: 'Are you sure you want to add new level?',
    UPDATE_LEVEL_MESSAGE: 'Are you sure you want to update selected level?',
    ADD_LEVELCATEGORY_MESSAGE: 'Are you sure you want to add new level category?',
    UPDATE_LEVELCATEGORY_MESSAGE: 'Are you sure you want to update selected level category?',
    ADD_TERM_MESSAGE: 'Are you sure you want to add new Term and Condition?',
    UPDATE_TERM_MESSAGE: 'Are you sure you want to update selected Term and Condition?',
    ADD_ABOUT_MESSAGE: 'Are you sure you want to add new About Us?',
    UPDATE_ABOUT_MESSAGE: 'Are you sure you want to update selected About Us?',
    ADD_FAQ_MESSAGE: 'Are you sure you want to add new FAQ?',
    UPDATE_FAQ_MESSAGE: 'Are you sure you want to update selected FAQ?',
    ADD_SOCIALMEDIA_MESSAGE: 'Are you sure you want to add new Social Media?',
    UPDATE_SOCIALMEDIA_MESSAGE: 'Are you sure you want to update selected Social Media?',
    ADD_SKU_MESSAGE: 'Are you sure you want to add new SKU?',
    UPDATE_SKU_MESSAGE: 'Are you sure you want to update selected SKU?',
    ADD_CASHIER_MESSAGE: 'Are you sure you want to add new cashier?',
    UPDATE_CASHIER_MESSAGE: 'Are you sure you want to update selected cashier?',
    ADD_ACCOUNT_MESSAGE: 'Are you sure you want to add new account?',
    UPDATE_ACCOUNT_MESSAGE: 'Are you sure you want to update selected account?',
    UPDATE_PROFILE_MESSAGE: 'Are you sure you want to update company profile?',
    UPDATE_CREDENTIAL_MESSAGE: 'Are you sure you want to update credentials?',
    SEND_PUSH_MESSAGE: 'Are you sure you want to send Push Notification?',
    CONFIRM_LABEL: 'Yes',
    CLOSE_LABEL: 'No',
    CANCEL_LABEL: 'Cancel',
    NOTIFICATION_DELAY: 5000
  },
  ERROR_MESSAGE: {
    ERROR_TITLE: 'Error',
    ERROR_DESCRIPTION: 'Oops! Something went wrong. Kindly check the internet.',
    EXPIRED_TITLE: 'Expired',
    EXPIRED_DESCRIPTION: 'Oops! It seems your session expired',
    EMPTY_TITLE: 'Empty',
    EMPTY_DESCRIPTION: 'Oops! Data is empty',
    FAILED_TITLE: 'Failed',
    FAILED_FETCH_DESCRIPTION: 'Oops! It seems that fetching data from the server failed. Kindly check your internet connection and reload the page',
    FAILED_ADD_DESCRIPTION: 'Oops! It seems there\'s a problem adding a new record. Kindly check your internet connection and reload the page',
    FAILED_UPDATE_DESCRIPTION: 'Oops! It seems there\'s a problem updating your record. Kindly check your internet connection and reload the page',
    CONNECTION_TITLE: 'Connection Error',
    CONNECTION_DESCRIPTION: 'Oops! Unable to connect to server. Please check your internet connection and reload the page'
  },
  ERROR_CODE: {
    PROFILETABLE: '#200',
    PRODUCTTABLE: '#201',
    PRODCATEGORYTABLE: '#202',
    PRODSUBCATEGORYTABLE: '#203',
    LOCATIONTABLE: '#204',
    LOYALTYTABLE: '#205',
    VOUCHERTABLE: '#206',
    POSTTABLE: '#207',
    TABLETTABLE: '#208',
    SETTINGTABLE: '#209',
    CASHIERTABLE: '#210',
    ACCOUNTABLE: '#211',
    BRANDTABLE: '#212',
    TERMTABLE: '#213',
    LEVELTABLE: '#214',
    SKUTABLE: '#215',
    CATEGORYTABLE: '#216',
    SUBCATEGORYTABLE: '#217',
    LOCATIONCATEGORYTABLE: '#218',
    ANNOUNCEMENTABLE: '#219',
    CITYTABLE: '#220',
    ORDERTABLE: '#220',
  },
  IMAGE_SIZE_LIMIT: 2e+6,
  IMAGE_FORMAT: ["jpg", "jpeg", "png", "gif", "bmp", "JPG", "JPEG", "PNG", "GIF", "BMP"],
  FIELD_EDIT:{
    PRIMARY_MAX_SIZE: 500,
    SECONDARY_MAX_SIZE: 1000,
    TERTIARY_MAX_SIZE: 1500
  },
  GODUSER: 'developer',
  SUPERUSER: ['developer', 'project-manager'],
  ACCOUNT_ROLES: [
    {id: 1, name: 'Super Admin', value: 'superadmin'},
    {id: 2, name: 'Administrator', value: 'administrator'},
    {id: 3, name: 'Marketing', value: 'marketing'},
    {id: 4, name: 'Accounting', value: 'accounting'}
  ],
  DEVELOPER_ROLE: [{id: 6, name: 'Developer', value: 'developer'}],
  ACCOUNT_ROLE_TABS: {
    'developer' : {
      reports: ['all'],
      cms: ['all']
    },
    'superadmin' : {
      reports: ['all'],
      cms: ['all']
    },
    'administrator': {
      reports: ['all'],
      cms: ['voucher', 'location', 'term', 'setting', 'account']
      // cms: ['voucher', 'location', 'term', 'setting/EARNqh670VGtcpj', 'account']
    },
    'marketing': {
      reports: [],
      cms: ['profile', 'loyalty', 'category', 'product', 'post', 'about', 'faq', 'setting', 'push']
      // cms: ['profile', 'loyalty', 'category', 'product', 'post', 'about', 'faq', 'setting/EARNqh670VGtcpj', 'push']
    },
    'accounting': {
      reports: ['all'],
      cms: []
    }
  },
  CUSTOM_ROLE: 'staff',
  CMS_TABS: [
    {id: 1, name: 'Profile', value: 'profile'},
    {id: 2, name: 'Push', value: 'push'},
    {id: 3, name: 'Main Category', value: 'category'},
    {id: 4, name: 'Sub category', value: 'subcategory'},
    {id: 5, name: 'Product', value: 'product'},
    {id: 6, name: 'Location', value: 'location'},
    {id: 7, name: 'Campaign', value: 'loyalty'},
    {id: 8, name: 'Voucher', value: 'voucher'},
    {id: 9, name: 'Exclusive', value: 'post'},
    {id: 10, name: 'Tablet', value: 'tablet'},
    {id: 11, name: 'Cashier', value: 'cashier'},
    {id: 12, name: 'Account', value: 'account'},
    // {id: 13, name: 'Setting', value: 'setting/EARNqh670VGtcpj'},
    {id: 13, name: 'Setting', value: 'setting'},
    {id: 14, name: 'Level', value: 'level'},
    {id: 15, name: 'Level Category', value: 'levelcategory'},
    {id: 16, name: 'Term', value: 'term'},
    {id: 17, name: 'SKU', value: 'sku'},
    {id: 18, name: 'Brand', value: 'brand'},
    {id: 19, name: 'About Us', value: 'about'},
    {id: 20, name: 'FAQs', value: 'faq'},
    {id: 999, name: 'All', value: 'all'}
  ],
  REPORT_TABS: [
    {id: 1, name: 'Downloads', value: 'downloads'},
    {id: 2, name: 'Card Registration', value: 'card-registration'},
    {id: 3, name: 'Demographics', value: 'demographics'},
    {id: 4, name: 'Loyalty Sales', value: 'loyalty'},
    {id: 5, name: 'Vouchers', value: 'vouchers'},
    {id: 6, name: 'Points Redemption', value: 'points-redemption'},
    {id: 999, name: 'All', value: 'all'}
  ]
  // GATEWAY_URL: 'http://savenearn.appsolutely.ph/dashboard/php/gateway.test.php'
}
