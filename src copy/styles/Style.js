import Config from '../config/base';

const AppStyles = {

  modalContainer_100 : {
    width: '100%',
    maxWidth: 'none'
  },
  modalContainer_80 : {
    width: '80%',
    maxWidth: 'none'
  },

  SaveButtonBox : {
    border: '1px solid ' + Config.MUITHEME.palette.accent1Color,
    marginRight: 5
  },

  editIcon: {
    color: Config.MUITHEME.palette.primary1Color,
    paddingTop: 0
  },

  modalHeader: {
    backgroundColor: Config.MUITHEME.palette.primary1Color,
    color: Config.MUITHEME.palette.alternateTextColor,
    marginBottom: '20px'
  },

  imageInput: {
    cursor: 'pointer',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  }
};

export default AppStyles;
