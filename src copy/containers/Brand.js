import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/brand/BrandComponent';

class Brand extends Component {
  render() {
    const {actions, brandState, router} = this.props;
    return <Main actions={actions} data={brandState} router={router}/>;
  }
}

Brand.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { brandState: state.brandState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    brandAction: require('../actions/brandAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.brandAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Brand);
