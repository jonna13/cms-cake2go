import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/tablet/TabletComponent';

class Tablet extends Component {
  render() {
    const {actions, tabletState, router} = this.props;
    return <Main actions={actions} data={tabletState} router={router} />;
  }
}

Tablet.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { tabletState: state.tabletState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { tabletAction: require('../actions/tabletAction.js') };
  const actionMap = { actions: bindActionCreators(actions.tabletAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Tablet);
