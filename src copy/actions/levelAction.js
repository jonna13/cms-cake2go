/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import * as types from '../constants/LevelActionTypes';
import {get, post} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getLevelCategory = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=levelcategory';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadLevelCategorySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadLevelCategoryFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LOCATIONTABLE));
        dispatch(loadLevelCategoryFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getLevels = (config) => {
  console.info('LevelAction getLevels');
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=leveltable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadLevelSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LEVELTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.LEVELTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewLevel = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=leveltable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        console.log('viewlevel', data);
        dispatch(viewLevelSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LEVELTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.LEVELTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let addLevel = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_level';
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());

    post(props).then(result => {
      let {data} = result;

      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('New level added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/level');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });

  }
}

export let updateLevel = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_level';

  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! level updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        router.push('/level');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadLevelSuccess = (data) => {
  return {
    type: types.GET_LEVEL_SUCCESS,
    data
  }
}

export let loadLevelCategoryFailed = () => {
  return {
    type: types.GET_LEVELCATEGORY_FAILED
  }
}

export let loadLevelCategorySuccess = (data) => {
  return{
    type: types.GET_LEVELCATEGORY_SUCCESS,
    data
  }
}

export let viewLevelSuccess = (data) => {
  return{
    type:types.VIEW_LEVEL_SUCCESS,
    data
  }
}
