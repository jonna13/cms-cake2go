/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import * as types from '../constants/SettingActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getSettings = (config) => {
  console.info('SettingAction getSettings');
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=earnsettingstable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadSettingSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.SETTINGTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.SETTINGTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getLevels = () =>{
  console.info('SettingAction getLevels');
  let param = '?function=json&category='+Config.GATEWAY.CATEGORY+'&table=leveltable_active';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadLevelSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadLevelFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LEVELTABLE));
        dispatch(loadLevelFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getBrands = () =>{
  console.info('SettingAction getBrands');
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=brand_setting';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadBrandSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadBrandFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LEVELTABLE));
        dispatch(loadBrandFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getBrands2 = () =>{
  console.info('SettingAction getBrands2');
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=brand';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadBrandSuccess2(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadBrandFailed2(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.LEVELTABLE));
        dispatch(loadBrandFailed2(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewSetting = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=earnsettingstable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewSettingSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.SETTINGTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.SETTINGTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}


export let addSetting = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_setting';
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());

      post(props).then(result => {
        let {data} = result;
        console.info('addSetting', props);
        if (data.response == 'Success') {
          dispatch(dialogAction.openNotification('New setting added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
          router.push('/setting');
        }
        else if (data.response == 'Expired'){
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
          dispatch(auth.logoutUser());
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
        }
        dispatch(loadingAction.hideLoading());

      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });

  }
}

export let updateSetting = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_setting';

  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! setting updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        // router.push('/setting/EARNJB23143DbVSs');
        router.push('/setting');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}


// export let updateSetting = (props, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'update_setting';
//   if (typeof props.status == 'boolean') {
//     props.status = (props.status) ? 'active' : 'inactive';
//   }
//
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//     if (_.isEmpty(image)){
//       updateSelectedRecord(props, router, dispatch);
//     }
//     else{
//       upload(uploadparam).then(result => {
//         let {data} = result;
//         if (data.response == "Success") {
//           props.icon = data.data;
//           updateSelectedRecord(props, router, dispatch);
//         }
//         else{
//           dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
//         }
//       }).catch( e => {
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//         dispatch(loadingAction.hideLoading());
//         console.info('Error', e);
//       });
//     }
//   }
// }
//
//
// let updateSelectedRecord = (props, router, dispatch) => {
//   post(props).then(result => {
//     let {data} = result;
//     console.info('updateSetting', result);
//     if (data.response == 'Success') {
//       dispatch(dialogAction.openNotification('Great! setting updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//       dispatch(removeSelectedRecord());
//       router.push('/setting');
//     }
//     else if (data.response == 'Expired'){
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//       dispatch(auth.logoutUser());
//     }
//     else{
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
//     }
//     dispatch(loadingAction.hideLoading());
//   }).catch( e => {
//     dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//     dispatch(loadingAction.hideLoading());
//     console.info('Error', e);
//   });
// }
//

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadSettingSuccess = (data) => {
  return {
    type: types.GET_SETTING_SUCCESS,
    data
  }
}

export let viewSettingSuccess = (data) => {
  return{
    type:types.VIEW_SETTING_SUCCESS,
    data
  }
}

export let loadLevelSuccess = (data) => {
  return{
    type: types.GET_SETTINGLEVEL_SUCCESS,
    data
  }
}

export let loadLevelFailed = () => {
  return{
    type: types.GET_SETTINGLEVEL_FAILED
  }
}

export let loadBrandSuccess = (data) => {
  return{
    type: types.GET_SETTINGBRAND_SUCCESS,
    data
  }
}

export let loadBrandFailed = () => {
  return{
    type: types.GET_SETTINGBRAND_FAILED
  }
}

export let loadBrandSuccess2 = (data) => {
  return{
    type: types.GET_SETTINGBRAND_SUCCESS2,
    data
  }
}

export let loadBrandFailed2 = () => {
  return{
    type: types.GET_SETTINGBRAND_FAILED2
  }
}
