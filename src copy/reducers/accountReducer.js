/**
 * Created by jedachas on 3/8/17.
 */
import * as types from '../constants/AccountActionTypes';
import update from 'react-addons-update';

const initialState = {
  accounts: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_ACCOUNT_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        accounts: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_ACCOUNT_SUCCESS:
      return update(state, {
        accounts: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_CATEGORY_SUCCESS:
    console.log('cat data accounts', action.data);
      return update(state, {
        accounts: {
          category: {
            $set: action.data.data
          }
        }
      });

    // case types.GET_LOCATIONSCATEGORY_SUCCESS:
    // console.log('locationscategory data accounts', action.data);
    //   return update(state, {
    //     accounts: {
    //       locationscategory: {
    //         $set: action.data.data
    //       }
    //     }
    //   });

    case types.GET_ACCOUNTLOCATION_SUCCESS:
      return update(state, {
        locations: {
          $set: action.data.data
        }
      });

    case types.GET_ACCOUNTLOCATION_FAILED:
      return update(state, {
        locations: {
          records: {
            $set: []
          }
        }
      });

    case types.GET_ACCOUNTBRAND_SUCCESS:
      return update(state, {
        brands: {
          $set: action.data.data
        }
      });

    case types.GET_ACCOUNTBRAND_FAILED:
      return update(state, {
        brands: {
          records: {
            $set: []
          }
        }
      });

    default:
      return state;

  }
}
