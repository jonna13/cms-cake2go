/* Define your initial state here.
 *
 * If you change the type from object to something else, do not forget to update
 * src/container/App.js accordingly.
 */
import Config from '../config/base';
import * as types from '../constants/AuthActionTypes';
import update from 'react-addons-update';

// import {browserHistory} from 'react-router';
// import { createHistory, useBasename } from "history";
//
// const browserHistory = useBasename(createHistory)({
//     basename: "/myapp"
// });

const initialState = {
  auth: !!sessionStorage.getItem(Config.MERCHANT_NAME)
};

module.exports = function(state = initialState.auth, action) {
  /* Keep the reducer clean - do not mutate the original state. */
  //let nextState = Object.assign({}, state);

  switch(action.type) {

    case types.LOGIN_IN_USER:
      return !!sessionStorage.getItem(Config.MERCHANT_NAME);

    case types.LOG_OUT_USER:
      console.info('LOG_OUT_USER');
      return !!sessionStorage.removeItem(Config.MERCHANT_NAME);

    default: {
      /* Return original state if no actions were consumed. */
      return state;
    }

  }
}
