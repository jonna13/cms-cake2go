/**
 * Created by jedachas on 2/9/17.
 */
import React from 'react';
import Config from '../../config/base';
import { Grid, Row, Col } from 'react-bootstrap';

import HttpIcon from 'material-ui/svg-icons/file/cloud';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import PhoneIcon from 'material-ui/svg-icons/hardware/smartphone';
import ContactIcon from 'material-ui/svg-icons/communication/phone';
import Address from 'material-ui/svg-icons/communication/location-on';
import Fax from 'material-ui/svg-icons/action/perm-phone-msg';
import Person from 'material-ui/svg-icons/action/face';
var Barcode = require('react-barcode');


require('styles/profile.css');

class ProfileDisplay extends React.Component {
  createMarkup = () => {
    return {__html: 'First &middot; Second'};
  }

  render(){
    if (this.props.profile.status == 'Success') {
      const {records} = this.props.profile;
      let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH + '' +
        Config.IMAGE.PROFILE + '/full/' + records.profilePic;

      let contactPerson = ((records.lname1) ? records.lname1 + ', ' : '') +
        ((records.fname1) ? records.fname1 : '') +
        ((records.mname1) ? ' ' + records.mname1 : '');
      return(
        <div className='content-container'>
          <Grid fluid>
            <Row>
              <Col xs={0} sm={4} md={4} lg={4} >
                <div className='profile-image'>
                  <img src={path} />
                </div>
              </Col>
              <Col xs={12} sm={8} md={8} lg={8} >
                <div>
                  <h1 className='profile-label-company'>{records.company} </h1>
                  <div className='profile-info' dangerouslySetInnerHTML={{'__html':records.about}} ></div>

                </div>
                <div className='profile-icon-parent'>

                  { /* Icon Communications */ }
                  <Grid fluid className='col-no-padding'>
                    <Row>
                      <Col xs={12} sm={12} md={6} lg={6} className='col-no-padding'>
                        <div className='profile-icon-section'>
                          <HttpIcon className='profile-icon primary-color'/>
                          <span className={ (records.website) ? 'profile-caption' : 'profile-caption-empty' }>
                            {records.website || 'n/a' }</span>
                        </div>
                        <div className='profile-icon-section'>
                          <EmailIcon className='profile-icon primary-color'/>
                          <span className={ (records.email) ? 'profile-caption' : 'profile-caption-empty' }>
                            {records.email || 'n/a'}</span>
                        </div>
                        <div className='profile-icon-section'>
                          <Person className='profile-icon primary-color'/>
                          <span className={ (contactPerson) ? 'profile-caption' : 'profile-caption-empty' }>
                            {contactPerson || 'n/a'}</span>
                        </div>
                      </Col>
                      <Col xs={12} sm={12} md={6} lg={6} className='col-no-padding'>
                        <div className='profile-icon-section'>
                          <PhoneIcon className='profile-icon primary-color'/>
                          <span className={ (records.mobile1) ? 'profile-caption' : 'profile-caption-empty' }>
                          { records.mobile1 || 'n/a' }</span>
                        </div>
                        <div className='profile-icon-section'>
                          <ContactIcon className='profile-icon primary-color'/>
                          <span className={ (records.landline1) ? 'profile-caption' : 'profile-caption-empty' }>
                            { records.landline1 || 'n/a' }</span>
                        </div>
                        <div className='profile-icon-section'>
                          <Fax className='profile-icon primary-color'/>
                          <span className={ (records.fax1) ? 'profile-caption' : 'profile-caption-empty' }>
                            { records.fax1 || 'n/a' }</span>
                        </div>
                      </Col>
                      <Col xs={12} sm={12} md={12} lg={12} className='col-no-padding'>
                        <div className='profile-icon-section'>
                          <Address className='profile-icon primary-color'/>
                            { (records.address) ?
                              <span className='profile-caption'
                                dangerouslySetInnerHTML={{'__html':records.address}} >
                              </span> :
                              <span className='profile-caption-empty'>'n/a'</span>
                             }
                        </div>
                      </Col>
                      <Col xs={12} sm={12} md={12} lg={12} className='col-no-padding'>
                        <center><Barcode value={ records.merchantCode } /></center>
                      </Col>
                    </Row>
                  </Grid>



                </div>
              </Col>
            </Row>
          </Grid>
        </div>
      );
    }
    else{
      return null;
    }
  }
}

export default ProfileDisplay;
