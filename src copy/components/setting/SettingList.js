/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import Config from '../../config/base';
 import Enlist from '../common/EnlistComponent';
 import ScreenListener from '../common/ScreenListener';

 import GridRow from './SettingGridRow';

class SettingList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getSettings(params);
  }

  render(){
    let {settings} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Amount', value: 'camount'},
      { title: 'Points', value: 'cpoints'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (settings.records.length > 0) {
      settings.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.SETTING}/>);
      });
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.settings}
            onGetData={this.handleGetData}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.settings}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default SettingList;
