/**
 * Created by jedachas on 2/9/17.
 *
 * Main Component
 *
 */
require('normalize.css/normalize.css');
require('styles/Grid.css');
// require('styles/App.css');
require('styles/Colored.css');

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Config from '../config/base';
import {AlertDialog, ConfirmDialog, Notification} from './common/DialogComponent';
import Link from './navigation/LinkComponent';
import MobileLink from './navigation/MobileLinkNav';
import Loading from './common/LoadingComponent';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
// import Progress from "react-progress-2";
// import "react-progress-2/main.css";

import { Grid, Row, Col } from 'react-bootstrap';


class AppComponent extends React.Component {
  render() {
    return (
      <div>
        { /* <Progress.Component/> */ }
      <MuiThemeProvider muiTheme={Config.MUITHEME}>
        <div>
          { /* <Header router={this.props.router}/> */ }
          <Grid fluid>
            <Row>
              <Col xs={0} sm={3} md={2} lg={2} className='v-header-bg'>
                <Link router={this.props.router}
                  actions={this.props.authActions}
                  authState={this.props.authState}/>
                <MobileLink router={this.props.router} 
                  actions={this.props.authActions}
                  authState={this.props.authState}/>
              </Col>
              <Col xs={12} sm={9} md={10} lg={10} className='v-main-container'>
                { /*<ReactCSSTransitionGroup
                component="div"
                transitionName="example"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={500}
                >
                {React.cloneElement(this.props.children, {
                  key: this.props.router.location.pathname
                })}
                </ReactCSSTransitionGroup>*/ }
                  {this.props.children }
              </Col>
            </Row>
          </Grid>

          { /* Dialog  */ }
          <Loading {...this.props.loading}/>
          <AlertDialog {...this.props.alert} dialogActions={this.props.actions} />
          <ConfirmDialog {...this.props.confirm} dialogActions={this.props.actions} />
          <Notification {...this.props.notification} dialogActions={this.props.actions} />
        </div>
      </MuiThemeProvider>
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

function mapStateToProps(state){
  return {
    alert: state.dialogState.alert,
    confirm: state.dialogState.confirm,
    notification: state.dialogState.notification,
    loading: state.loadingState.loading,
    authState : state.authReducer
  }
}
function mapDispatchToProps(dispatch) {
  const actions = {
    dialogActions: require('../actions/dialogAction.js'),
    authAction: require('../actions/authAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.dialogActions, dispatch),
    authActions : bindActionCreators(actions.authAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);
