/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';

class LocationForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      name: '',
      brandID: '',
      branchCode: ''
    };
  }

  componentWillMount(){
    const {actions, data} = this.props;
    // actions.getCategory();
    // for add
    if (!this.props.shouldEdit){
      this.setState({
        shouldDisplay: true,
      });
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit

    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
        branchCode: nextProps.data.selectedRecord.branchCode
      }
    }
  }

  handleBranchCodeChange = (e, idx, branchCode) => {
    this.setState({branchCode});
    this.props.onBranchCodeChange(branchCode);
  };

  handleBrandChange = (e, idx, brandID) => {
    this.setState({brandID});
    this.props.onChangeBrand(brandID);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    let branchCodeList = [];
    const {data} = this.props;
    console.log('Location Form data', data.branchCodes);


    if (this.props.data.branchCodes) {
      branchCodeList.push(<MenuItem key={0} value='Select Branch Code' />);
      this.props.data.branchCodes.map( (val, idx) => {
        branchCodeList.push(<MenuItem key={(idx+1)} value={val.locID} primaryText={val.branchCode} />);
      });
    }

    // var category = [];
    // console.log('category', data.locations.category[0].brandID);
    //     if (data.locations.category.length > 0) {
    //       data.locations.category.forEach((val, key) => {
    //         category.push(
    //           <MenuItem
    //           key={key}
    //           value={val.brandID}
    //           primaryText={val.name}
    //           />
    //         );
    //       });
    //     }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                {/*<TextField
                  disabled={true}
                  name="brandID"
                  floatingLabelText="Brand"

                /><br />
                <SelectField
                  value={(data.selectedRecord.brandID) ? data.selectedRecord.brandID: this.state.brandID}
                  onChange={this.handleBrandChange}
                  className='textfield-regular'
                  floatingLabelText="Brand"
                  hintText="Select Brand">
                  {category}
                </SelectField><br />*/}
                {/*<SelectField
                  value={(data.selectedRecord.branchCode) ? data.selectedRecord.branchCode: this.state.branchCode}
                  onChange={this.handleBranchCodeChange}
                  className='textfield-regular'
                  floatingLabelText="Branch Code"
                  hintText="Select Branch Code"
                  autoWidth={false}>
                  {branchCodeList}
                </SelectField><br />*/}
                <TextField
                  name="name"
                  hintText="Enter Location Name"
                  floatingLabelText="Location Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <DefaultTextArea
                  name="address"
                  hintText="Enter Address"
                  floatingLabelText="Address"
                  defaultValue={(data.selectedRecord.address) ? data.selectedRecord.address : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <TextField
                  name="latitude"
                  hintText="Enter Latitude"
                  floatingLabelText="Latitude"
                  defaultValue={(data.selectedRecord.latitude) ? data.selectedRecord.latitude : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="longitude"
                  hintText="Enter Longitude"
                  floatingLabelText="Longitude"
                  defaultValue={(data.selectedRecord.longitude) ? data.selectedRecord.longitude : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <DefaultTextArea
                  name="businessHrs"
                  hintText="Enter Business Hours"
                  floatingLabelText="Business Hours"
                  defaultValue={(data.selectedRecord.businessHrs) ? data.selectedRecord.businessHrs : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="branchCode"
                  hintText="Enter Branch Code"
                  floatingLabelText="Branch Code"
                  defaultValue={(data.selectedRecord.branchCode) ? data.selectedRecord.branchCode : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="phone"
                  hintText="Enter Phone"
                  floatingLabelText="Phone"
                  defaultValue={(data.selectedRecord.phone) ? data.selectedRecord.phone : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="email"
                  hintText="Enter Email"
                  floatingLabelText="Email"
                  defaultValue={(data.selectedRecord.email) ? data.selectedRecord.email : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <Checkbox
                  label="locFlag"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.locFlag == 'active') ? true : false) : false }
                  onCheck={this.onLocFlagCheck}
                />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default LocationForm;
