/**
 * Created by jedachas on 3/8/17.
 */

import React, { Component } from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import View from 'material-ui/svg-icons/action/dns';

class PageSizeComponent extends Component {
  state = {
    value: 5
  }

  componentWillReceiveProps(nextProps){
    let {pageSize} = nextProps.pageinfo;
    this.setState({value:pageSize});
  }

  render() {
    var {pageinfo} = this.props;
    var iconClass = (pageinfo.pageSize == 5) ?
      'icon-action' : 'icon-action-active';

    return(
      <div className='pageview-container'>
        <IconMenu
          iconButtonElement={<IconButton tooltip="Choose page view">
            <View className={iconClass}/>
          </IconButton>}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
          onChange={this.props.handleChange}
          value={this.state.value}
        >
          <MenuItem primaryText="5" value={5} />
          <MenuItem primaryText="10" value={10} />
          <MenuItem primaryText="20" value={20} />
        </IconMenu>
      </div>
    );
  }
}

export default PageSizeComponent;
