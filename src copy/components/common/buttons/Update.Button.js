'use strict';

import React from 'react';

import IconButton from 'material-ui/IconButton';
import Update from 'material-ui/svg-icons/content/save';

class UpdateButton extends React.Component{
  render(){
    return(
      <div>
        <div className='v-floating-action-circle hidden-xs'>
          <IconButton tooltip="Click to update" touch={true} tooltipPosition="bottom-left">
            <Update className='floating-button-icon' onTouchTap={this.props.handleOpen}/>/>
          </IconButton>
        </div>

        <div className='floating-button visible-xs'>
          <IconButton touch={true} tooltipPosition="bottom-left">
            <Update className='floating-button-icon' onTouchTap={this.props.handleOpen}/>/>
          </IconButton>
        </div>
      </div>
    );
  }
}

export default UpdateButton;
