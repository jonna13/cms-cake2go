'use strict';

import React from 'react';

import IconButton from 'material-ui/IconButton';
import Create from 'material-ui/svg-icons/content/create';

class ProfileButton extends React.Component{

  render(){
    return(
      <div>
        <div className='v-floating-action-circle hidden-xs'>
          <IconButton tooltip="Click to edit" touch={true} tooltipPosition="bottom-left">
            <Create className='floating-button-icon' onTouchTap={this.props.onProfileEdit} />
          </IconButton>
        </div>

        <div className='floating-button visible-xs'>
          <IconButton touch={true} tooltipPosition="bottom-left">
            <Create className='floating-button-icon' onTouchTap={this.props.onProfileEdit}/>
          </IconButton>
        </div>
      </div>
    );
  }
}

export default ProfileButton;
