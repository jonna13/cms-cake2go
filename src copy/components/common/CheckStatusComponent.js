'use strict';

import React, { Component } from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Check from 'material-ui/svg-icons/action/check-circle';

class CheckStatusComponent extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: 1
    };
  }

  componentWillReceiveProps(nextProps){
    let {selectedStatus} = nextProps.pageinfo;
    if (selectedStatus) {
      if (selectedStatus == 'active') {
        this.setState({value:2});
      }
      else if (selectedStatus == 'inactive'){
        this.setState({value:3})
      }
    }
    else{
      this.setState({value:1});
    }
  }

  render(){
    // console.info('CS render:props', this.props)
    var {pageinfo} = this.props;
    var iconClass = (pageinfo.selectedStatus == '' || !pageinfo.selectedStatus) ?
      'icon-action' : 'icon-action-active';

    return(
      <div className='checkstatus-container'>
        <IconMenu
          iconButtonElement={<IconButton tooltip="Choose status">
            <Check className={iconClass}/>
          </IconButton>}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
          onChange={this.props.handleChange}
          value={this.state.value}
        >
          <MenuItem primaryText="None" value={1} />
          <MenuItem primaryText="Active" value={2} />
          <MenuItem primaryText="Inactive" value={3} />
        </IconMenu>
      </div>
    );
  }
}

export default CheckStatusComponent;
