'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import SocialMediaList from './SocialMediaList';

class SocialMediaComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/socialmedia_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/socialmedia/'+item.socialmediaID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.socialmedias;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.socialmedias;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.socialmedias;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.socialmedias;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.socialmedias;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getSocialMedias(params);
  }

  render() {
    const {pageinfo} = this.props.data.socialmedias;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>SocialMedia</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.socialmedias.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.socialmedias.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.socialmedias.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <SocialMediaList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

SocialMediaComponent.displayName = 'SocialMediaSocialMediaComponent';

// Uncomment properties you need
// SocialMediaComponent.propTypes = {};
// SocialMediaComponent.defaultProps = {};

export default SocialMediaComponent;
