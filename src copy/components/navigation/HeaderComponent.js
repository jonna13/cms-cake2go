'use strict';

// import React from 'react';
import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/menu';
import { ListItem} from 'material-ui/List';
import jwtDecode from 'jwt-decode';
import Config from '../../config/base';

let logo = require('../../images/menu-logo.png');

class Logged extends Component{
  state = {
    openMenu: false,
    tabs: []
  }

  componentWillMount(){
    let user = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    this.setState({
      tabs: user.cmsTabs.split(',')
    });
  }

  handleNavigate = (selectedLocation) =>{
    const {pathname} = this.props.router.location;

    if (pathname == selectedLocation) return;
    else{
      this.setState({openMenu: false});
      this.props.router.push(selectedLocation);
    }
  }

  handleOnRequestChange = (value) => {
    this.setState({
      openMenu: value
    });
  }

  handleLogout = () => {
    const {actions} = this.props;
    actions.logoutUser(this.props.router);
  }

  linkClass = (menu) => {
    const {role, tabs} = this.state;
    if (tabs[0] == 'all') {
      return 'visible';
    }else
      return (tabs.includes(menu)) ? 'visible' : 'hide';
  }

  render(){
    return(
      <IconMenu
        className='header-icon-menu'
        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
        open={this.state.openMenu}
        onRequestChange={this.handleOnRequestChange}
      >
        <MenuItem primaryText="Company Profile" className={this.linkClass('profile')}
          onClick={ ()=>{this.handleNavigate('/')} }/>
        <MenuItem className={this.linkClass('loyalty')}
          primaryText="Rewards" onClick={ ()=>{this.handleNavigate('/loyalty')} }/>
        {/*<MenuItem primaryText="Levels" className={this.linkClass('level')}
          onClick={ ()=>{this.handleNavigate('/level')} }/>
        <ListItem
          primaryText="Levels"
          primaryTogglesNestedList={true}
          nestedItems={[
            <MenuItem
              key={1}
              primaryText="Level"
              onClick={ ()=>{
                this.handleNavigate('/level');
              }}
            />,
            <MenuItem
              key={2}
              primaryText="Level Category"
              onClick={ ()=>{
                this.handleNavigate('/levelcategory');
              }}
            />
          ]}
        />

        <ListItem
          primaryText="Promos & Discount"
          primaryTogglesNestedList={true}
          nestedItems={[
            <MenuItem
              key={1}
              primaryText="Category"
              onClick={ ()=>{
                this.handleNavigate('/category');
              }}
            />,
            <MenuItem
              key={2}
              primaryText="Subcategory"
              onClick={ ()=>{
                this.handleNavigate('/subcategory');
              }}
            />,
            <MenuItem
              key={3}
              primaryText="Product"
              onClick={ ()=>{
                this.handleNavigate('/product');
              }}
            />
          ]}
        />*/}
        <MenuItem className={this.linkClass('location')}
          primaryText="Locations" onClick={ ()=>{this.handleNavigate('/location')} }/>
        <MenuItem className={this.linkClass('voucher')}
          primaryText="Vouchers" onClick={ ()=>{this.handleNavigate('/voucher')} }/>
        <MenuItem className={this.linkClass('brand')}
          primaryText="Brand Management" onClick={ ()=>{this.handleNavigate('/brand')} }/>
        <MenuItem className={this.linkClass('post')}
          primaryText="What's New" onClick={ ()=>{this.handleNavigate('/post')} }/>
       {/*<MenuItem className={this.linkClass('cashier')}
          primaryText="Cashiers" onClick={ ()=>{this.handleNavigate('/cashier')} }/>*/}
        <MenuItem className={this.linkClass('account')}
          primaryText="Account" onClick={ ()=>{this.handleNavigate('/account')} }/>
        {/*<MenuItem className={this.linkClass('term')}
          primaryText="Terms and Conditions" onClick={ ()=>{this.handleNavigate('/term')} }/>*/}
        <MenuItem className={this.linkClass('product')}
          primaryText="Featured Products" onClick={ ()=>{this.handleNavigate('/product')} }/>
        <MenuItem className={this.linkClass('tablet')}
          primaryText="POS Management" onClick={ ()=>{this.handleNavigate('/tablet')} }/>
        <MenuItem className={this.linkClass('setting')}
          primaryText="Settings" onClick={ ()=>{this.handleNavigate('/setting')} }/>
        <MenuItem className={this.linkClass('sku')}
          primaryText="SKU" onClick={ ()=>{this.handleNavigate('/sku')} }/>
        <MenuItem className={this.linkClass('push')}
          primaryText="Push" onClick={ ()=>{this.handleNavigate('/push')} }/>
        <MenuItem primaryText="Credential" onClick={this.props.onAccount} />
        <MenuItem primaryText="About" onClick={this.props.onInfo} />
        <MenuItem primaryText="Logout" onClick={this.handleLogout} />
      </IconMenu>
    )
  }
}

class HeaderComponent extends Component {
  render() {
    return (
      <div>
        <AppBar
          className='header-container'
          iconElementLeft={<img className='img-header' src={logo}/>}
          iconElementRight={ <Logged {...this.props} /> }
        />

      </div>
    )
  }
}

HeaderComponent.displayName = 'NavigationHeaderComponent';

// Uncomment properties you need
HeaderComponent.propTypes = {
  actions: PropTypes.object.isRequired
};
// HeaderComponent.defaultProps = {};

function mapStateToProps(state) {
  const props = { isLogged_in : state.authReducer };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    authAction: require('../../actions/authAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.authAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderComponent);
