'use strict';

import React, {
  Component,
  PropTypes
} from 'react';
import { Link } from 'react-router';

import Term from 'material-ui/svg-icons/action/help-outline';
import Voucher from 'material-ui/svg-icons/action/card-giftcard';
import LevelIcon from 'material-ui/svg-icons/action/supervisor-account';
import Business from 'material-ui/svg-icons/communication/business';
import Chat from 'material-ui/svg-icons/communication/chat-bubble';
import Cart from 'material-ui/svg-icons/action/shopping-basket';
import Assignment from 'material-ui/svg-icons/action/assignment';
import Store from 'material-ui/svg-icons/action/store';
import Category from 'material-ui/svg-icons/action/group-work';
import Subcategory from 'material-ui/svg-icons/action/reorder';
import Star from 'material-ui/svg-icons/action/stars';
import Branch from 'material-ui/svg-icons/communication/location-on';
import Loyalty from 'material-ui/svg-icons/action/loyalty';
import New from 'material-ui/svg-icons/av/fiber-new';
import Device from 'material-ui/svg-icons/device/devices';
import Lock from 'material-ui/svg-icons/action/lock-outline';
import Info from 'material-ui/svg-icons/action/info';
import Setting from 'material-ui/svg-icons/action/settings';
import Level from 'material-ui/svg-icons/av/recent-actors';
import Person from 'material-ui/svg-icons/social/person';
import Account from 'material-ui/svg-icons/action/account-circle';
import LocalOffer from 'material-ui/svg-icons/maps/local-offer';
import LocationCity from 'material-ui/svg-icons/social/location-city';
import Description from 'material-ui/svg-icons/action/description';
import Share from 'material-ui/svg-icons/social/share';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import Config from '../../config/base';
import jwtDecode from 'jwt-decode';
import InfoComponent from '../common/InfoComponent';

import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';

import FontIcon from 'material-ui/FontIcon';

import Header from './VerticalHeader';


class LinkComponent extends React.Component {

  state = {
    activeKey: 1,
    open: false,
    tabs: []
  }

  handleSelect = (selectedKey) => {
    this.setState({activeKey: selectedKey});

    switch (selectedKey) {
      case 1:
        this.props.router.push('/');
        break;
      case 2:
        this.props.router.push('/loyalty');
        break;
      // case 2.1:
      //   this.props.router.push('/level');
      //   break;
      // case 2.2:
      //   this.props.router.push('/levelcategory');
      //   break;
      case 3.1:
        this.props.router.push('/category');
        break;
      case 3.2:
        this.props.router.push('/subcategory');
        break;
      case 3.3:
        this.props.router.push('/product');
        break;
      // case 3:
      //   this.props.router.push('/product');
      //   break;
      case 4:
        this.props.router.push('/voucher');
        break;
      case 5:
        this.props.router.push('/post');
        break;
      case 6:
        this.props.router.push('/location');
        break;
      case 7:
        this.props.router.push('/about');
        break;
      case 8:
        this.props.router.push('/faq');
        break;
      case 9:
        this.props.router.push('/term');
        break;
      case 10:
        this.props.router.push('/socialmedia');
        break;
      // case 4:
      //   this.props.router.push('/brand');
      //   break;

      // case 7:
      //   this.props.router.push('/tablet');
      //   break;
      // case 8:
      //   this.props.router.push('/sku');
      //   break;
      case 11:
        this.props.router.push('/setting');
        break;
      case 12:
        this.props.router.push('/push');
        break;
      case 13:
        this.props.router.push('/account');
        break;

      default:
        return;

    }
  }

  handleRouteReload = (pathname) => {
    if (pathname == '/loyalty') {
      this.setState({activeKey: 2});
    }
    // if (pathname == '/level') {
    //   this.setState({activeKey: 2.1});
    // }
    // else if (pathname.includes('/levelcategory')) {
    //   this.setState({activeKey: 2.2});
    // }
    if (pathname == '/category') {
      this.setState({activeKey: 3.1});
    }
    else if (pathname.includes('/subcategory')) {
      this.setState({activeKey: 3.2});
    }
    else if (pathname.includes('/product')) {
      this.setState({activeKey: 3.3});
    }
    // else if (pathname.includes('/product')) {
    //   this.setState({activeKey: 3});
    // }
    else if (pathname.includes('/voucher')) {
      this.setState({activeKey: 4});
    }
    // else if (pathname.includes('/brand')) {
    //   this.setState({activeKey: 4});
    // }
    else if (pathname.includes('/post')) {
      this.setState({activeKey: 5});
    }
    else if (pathname.includes('/location')) {
      this.setState({activeKey: 6});
    }
    else if (pathname.includes('/about')) {
      this.setState({activeKey: 7});
    }
    else if (pathname.includes('/faq')) {
      this.setState({activeKey: 8});
    }
    else if (pathname.includes('/term')) {
      this.setState({activeKey: 9});
    }
    else if (pathname.includes('/socialmedia')) {
      this.setState({activeKey: 10});
    }
    // else if (pathname.includes('/tablet')) {
    //   this.setState({activeKey: 7});
    // }
    // else if (pathname.includes('/sku')) {
    //   this.setState({activeKey: 8});
    // }
    else if (pathname.includes('/setting/EARNJB23143DbVSs')) {
      this.setState({activeKey: 11});
    }
    else if (pathname.includes('/push')) {
      this.setState({activeKey: 12});
    }
    else if (pathname.includes('/account')) {
      this.setState({activeKey: 13});
    }
    else if (pathname == '/') {
      this.setState({activeKey: 1});
    }
    console.info('LinkComponent handleRouteReload', pathname, this.state);
  }

  componentWillMount(){
    let {pathname} = this.props.router.location;
    this.handleRouteReload(pathname);

    let user = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    this.setState({
      tabs: user.cmsTabs.split(',')
    });
  }

  handleClick = (event) => {
    const {actions} = this.props;
    actions.logoutUser(this.props.router);
  }

  handleInfo = (event) => {
    this.setState({open: true});
  }

  handleClose = () => {
    this.setState({open:false});
  }

  handleAccount = () => {
    const {router} = this.props;
    this.setState({activeKey:0});
    router.push('/credential');
  }

  linkClass = (menu) => {
    const {role, tabs} = this.state;
    if (tabs[0] == 'all') {
      return 'visible';
    }else
      return (tabs.includes(menu)) ? 'visible' : 'hide';
  }

  groupLinkClass = (menus) => {
    const {tabs} = this.state;
    if (tabs[0] == 'all') {
      return 'visible';
    }
    else {
      let contains = 0;
      menus.forEach( x => {
        if (tabs.includes(x)) contains++;
      });
      return (contains) ? 'visible' : 'hide';
    }
  }

  render() {
    const {role, tabs} = this.state;

    return (
      <div className="hidden-xs nav-fixed">
        <Header router={this.props.router}>
          <div className='nav-bar-container'>
            <ul className='nav-bar'>
              <Nav stacked activeKey={this.state.activeKey} onSelect={this.handleSelect}>
                <NavItem eventKey={1} className={this.linkClass('profile')}>
                  <Business className='nav-icon'/>Company Profile
                </NavItem>
                <NavItem eventKey={2} className={this.linkClass('loyalty')}>
                  <Star className='nav-icon'/>Rewards
                  </NavItem>
                {/*<NavItem eventKey={2} className={this.linkClass('level')}>
                  <LevelIcon className='nav-icon'/>Level
                  </NavItem>
                  <NavDropdown id="basic-nav-dropdown" eventKey={2} className={this.groupLinkClass(['level', 'levelcategory'])}
                    title={<span><Store className='nav-icon'/>Level</span>}>
                    <MenuItem eventKey={2.1} className={this.linkClass('level')}>
                      <LevelIcon className='nav-icon-sub'/>Level</MenuItem>
                    <MenuItem eventKey={2.2} className={this.linkClass('levelcategory')}>
                      <Subcategory className='nav-icon-sub'/>Level Category</MenuItem>
                    </NavDropdown>*/}
                {/*<NavItem eventKey={3} className={this.linkClass('level')}>
                  <LevelIcon className='nav-icon'/>Offers
                  </NavItem>*/}
                <NavDropdown id="basic-nav-dropdown" eventKey={3} className={this.groupLinkClass(['level', 'levelcategory'])}
                  title={<span><LocalOffer className='nav-icon'/>Offers</span>}>
                  <MenuItem eventKey={3.1} className={this.linkClass('level')}>
                    <Category className='nav-icon-sub'/>Main Category</MenuItem>
                  <MenuItem eventKey={3.2} className={this.linkClass('levelcategory')}>
                    <Subcategory className='nav-icon-sub'/>Subcategory</MenuItem>
                  <MenuItem eventKey={3.3} className={this.linkClass('product')}>
                    <Store className='nav-icon-sub'/>Product</MenuItem>
                  </NavDropdown>
                {/*<NavItem eventKey={3} className={this.linkClass('product')}>
                  <Store className='nav-icon'/>Featured Products
                  </NavItem>*/}
                <NavItem eventKey={4} className={this.linkClass('voucher')}>
                  <Voucher className='nav-icon'/>Voucher
                  </NavItem>
                {/*<NavItem eventKey={4} className={this.linkClass('brand')}>
                  <Loyalty className='nav-icon'/>Brand Management
                  </NavItem>*/}
                <NavItem eventKey={5} className={this.linkClass('post')}>
                  <Chat className='nav-icon'/>What&#39;s New
                  </NavItem>
                <NavItem eventKey={6} className={this.linkClass('location')}>
                  <Branch className='nav-icon'/>Locations
                  </NavItem>
                <NavItem eventKey={7} className={this.linkClass('about')}>
                  <LocationCity className='nav-icon'/>About Us
                  </NavItem>
                <NavItem eventKey={8} className={this.linkClass('faq')}>
                  <Term className='nav-icon'/>FAQs
                  </NavItem>
                <NavItem eventKey={9} className={this.linkClass('term')}>
                  <Description className='nav-icon'/>Terms and Conditions
                  </NavItem>
                <NavItem eventKey={10} className={this.linkClass('socialmedia')}>
                  <Share className='nav-icon'/>Social Media
                  </NavItem>
                {/*<NavItem eventKey={7} className={this.linkClass('tablet')}>
                  <Device className='nav-icon'/>POS Management
                  </NavItem>
                <NavItem eventKey={8} className={this.linkClass('sku')}>
                  <Assignment className='nav-icon'/>SKU
                  </NavItem>*/}
                <NavItem eventKey={11} className={this.linkClass('setting')}>
                  <Setting className='nav-icon'/>Earn Settings
                  </NavItem>
                <NavItem eventKey={12} className={this.linkClass('push')}>
                  <Chat className='nav-icon'/>Push
                  </NavItem>
                <NavItem eventKey={13} className={this.linkClass('account')}>
                  <Account className='nav-icon'/>Accounts
                  </NavItem>
              </Nav>
            </ul>
          </div>
        </Header>

        <div className="footer-container">
          <div className="footer-btn footer-left" onClick={this.handleAccount}>
            <Account className='footer-icon'/>
          </div>
          <div className="footer-btn" onClick={this.handleInfo}>
            <Info className='footer-icon'/>
          </div>
          <div className="footer-btn" onClick={this.handleClick}>
            <Lock className='footer-icon'/>
          </div>
        </div>

        {/* Info */}
        <InfoComponent
          open={this.state.open}
          onRequestClose={this.handleClose}/>
      </div>
    )
  }
}

LinkComponent.displayName = 'NavigationLinkComponent';

// Uncomment properties you need
// LinkComponent.propTypes = {};
// LinkComponent.defaultProps = {};
//  <li><Link to="" onClick={ () => handleClick(property) }>Logout</Link></li>

export default LinkComponent;
