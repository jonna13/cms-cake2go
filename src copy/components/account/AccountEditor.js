/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import {postUrl} from '../../api/data.api';
import _ from 'lodash';
import PrintComponent from '../common/PrintComponent';

import AccountForm from './AccountForm';

class AccountEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      account: {
        username: '',
        password: '',
        fullname: '',
        role: '',
        reportTabs: '',
        cmsTabs: '',
        profilePic: '',
        status: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.ACCOUNT,
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewAccount(this.props.params.id);
    }
    actions.getBrands();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.accountState.selectedRecord)) {
        this.setState({
          account: nextProps.accountState.selectedRecord
        });
      }
    }
  }

  // handle going back to Account
  handleReturn = () => {
    this.props.router.push('/account');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    console.info('handleUpdate:state', this.state);
    if (this.validateInput(this.state.account)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_ACCOUNT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateAccount(this.state.account, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    console.info('handleAdd:state', this.state);
    if (this.validateInput(this.state.account)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_ACCOUNT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addAccount(this.state.account, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const Expression = (/^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z]{5,}$/);
    const {dialogActions} = this.props;
    if (data.username == '') {
      dialogActions.openNotification('Oops! No username found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (/\s/g.test(data.username)) {
      dialogActions.openNotification('Oops! username shoudn\'t have space', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.password == '') {
      dialogActions.openNotification('Oops! No password found!!!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.password != data.reenter) {
      dialogActions.openNotification('Oops! password did not match found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (!data.password.match(Expression)) {
      dialogActions.openNotification('Should have at least 1 number & 1 letter. At least 5 characters', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.role == '') {
      dialogActions.openNotification('Oops! No role found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    else {
      if (['administrator', 'marketing', 'manager', 'project-manager', 'developer'].includes(data.role)) {
        this.state.account['cmsTabs'] = Config.ACCOUNT_ROLE_TABS[data.role].cms.join();
        this.state.account['reportTabs'] = Config.ACCOUNT_ROLE_TABS[data.role].reports.join();
      }
    }
    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let account = this.state.account;
    account[field] = e.target.value;
  }

  handleGeneratedValue = (name, value) => {
    let account = this.state.account;
    account[name] = value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.account['status'] = e;
  }

  handleReportTabChange = (val) => {
    this.state.account['reportTabs'] = val.map(v=> {return v.value}).join();
  }

  handleCmsTabChange = (val) => {
    this.state.account['cmsTabs'] = val.map(v=> {return v.value}).join();
  }

  handleRoleChange = (val) => {
    if (['administrator', 'marketing', 'manager', 'project-manager', 'developer'].includes(val)) {
      this.state.account['cmsTabs'] = Config.ACCOUNT_ROLE_TABS[val].cms.join();
      this.state.account['reportTabs'] = Config.ACCOUNT_ROLE_TABS[val].reports.join();
    }
    this.state.account['role'] = val;
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Account' : 'Add Account'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <AccountForm
          data={this.props.accountState}
          shouldEdit={ (this.props.params.id) ? true : false }
          actions={this.props.actions}
          onChange={this.handleData}
          onGenerateValue={this.handleGeneratedValue}
          onStatusChange={this.handleStatusChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          onRoleChange={this.handleRoleChange}
          onReportTabChange={this.handleReportTabChange}
          onCmsTabChange={this.handleCmsTabChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    accountState: state.accountState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    accountAction: require('../../actions/accountAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.accountAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountEditor);
