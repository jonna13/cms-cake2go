/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './PostGridRow';

class PostList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getPosts(params);
  }

  render(){
    let {posts} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Title', value: 'title'},
      { title: 'Image', value: 'image'},
      { title: 'Description', value: 'description'},
      { title: 'Type', value: 'type'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (posts.records.length > 0) {
      posts.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.POST}/>);
      });
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.posts}
            onGetData={this.handleGetData}
            imageModule={Config.IMAGE.POST}
            hasImage={true}
            imageKey='image'
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.posts}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default PostList;
