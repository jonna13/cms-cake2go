'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import LevelCategoryList from './LevelCategoryList';

class LevelCategoryComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/levelcategory_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/levelcategory/'+item.levelCategoyID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.levelcategories;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.levelcategories;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.levelcategories;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.levelcategories;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.levelcategories;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getLevelCategory(params);
  }

  render() {
    const {pageinfo} = this.props.data.levelcategories;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Level Category</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.levelcategories.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.levelcategories.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.levelcategories.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <LevelCategoryList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

LevelCategoryComponent.displayName = 'LevelLevelCategoryComponent';

// Uncomment properties you need
// LevelCategoryComponent.propTypes = {};
// LevelCategoryComponent.defaultProps = {};

export default LevelCategoryComponent;
