/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './AboutGridRow';

class AboutList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getAbouts(params);
  }

  render(){
    let {abouts} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Description', value: 'description'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (abouts.records.length > 0) {
      abouts.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.abouts}
            onGetData={this.handleGetData}
            hasImage={false}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.abouts}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default AboutList;
