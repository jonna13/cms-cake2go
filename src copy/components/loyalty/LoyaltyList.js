/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import GridRow from './LoyaltyGridRow';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

class LoyaltyList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getLoyalty(params, this.props.router);
  }

  render(){
    let {loyalty} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Type', value: 'type'},
      { title: 'Points', value: 'points'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (loyalty.records.length > 0) {
      loyalty.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module='loyalty'/>);
      })
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.loyalty}
            onGetData={this.handleGetData}
            hasImage={false}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.loyalty}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default LoyaltyList;
