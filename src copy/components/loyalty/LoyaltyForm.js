/**
 * Created by jonna on 2/12/18.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';

class LoyaltyForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      levelID: '',
      type: '',
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        levelID: nextProps.data.selectedRecord.levelID,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  handleLevelChange = (e, idx, levelID) => {
    this.setState({levelID});
    this.props.onLevelChange(levelID);
  };

  handleTypeChange = (e, idx, type) => {
    this.setState({type});
    this.props.onTypeChange(type);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    let levelList = [];
    const {data} = this.props;

    if (data.levels) {
      levelList.push(<MenuItem key={0} value='' primaryText='Select Level' />);
      data.levels.map( (val, idx) => {
        levelList.push(<MenuItem key={(idx+1)} value={val.levelID} primaryText={val.name} />);
      });
    }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>

                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="points"
                  type="number"
                  min="0"
                  hintText="Enter Points"
                  floatingLabelText="Points"
                  defaultValue={(data.selectedRecord.points) ? data.selectedRecord.points : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <SelectField value={data.selectedRecord.type} onChange={this.handleTypeChange} floatingLabelText="Type" hintText="Select Type" floatingLabelFixed={true}>
                  <MenuItem value="regular" primaryText="Regular" />
                  <MenuItem value="premium" primaryText="Premium" />
                </SelectField><br />
                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <DefaultTextArea
                  name="terms"
                  hintText="Enter Terms"
                  floatingLabelText="Terms"
                  defaultValue={(data.selectedRecord.terms) ? data.selectedRecord.terms : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default LoyaltyForm;
