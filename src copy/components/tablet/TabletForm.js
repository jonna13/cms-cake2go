/**
 * Created by jedachas on 3/10/17.
 */
import React from 'react';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';

class TabletForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      locID: '',
      brandID: '',
      brandName: '',
      postype: 'pasi',
      deploy: 'false',
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        locID: nextProps.data.selectedRecord.locID,
        brandID: nextProps.data.selectedRecord.brandID,
        brandName: nextProps.data.selectedRecord.brandName,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  handleLocationChange = (e, idx, locID) => {
    this.setState({locID});
    this.props.onLocationChange(locID);
  };

  handleBrandChange = (e, idx, brandID) => {
    // const {actions} = this.props;
    // let name = this.props.data.brands.filter(x=> { return x.brandID == brandID})[0].name;
    this.setState({brandID});
    this.props.onBrandChange(brandID, name);
    // actions.getLocations(brandID);
  };

  handlePosTypeChange = (e, idx, postype) => {
    this.setState({postype});
    this.props.onPosTypeChange(postype);
  }

  handleDeployChange = (e, idx, deploy) => {
    this.setState({deploy});
    this.props.onDeployChange(deploy);
  };

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    let locationList = [];
    let brandList = [];
    const {data} = this.props;
    console.log('data', data);
    if (this.props.data.locations) {
      locationList.push(<MenuItem key={0} value='Select Location' />);
      this.props.data.locations.map( (val, idx) => {
        locationList.push(<MenuItem key={(idx+1)} value={val.locID} primaryText={val.name} />);
      });
    }
    if (this.props.data.brands) {
      brandList.push(<MenuItem key={0} value='Select Brand' />);
      this.props.data.brands.map( (val, idx) => {
        brandList.push(<MenuItem key={(idx+1)} value={val.brandID} primaryText={val.name} />);
      });
    }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={12} lg={12}>
              <div className='content-field-holder'>
                <TextField
                  disabled={true}
                  name="deviceCode"
                  floatingLabelText="Device Code"
                  defaultValue={(data.selectedRecord.deviceCode) ? data.selectedRecord.deviceCode : '' }
                  onChange={this.props.onChange}
                  className={(this.props.shouldEdit) ? 'textfield-default' : 'hide'}
                /><br />
                <SelectField
                  value={(data.selectedRecord.brandID) ? data.selectedRecord.brandID: this.state.brandID}
                  onChange={this.handleBrandChange}
                  className='textfield-regular'
                  floatingLabelText="Brand"
                  hintText="Select Brand"
                  autoWidth={false}>
                  {brandList}
                </SelectField><br />
                <SelectField
                  value={(data.selectedRecord.locID) ? data.selectedRecord.locID: this.state.locID}
                  onChange={this.handleLocationChange}
                  className='textfield-regular'
                  floatingLabelText="Location"
                  hintText="Select Location"
                  autoWidth={false}>
                  {locationList}
                </SelectField><br />
                {/*<SelectField value={(data.selectedRecord.postype) ? data.selectedRecord.postype : this.state.postype} onChange={this.handlePosTypeChange}
                  autoWidth={false} floatingLabelText="POS Type" hintText="Select POS Type" floatingLabelFixed={true}>
                  <MenuItem value="pasi" primaryText="PASI" />
                  <MenuItem value="aloha" primaryText="ALOHA" />
                </SelectField><br />*/}
                <TextField
                  type="text"
                  name="terminalNum"
                  hintText="Enter Terminal No."
                  floatingLabelText="Terminal No."
                  defaultValue={(data.selectedRecord.terminalNum) ? data.selectedRecord.terminalNum : '' }
                  onChange={this.props.onChange}
                  rows={2}
                /><br />
                {/*<DropDownMenu
                  value={this.state.brand}
                  onChange={this.handleBrandChange}
                  autoWidth={true}
                  className={(!this.props.shouldEdit) ? 'dropdownButton-fixed-300' : 'dropdownButton'}>
                  {brandList}
                </DropDownMenu>
                <DropDownMenu
                  value={this.state.location}
                  onChange={this.handleLocationChange}
                  autoWidth={true}
                  openImmediately={(!this.props.shouldEdit) ? true: false}
                  className={(!this.props.shouldEdit) ? 'dropdownButton-fixed-300' : 'dropdownButton'}>
                  {locationList}
                </DropDownMenu>*/}
                { (this.props.shouldEdit) ?

                  <SelectField value={(data.selectedRecord.deploy) ? data.selectedRecord.deploy : this.state.deploy}
                    onChange={this.handleDeployChange}
                    autoWidth={false} floatingLabelText="Deploy" hintText="Select POS Type" floatingLabelFixed={true}>
                    <MenuItem value="true" primaryText="True" />
                    <MenuItem value="false" primaryText="False" />
                  </SelectField>: ''}
                <Checkbox
                  label="Enable Tablet"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }
  }
}

export default TabletForm;
