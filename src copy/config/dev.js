'use strict';

import baseConfig from './base';

let config = {
  appEnv: 'dev',  // feel free to remove the appEnv property here
  PROJECT_PATH: 'http://54.254.157.211/',
  // PROJECT_PATH: 'http://122.53.63.92/project/henann/',
  PROJECT_BASE: 'dashboard',
};

export default Object.freeze(Object.assign({}, baseConfig, config));
